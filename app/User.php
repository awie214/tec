<?php

namespace App;

use App\Http\Traits\System_Config;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable, System_Config;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'middle_name', 'username', 'email', 'password', 'confirm_password', 'user_type' 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
    */

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isSuperAdmin()
    {
        return Auth::user()->level == 0;
    }

    // Regular Admin or Supervisor Account

    public function isRegularAdmin()
    {
        return Auth::user()->level == 1;
    }

    public function isProcessor()
    {
        return Auth::user()->level == 2;
    }

    public function isUser()
    {
        return Auth::user()->level == 3;
    }

    public function isTravelTaxUser()
    {
        return Auth::user()->level == 4;
    }

    public function getFullName()
    {
        $user = Auth::user();

        return $user->last_name.', '.$user->first_name.' '.$user->middle_name[0].'.';
    }

    public function getFullNameFML()
    {
        $user = Auth::user();

        return $user->first_name.' '.$user->middle_name[0].'. '.$user->last_name;
    }

    public function getUserLevel()
    {
        $user = Auth::user();

        $user_level = $this->user_level;

        return $user_level[$user->level]['name'];
    }
}
