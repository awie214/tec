<?php

namespace App\Http\Traits;

use DB;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

trait System_Config
{
	protected $user_level = array(
		['id' => 0, 'name' => 'super_admin', 'level' => 0],
		['id' => 1, 'name' => 'regular_admin', 'level' => 1],
		['id' => 2, 'name' => 'processor', 'level' => 2],
		['id' => 3, 'name' => 'user', 'level' => 3],
		['id' => 4, 'name' => 'travel_tax_user', 'level' => 4],
	);

	protected $filter_by = array('user_level');

	protected $system_config_option = ['user_account'];

	protected function is_system_config_option_exist($option)
	{
		if(!in_array($option, $this->system_config_option)) throw new Exception(trans('page.not_found', ['attribute' => strtolower(trans('page.option'))]));

		return true;
	}

	protected function count_user_account($id = null, $email_address = null, $except = array(), $user_level = array())
	{
		$query = DB::table('users')
		->where('users.deleted_at', null);

		if($email_address) $query->where('users.email', 'like', '%'.$email_address.'%');

		if($except) $query->wherenotin('users.id', $except);

		if($user_level) $query->wherein('users.level', $user_level);

		if($id) $query->where('users.id', $id);

		return $query->count();
	}

	protected function get_user_account($id = null, $level = null, $status = null)
	{
		$query = DB::table('users')
		->where('users.deleted_at', null)
		->select('users.id', 'users.last_name', 'users.first_name', 'users.middle_name', 'users.email', 'users.username', 'users.level', 'users.status', DB::raw('CONCAT(users.last_name,", ",users.first_name," ", SUBSTRING(users.middle_name, 1, 1),".") AS full_name'));

		if($level) $query->where('users.level', $level);

		if(in_array($status, ['0', '1'])) $query->where('users.status', $status);

		if($id)
		{
			 return $query->where('users.id', $id)->first();
		}
		else
		{
			return $query->orderby('users.last_name')->get();
		}
	}

	protected function check_exist_user_account($id, $level = null, $status = null)
	{
		if($this->count_user_account($id) == 0) throw new Exception(trans('page.no_record_found'));

		return $this->get_user_account($id, $level, $status);
	}
}