<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Application;
use App\Http\Traits\System_Config;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

Class ApplicationController extends Controller
{
	use Application, System_Config;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function tec_application_datatables(request $request)
    {
        try
        {
            $option = $request->get('selected_option');

            $data = array();

            if(in_array($option, ['passport', 'airline', 'additional', 'file_0', 'file_1', 'file_2', 'file_3']))
            {
                $section_id = $request->get('section_id');
                $tec_id = $request->get('tec_id');

                $data = $this->get_tec_attachment_by(null, $section_id, $tec_id, $option);
            }

            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option){
                $start_tools = '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/attachment/tec/'.$data->tec_id.'/'.$data->file_name).'" class="dropdown-item" target="_blank" >
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>';

                if(!$data->generate_to_email)
                {
                    if(auth::user()->isProcessor()) $start_tools = $start_tools.'<a href="javascript:void(0);" class="dropdown-item" onclick=remove_item("'.$option.'",'.$data->id.')><i class="icon icon-left mdi mdi-delete"></i>Remove</a>';
                }
                

                if(auth::user()->isSuperAdmin()) $start_tools = $start_tools.'<a href="javascript:void(0);" class="dropdown-item" onclick=remove_item("'.$option.'",'.$data->id.')><i class="icon icon-left mdi mdi-delete"></i>Remove</a>';

                $end_tools =  '</div></div>';

                return  $start_tools.$end_tools;
            })
            ->addColumn('file', function($data){
                $explode_file_name = explode('.', $data->file_name);

                if(in_array($explode_file_name[1], ['png', 'jpg', 'jpeg']))
                {
                    return '<img src="'.url('/attachment').'/tec/'.$data->tec_id.'/'.$data->file_name.'" width=120px height=120px />';
                }
                else
                {
                    return 'Preview Not Available';
                }
            })
            ->addColumn('file_type', function($data){
                $explode_file_name = explode('.', $data->file_name);

                if(in_array($explode_file_name[1], ['png', 'jpg', 'jpeg']))
                {
                    return 'Image';
                }
                else
                {
                    return 'PDF';
                }
            })
            ->editColumn('upload_at', function($data){
                return [
                  'display' => e(date('m/d/Y H:i:s', strtotime($data->upload_at))),
                  'timestamp' => strtotime($data->upload_at)
               ];
            });

            $rawColumns = ['action', 'file'];
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 201);
        }

        return $datatables->rawColumns($rawColumns)->make(true);
    }

    public function filter_tec_application_by(request $request)
    {   
        try
        {
            $tec_id = $request->get('tec_id');
            $filter_by = 'application_status';
            $filter_user = $request->get('filter_user');
            $filter_list = $request->get('filter_list');
            $filter_order = $request->get('filter_order');
            $filter_value = $request->get('filter_value');

            $filter_month = $request->get('filter_month');
            $filter_year = $request->get('filter_year');

            $query = DB::table('tec_application as ta')
            ->where('ta.deleted_at', null)
            ->select('ta.*', DB::raw('CONCAT(UCASE(ta.last_name),", ",UCASE(ta.first_name)," ", IFNULL(SUBSTRING(UCASE(ta.middle_name), 1, 1), ""), IF(ISNULL(SUBSTRING(UCASE(ta.middle_name), 1, 1)) or ta.middle_name != "", ".", "")) AS full_name'), DB::raw('CONCAT("TEC-", LPAD(ta.id, 5, 0)) AS tec_id'), DB::raw('DATE_FORMAT(ta.date_application, "%d %b %Y") as date_application'), DB::raw('CONCAT("APP-", LPAD(ta.id, 5, 0)) AS app_id'));

            if($filter_by)
            {
                if($filter_by == 'application_status')
                {
                    if($filter_list) 
                    {
                        if($filter_list == 4)
                        {
                            $query->whereNotNull('ta.generate_to_email');

                            if(Auth::user()->isRegularAdmin()) 
                            {
                                $query->where('ta.supervisor_id', Auth::user()->id);
                            }
                        }
                        elseif($filter_list == 'all')
                        {
                            if(Auth::user()->isRegularAdmin()) 
                            {
                                $query->wherein('ta.status', [2, 3])->whereNull('ta.generate_to_email');
                            }
                            elseif(Auth::user()->isProcessor() || Auth::user()->isSuperAdmin())
                            {
                                $query->wherein('ta.status', [1, 2, 3])->whereNull('ta.generate_to_email');
                            }
                        }
                        else
                        {
                            $query->where('ta.status', $filter_list)->whereNull('ta.generate_to_email');
                        }
                    }  
                }
            }

            if(in_array($filter_order, ['0', '1'])) 
            {
                $filter_order = $filter_order == '1' ? 'asc' : 'desc';

                $query->orderby('ta.id', $filter_order);
            }

            // Only for the processor

            if(Auth::user()->isProcessor())
            {
                $query->where('ta.assign_processor_id', Auth::user()->id);
            }
            else
            {
                if($filter_user) $query->where('ta.assign_processor_id', $filter_user);
            }

            //if($filter_value) $query->where('ta.id', 'like', '%'.$filter_value.'%');

            if($filter_value) 
            {
                $query->where(function ($query) use ($filter_value) {
                    $query->where('ta.last_name', 'like', $filter_value.'%')
                          ->orwhere('ta.first_name', 'like', '%'.$filter_value.'%')
                          ->orwhere('ta.middle_name', 'like', '%'.$filter_value.'%');
                });
            }

            if($filter_month && $filter_year)
            {
                $start_date = implode('-', [$filter_year, $filter_month, '01']);
                $end_date = implode('-', [$filter_year, $filter_month, '31']);

                $query->whereBetween('date_application', [ $start_date , $end_date]);
            }

            $response = array();

            if($tec_id)
            {
                $tec_application = $query->where('ta.id', $tec_id)->first(); 

                /**
                $tec_application->file_0 = null;
                $tec_application->file_1 = null;
                $tec_application->file_2 = null;
                $tec_application->file_3 = null;

                foreach($this->get_tec_application_upload(null, $tec_application->id, $tec_application->applicant_type_id) as $key => $val)
                {
                    $file_name = substr($val->file_name, 0, 6); //'file_'.$key;

                    $tec_application->$file_name =  $val->file_name;
                }
                **/

                $response['tec_application'] = $tec_application;
            }
            else
            {
                $tec_application = $query->get();

                $response['tec_applications'] = $tec_application;
                $response['count_tec_application'] = count($tec_application);
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }

    public function tr_application_datatables(request $request)
    {
        try
        {
            $option = $request->get('selected_option');

            $data = array();

            if(in_array($option, ['passport', 'airline', 'tieza_official_receipt', 'additional', 'file_0', 'file_1', 'file_2', 'file_3', 'file_4']))
            {
                $section_id = $request->get('section_id');
                $tr_id = $request->get('tr_id');

                $data = $this->get_tr_attachment_by(null, $section_id, $tr_id, $option);
            }

            $datatables = Datatables::of($data)
            ->addColumn('action', function($data) use ($option){
                $start_tools = '<div class="tools">
                            <button type="button" data-toggle="dropdown" class="btn btn-secondary dropdown-toggle" aria-expanded="false"><i class="icon icon-left mdi mdi-settings-square"></i>Options<span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                            <div role="menu" class="dropdown-menu" x-placement="bottom-start">
                                <a href="'.url('/attachment/tr/'.$data->tr_id.'/'.$data->file_name).'" class="dropdown-item" target="_blank" >
                                    <i class="icon icon-left mdi mdi-eye"></i>View
                                </a>';

                if($data->is_complete == 0)
                {
                    if(auth::user()->isProcessor()) $start_tools = $start_tools.'<a href="javascript:void(0);" class="dropdown-item" onclick=remove_item("'.$option.'",'.$data->id.')><i class="icon icon-left mdi mdi-delete"></i>Remove</a>';
                }
                

                if(auth::user()->isSuperAdmin()) $start_tools = $start_tools.'<a href="javascript:void(0);" class="dropdown-item" onclick=remove_item("'.$option.'",'.$data->id.')><i class="icon icon-left mdi mdi-delete"></i>Remove</a>';

                $end_tools =  '</div></div>';

                return  $start_tools.$end_tools;
            })
            ->addColumn('file', function($data){
                $explode_file_name = explode('.', $data->file_name);

                if(in_array($explode_file_name[1], ['png', 'jpg', 'jpeg']))
                {
                    return '<img src="'.url('/attachment').'/tr/'.$data->tr_id.'/'.$data->file_name.'" width=120px height=120px />';
                }
                else
                {
                    return 'Preview Not Available';
                }
            })
            ->addColumn('file_type', function($data){
                $explode_file_name = explode('.', $data->file_name);

                if(in_array($explode_file_name[1], ['png', 'jpg', 'jpeg']))
                {
                    return 'Image';
                }
                else
                {
                    return 'PDF';
                }
            })
            ->editColumn('upload_at', function($data){
                return [
                  'display' => e(date('m/d/Y H:i:s', strtotime($data->upload_at))),
                  'timestamp' => strtotime($data->upload_at)
               ];
            });

            $rawColumns = ['action', 'file'];
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 201);
        }

        return $datatables->rawColumns($rawColumns)->make(true);
    }

    public function filter_tr_application_by(request $request)
    {
        try
        {
            $tr_id = $request->get('tr_id');

            $filter_by = 'application_status';
            $filter_value = $request->get('filter_value');

            // default
            $filter_user = $request->get('filter_user');
            $filter_list = $request->get('filter_list');
            $filter_order = $request->get('filter_order');

            // travel tax main
            $filter_month = $request->get('filter_month');
            $filter_year = $request->get('filter_year');
            $filter_form = $request->get('filter_form');

            $query = DB::table('tr_application as tr')
            ->where('tr.deleted_at', null)
            ->select('tr.*', DB::raw('CONCAT(tr.last_name,", ",tr.first_name," ", IFNULL(SUBSTRING(tr.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(tr.middle_name, 1, 1)) or tr.middle_name != "", ".", "")) AS full_name'), DB::raw('CONCAT("TR-", LPAD(tr.id, 5, 0)) AS tr_id'), DB::raw('DATE_FORMAT(tr.date_application, "%d %b %Y") as date_application'), DB::raw('CONCAT("APP-", LPAD(tr.id, 5, 0)) AS app_id'));

            if($filter_by)
            {
                if($filter_by == 'application_status')
                {
                    if($filter_list) 
                    {
                        if($filter_list == 4)
                        {
                            $query->where('tr.is_complete', 1);

                            if(Auth::user()->isRegularAdmin()) 
                            {
                                $query->where('tr.supervisor_id', Auth::user()->id);
                            }
                        }
                        elseif($filter_list == 'all')
                        {
                            if(Auth::user()->isRegularAdmin()) 
                            {
                                $query->wherein('tr.status', [2, 3])->where('tr.is_complete', 0);
                            }
                            elseif(Auth::user()->isProcessor() || Auth::user()->isSuperAdmin())
                            {
                                $query->wherein('tr.status', [1, 2, 3])->where('tr.is_complete', 0);
                            }
                        }
                        else
                        {
                            $query->where('tr.status', $filter_list)->where('tr.is_complete', 0);
                        }
                    }
                    else
                    {
                        if(Auth::user()->isTravelTaxUser())
                        {
                            $query->where('tr.is_complete', 1);
                        }
                    }
                }
            }

            if($filter_month && $filter_year)
            {
                if($filter_month !== 'all')
                {
                    $query->whereMonth('tr.date_application', $filter_month);
                } 
                elseif($filter_year !== 'all')
                {
                    $query->whereYear('tr.date_application', $filter_year);
                }   
            }

            if($filter_form)
            {
                if($filter_form !== 'all') $query->where('tr.type_travel_tax_refund', $filter_form);
            }

            if(in_array($filter_order, ['0', '1'])) 
            {
                $filter_order = $filter_order == '1' ? 'asc' : 'desc';

                $query->orderby('tr.id', $filter_order);
            }

            // Only for the processor

            if(Auth::user()->isProcessor())
            {
                $query->where('tr.assign_processor_id', Auth::user()->id);
            }
            else
            {
                if($filter_user) $query->where('tr.assign_processor_id', $filter_user);
            }

            //if($filter_value) $query->where('ta.id', 'like', '%'.$filter_value.'%');

            if($filter_value) 
            {
                $query->where(function ($query) use ($filter_value) {
                    $query->where('tr.last_name', 'like', $filter_value.'%')
                          ->orwhere('tr.first_name', 'like', '%'.$filter_value.'%')
                          ->orwhere('tr.middle_name', 'like', '%'.$filter_value.'%');
                });
            }

            $response = array();

            if($tr_id)
            {
                $tr_application = $query->where('tr.id', $tr_id)->first(); 

                $response['tr_application'] = $tr_application;
            }
            else
            {
                $tr_application = $query->get();

                $response['tr_applications'] = $tr_application;
                $response['count_tr_application'] = count($tr_application);
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }

    public function filter_section_by(request $request)
    {
        try
        {
            $section_id = $request->get('section_id');
            $section_type = $request->get('section_type');

            $query = DB::table('sections');

            if($section_type) $query->where('sections.section_type', $section_type);
            
            $response = array();

            if($section_id)
            {
                $response['section'] = $query->where('sections.id', $section_id)->first(); 
            }
            else
            {
                $sections = $query->get();

                $response['sections'] = $sections;
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }

    public function filter_section_upload_by(request $request)
    {
        try
        {
            $section_upload_id = $request->get('section_upload_id');
            $section_id = $request->get('section_id');

            $query = DB::table('sections_upload as su');

            if($section_id) $query->where('su.section_id', $section_id);
            
            $response = array();

            if($section_upload_id)
            {
                $response['sections_upload'] = $query->where('su.id', $section_upload_id)->first(); 
            }
            else
            {
                $sections_upload = $query->get();

                $response['sections_uploads'] = $sections_upload;
                $response['count_sections_upload'] = count($sections_upload);
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }

    public function get_image_application(request $request)
    {
        $file_name = $request->get('file_name');

        $folder_path = storage_path('esig/'.$file_name);

        if (file_exists($folder_path)) {
            return response()->file($folder_path, array('Content-Type' =>'image/jpeg'));
        }

        abort(404);
    }

    public function filter_airline_by(request $request)
    {
        try
        {
            $airline_id = $request->get('airline_id');

            $query = DB::table('airlines');

            $response = array();

            if($airline_id)
            {
                $response['airline'] = $query->where('airlines.id', $airline_id)->first(); 
            }
            else
            {
                $airlines = $query->get();

                $response['airlines'] = $airlines;
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }
}