<?php

namespace App\Http\Controllers\APIController;

use App\Http\Controllers\Controller;
use App\Http\Traits\System_Config;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

Class SystemConfigController extends Controller
{
	use System_Config;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function filter_user_account_by(request $request)
    {   
        try
        {
            $user_id = $request->get('user_id');
            $filter_by = $request->get('filter_by');
            $filter_list = $request->get('filter_list');
            $filter_status = $request->get('filter_status');
            $filter_value = $request->get('filter_value');

            $query = DB::table('users')
            ->where('users.deleted_at', null)
            ->select('users.id', 'users.last_name', 'users.first_name', 'users.middle_name', 'users.email', 'users.username', 'users.level', 'users.status', DB::raw('CONCAT(users.last_name,", ",users.first_name," ", IFNULL(SUBSTRING(users.middle_name, 1, 1), ""), IF(SUBSTRING(users.middle_name, 1, 1) IS NOT NULL, ".", "")) AS full_name'));

            if($filter_by)
            {
                if($filter_by == 'user_level')
                {
                    $query->where('users.level', $filter_list);
                }
            }

            if(in_array($filter_status, ['0', '1'])) $query->where('users.status', $filter_status);
        
            if($filter_value) 
            {
                $query->where(function ($query) use ($filter_value) {
                    $query->where('users.last_name', 'like', $filter_value.'%')
                          ->orwhere('users.first_name', 'like', '%'.$filter_value.'%')
                          ->orwhere('users.middle_name', 'like', '%'.$filter_value.'%');
                });
            }

            $response = array();

            if($user_id)
            {
                $response['user_account'] = $query->where('users.id', $user_id)->first(); 
            }
            else
            {
                $user_account = $query->orderby('users.last_name', 'asc')->get();

                $response['user_accounts'] = $user_account;
                $response['count_user_account'] = count($user_account);
            }
        }
        catch(Exception $e)
        {
            return response(['errors' => $e->getMessage()], 422); 
        }

        return response($response);
    }
}