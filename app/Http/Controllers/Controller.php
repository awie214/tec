<?php

namespace App\Http\Controllers;

use App\Http\Traits\Page;
use Exception;
use DB;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Mail;
use PDF;
use Spatie\PdfToImage\Pdf as SpatiePdf;

class Controller extends BaseController
{
    use AuthorizesRequests, 
        DispatchesJobs, 
        ValidatesRequests, 
        Page;

    public static function convert_number_to_words($number) 
    {

        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . Self::convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . Self::convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = Self::convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= Self::convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }

    public function user()
    {
        if(Auth::user()->isUser())
        {
            return Auth::user();
        }
        
        return null;
    }

    public function test_email()
    {
        $to_name = 'Xybriel';
        $to_email = "xybrielmiranda.tieza@gmail.com";
        $data = array('name'=> 'Xyb', 'body' => 'a test mail', 'image_src' => url('/images/5/id_picture_2x2_20200612_QsWljMBxDQ4ZEokBdvveLmxdLB2X5Cai0qRrPW1fIUbRuwWnsk.png'));

        Mail::send('mail.tec-report', $data, function($message) use ($to_email){
        $message->from(env('MAIL_USERNAME'),'Test'); 
        $message->to($to_email)->subject('test');
        });
    }

    public function test_viewmail($option)
    {

        if($option == 'ar')
        {
            $file = 'mail.tr_acknowledge_receipt';
        }
        elseif($option == 'form_353')
        {
            $file = 'mail.tr_form_353';
        }

        $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($file, [])->setWarnings(false);

            $folder_path = storage_path('tieza/test');

            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

            $pdf->save($folder_path.'/test_'.$option.'.pdf');


            //return $pdf->stream($file_name, array("Attachment" => 0));

          //  $pdf = new SpatiePdf($folder_path.'/test.pdf');

           // $pdf->saveImage($folder_path.'/test.png');

           // $generate_image = $folder_path.'/test.png';

        return view($file);
    }

    public function date_flight()
    {
        return view('home.check_date_flight');
    }

    public function check_date_flight(request $request)
    {
        try
        {
            $rules = [
            'date_flight' => 'required|date|date_format:Y-m-d'
            ];

            $this->validate_request($request->all(), $rules);

            $date_flight = $request->get('date_flight');
            //$date_flight_added_days = date('Y-m-d', strtotime($date_flight. ' + 3 day'));
            $date_today = date('Y-m-d');
            $date_today_add_days = date('Y-m-d', strtotime($date_today. ' + 4 day'));

            if($date_today_add_days > $date_flight) throw new Exception(json_encode(['date_flight' => ['You can only apply for your TEC online if your flight is more than 3 days from the date of your application. Please visit any of our Travel Tax sattelite offices or airport counters to apply for your TEC on-site. Thank you.']]));
        }
        catch(Exception $e) 
        {
            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }

    protected function update_name()
    {
        $clients = DB::table('tec_application as ta')
        ->where('ta.deleted_at', null)
        ->join('users', 'users.id', 'ta.user_id')
        ->select('ta.*', 'users.last_name as ln', 'users.first_name as fn', 'users.middle_name as mn', 'users.email as umail', 'users.mobile_no as umobile')
        ->get();

        foreach($clients as $key => $val)
        {   
            DB::table('tec_application as ta')
            ->where('ta.id', $val->id)
            ->update([
            'last_name' => $val->ln,
            'first_name' => $val->fn,
            'middle_name' => $val->mn,
            'email' => $val->umail,
            'mobile_no' => $val->umobile
            ]);
        }

        dd('Update successfully');


    }
}
