<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\System_Config;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class SystemConfigController extends Controller
{
    use System_Config;

    private $system_config_url;

    private $system_config_icon;

    private $input_user_account;

    public function __construct()
    {
    	$this->middleware('auth');

        $this->module = 'system_config';

        $this->system_config_url = url('/system_config');

        $this->system_config_icon = 'mdi mdi-settings';

        $this->input_user_account = array(
            ['field_name' => 'last_name', 'input_name' => 'last_name'],
            ['field_name' => 'first_name', 'input_name' => 'first_name'],
            ['field_name' => 'middle_name', 'input_name' => 'middle_name'],
            ['field_name' => 'username', 'input_name' => 'username'],
            ['field_name' => 'email', 'input_name' => 'email_address'],
            ['field_name' => '', 'input_name' => 'new_password'],
            ['field_name' => '', 'input_name' => 'confirm_password'],
            ['field_name' => 'level', 'input_name' => 'user_level'],
            ['field_name' => 'status', 'input_name' => 'user_status'],
        );
   	}

    public function system_config(request $request, $option)
    {
        try
        {
            $this->is_system_config_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option, 'system_config_url' => $this->system_config_url, 'icon' => $this->system_config_icon];

            if($option == 'user_account')
            {
                $view = 'system_config.user_account.index';

                $data = array_merge($data, ['file' => 'system_config.user_account.form', 'default_inputs' => $this->input_user_account, 'user_level' => $this->user_level, 'filter_by' => $this->filter_by]);
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        
        return view($view, $data);
    }

    public function save_system_config(request $request, $option)
    {
        try 
        { 
            $this->is_system_config_option_exist($option);

            $data = array();

            $rules = [
            'last_name' => 'required|max:50',
            'first_name' => 'required|max:50',
            'middle_name' => 'sometimes|nullable|max:50',
            'email_address' => 'required|email'
            ];

            if($new_password = $request->get('new_password'))
            {
                $rules['new_password'] = 'required|min:6|max:50';
                $rules['confirm_password'] = 'same:new_password';

                $data['password'] = bcrypt($request->get('new_password'));
            }

            $rules['user_level'] = 'in:'.implode(',', array_column($this->user_level, 'level'));
            $rules['user_status'] = 'in:0,1';

            $this->validate_request($request->all(), $rules);

            $last_name = $request->get('last_name');
            $first_name = $request->get('first_name');
            $middle_name = $request->get('middle_name');


            $data = array_merge($data, [
            'last_name' => $last_name,
            'first_name' => $first_name,
            'middle_name' => $middle_name,
            'email' => $request->get('email_address'),
            'level' => $request->get('user_level'),
            'status' => $request->get('user_status'),
            ]);

            


            if($user_id = $request->get('user_id'))
            {
                $implode_username = implode('', [$first_name[0], $middle_name[0], $last_name[0], str_pad($user_id, 4, 0, STR_PAD_LEFT)]);

                $data = array_merge($data, [
                'username' => $implode_username,
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ]);

                $this->check_exist_user_account($user_id);

                DB::beginTransaction();

                DB::table('users')
                ->where('users.id', $user_id)
                ->update($data);

                DB::commit();
            }
            else
            {
                $data = array_merge($data, [
                'created_by' => Auth::user()->id
                ]);

                DB::beginTransaction();

                DB::table('users')
                ->insert($data);

                $lastID = DB::getPdo()->lastInsertId();

                $implode_username = implode('', [$first_name[0], $middle_name[0], $last_name[0], str_pad($lastID, 4, 0, STR_PAD_LEFT)]);

                DB::table('users')
                ->where('users.id', $lastID)
                ->update([
                'username' => $implode_username,
                ]);

                DB::commit();
            }
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }

    public function create_system_config(request $request, $option)
    {
        
    }

    public function store_system_config(request $request, $option)
    {

    }

    public function edit_system_config(request $request, $option)
    {

    }

    public function update_system_config(request $request, $option)
    {

    }

    public function delete_system_config(request $request, $option)
    {
        try 
        { 
            $this->is_system_config_option_exist($option);

            $user_id = $request->get('user_id');

            $this->check_exist_user_account($user_id);

            DB::beginTransaction();

            DB::table('users')
            ->where('users.id', $user_id)
            ->update([
            'deleted_at' => DB::raw('now()')
            ]);

            DB::commit();
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }
}
