<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Application;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
	use Application;

    public function __construct()
    {
        $this->middleware('auth');

        $this->module = 'home';
    }

    public function index()
    {
    	$data = ['module' => $this->module];

        /**

    	if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
    	{
    		$data = array_merge($data, [
    		'no_awaiting_applicant' => count($this->get_tec_application_by(null, null, 1 ,null, date('Y-d-m'))),
    		'no_approved_applicant' => count($this->get_tec_application_by(null, null, 2 ,null, date('Y-d-m'))),
    		'no_denied_applicant' => count($this->get_tec_application_by(null, null, 3 ,null, date('Y-d-m'))),
    		'no_application' => count($this->get_tec_application_by(null, null, null ,null, date('Y-d-m'))),
    		]);
    	}
    	elseif(Auth::user()->isProcessor())
    	{
    		$auth_processor_id = Auth::user()->id;

    		$data = array_merge($data, [
    		'no_awaiting_applicant' => count($this->get_tec_application_by(null, $auth_processor_id, 1 ,null, date('Y-d-m'))),
    		'no_approved_applicant' => count($this->get_tec_application_by(null, $auth_processor_id, 2 ,null, date('Y-d-m'))),
    		'no_denied_applicant' => count($this->get_tec_application_by(null, $auth_processor_id, 3 ,null, date('Y-d-m'))),
    		'no_application' => count($this->get_tec_application_by(null, $auth_processor_id, null ,null, date('Y-d-m'))),
    		]);
    	}
        **/

        return view('home.index', $data);
    }
}
