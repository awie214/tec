<?php

namespace App\Http\Controllers\WebController;

use App\Http\Controllers\Controller;
use App\Http\Traits\Application;
use App\Http\Traits\System_Config;
use DB;
//use Endroid\QrCode\QrCode;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Mail;
use PDF;
use Spatie\PdfToImage\Pdf as SpatiePdf;
//use Spatie\Browsershot\Browsershot;

use SimpleSoftwareIO\QrCode\Facades\QrCode; 

class ApplicationController extends Controller
{
    use Application, System_Config;

    public function __construct()
    {
    	$this->middleware('auth');

        $this->module = 'application';

        $this->application_url = url('/application');
        $this->based_url = url('/');

        $this->application_icon = 'mdi mdi-file';

        $this->middleware(function ($request, $next) {
            if(Auth::user()->isProcessor()) 
            {
                $this->application_url = url('/processor/application');
                $this->based_url = url('/processor');
            }
            elseif(Auth::user()->isRegularAdmin())
            {
                $this->application_url = url('/supervisor/application');
                $this->based_url = url('/supervisor');
            }
            elseif(Auth::user()->isTravelTaxUser())
            {
                $this->application_url = url('/travel_tax/application');
                $this->based_url = url('/travel_tax');
            }

            return $next($request);
        });
   	}

    public function application(request $request, $option)
    {
        try
        {
            $this->is_application_option_exist($option);

            $data = ['module' => $this->module, 'option' => $option, 'title_description' => $option.'_description', 'application_url' => $this->application_url, 'based_url' => $this->based_url, 'icon' => $this->application_icon];

            if($option == 'overview')
            {
                $view = 'application.overview.index';
            }
            elseif($option == 'tec_application')
            {   
                $view = 'application.tec_application.index';

                $data = array_merge($data, ['file' => 'application.tec_application.form', 'list_users' => $this->get_user_account(null, 3, 1), 'application_status' => $this->application_status, 'countries' => $this->get_countries_by(), 'list_processors' => $this->get_user_account(null, 2, 1), 'list_sections' => $this->get_section_by(), 'list_reason_denials' => $this->get_reason_denials_by(), 'list_type_applicants' => $this->get_type_applicants_by(), 'list_airlines' => $this->get_airlines_by(), 'list_supervisor' => $this->get_user_account(null, 1, 1)]); 
            }
            elseif($option == 'tr_application')
            {   
                $view = 'application.tr_application.index';

                $file = 'application.tr_application.form';

                if(Auth::user()->isTravelTaxUser()) $file = 'application.tr_application.form_travel_tax';
                
                $data = array_merge($data, ['file' => $file, 'list_users' => $this->get_user_account(null, 3, 1), 'application_status' => $this->application_status, 'countries' => $this->get_countries_by(), 'list_processors' => $this->get_user_account(null, 2, 1), 'list_sections' => $this->get_section_by(), 'list_reason_denials' => $this->get_reason_denials_by(), 'list_type_applicants' => $this->get_type_applicants_by(null, null, ['1']), 'list_airlines' => $this->get_airlines_by(), 'list_supervisor' => $this->get_user_account(null, 1, 1), 'travel_tax_refund_form' => $this->travel_tax_refund_form, 'type_applicant_refund_list' => $this->type_applicant_refund_list, 'get_refund_amount_by' => $this->get_refund_amount_by(), 'list_banks' => $this->get_bank_by(), 'list_refund_reason' => $this->reason_refund]); 
            }
            elseif($option == 'tec_dashboard')
            {
                $view = 'application.tec_dashboard.index';

                $data['icon'] = 'mdi mdi-chart';

                $filter_month = $request->input('filter_month');
                $filter_year = $request->input('filter_year');


                if($filter_month && $filter_year)
                {
                    $start_date = implode('-', [$filter_year, $filter_month, '01']);
                    $end_date = implode('-', [$filter_year, $filter_month, '31']);

                    $data = array_merge($data, [
                    'filter_month' => $filter_month,
                    'filter_year' => $filter_year,
                    'no_awaiting_applicant' => count($this->get_tec_application_by(null, null, 1 , $start_date, $end_date)),
                    'no_approved_applicant' => count($this->get_tec_application_by(null, null, 2 , $start_date, $end_date)),
                    'no_denied_applicant' => count($this->get_tec_application_by(null, null, 3 , $start_date, $end_date)),
                    'no_application' => count($this->get_tec_application_by(null, null, null , $start_date, $end_date)),
                    ]);
                }
                else
                {
                    $data = array_merge($data, [
                    'no_awaiting_applicant' => count($this->get_tec_application_by(null, null, 1 ,null, date('Y-m-31'))),
                    'no_approved_applicant' => count($this->get_tec_application_by(null, null, 2 ,null, date('Y-m-31'))),
                    'no_denied_applicant' => count($this->get_tec_application_by(null, null, 3 ,null, date('Y-m-31'))),
                    'no_application' => count($this->get_tec_application_by(null, null, null ,null, date('Y-m-31'))),
                    ]);
                }


                $data = array_merge($data, ['file' => 'application.tec_dashboard.form']);
            }
            elseif($option == 'tec_report')
            {
                $view = 'application.tec_report.index';

                $data = array_merge($data, ['file' => 'application.tec_report.form', 'tec_reports' => $this->tec_reports, 'application_status' => $this->application_status]); 
            }
            elseif($option == 'tr_dashboard')
            {
                $view = 'application.tr_dashboard.index';

                $data['icon'] = 'mdi mdi-chart';

                $filter_month = $request->input('filter_month');
                $filter_year = $request->input('filter_year');


                if($filter_month && $filter_year)
                {
                    $start_date = implode('-', [$filter_year, $filter_month, '01']);
                    $end_date = implode('-', [$filter_year, $filter_month, '31']);

                    $data = array_merge($data, [
                    'filter_month' => $filter_month,
                    'filter_year' => $filter_year,
                    'no_awaiting_applicant' => count($this->get_tr_application_by(null, null, 1 , $start_date, $end_date)),
                    'no_approved_applicant' => count($this->get_tr_application_by(null, null, 2 , $start_date, $end_date)),
                    'no_denied_applicant' => count($this->get_tr_application_by(null, null, 3 , $start_date, $end_date)),
                    'no_application' => count($this->get_tr_application_by(null, null, null , $start_date, $end_date)),
                    ]);
                }
                else
                {
                    $data = array_merge($data, [
                    'no_awaiting_applicant' => count($this->get_tr_application_by(null, null, 1 ,null, date('Y-m-31'))),
                    'no_approved_applicant' => count($this->get_tr_application_by(null, null, 2 ,null, date('Y-m-31'))),
                    'no_denied_applicant' => count($this->get_tr_application_by(null, null, 3 ,null, date('Y-m-31'))),
                    'no_application' => count($this->get_tr_application_by(null, null, null ,null, date('Y-m-31'))),
                    ]);
                }

                $data = array_merge($data, ['file' => 'application.tr_dashboard.form']);
            }
            elseif($option == 'tr_report')
            {
                dd('This page is under construction.');
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        
        return view($view, $data);
    }

    public function save_application(request $request, $option)
    {
        try 
        { 
            $this->is_application_option_exist($option);

            $LastID = null;

            $response = array();

            if($option == 'tec_application')
            {
                $data = array();

                $rules = [
                // 'applicant_name' => 'required|in:'.implode(',', $this->get_value_by($this->get_user_account(null, 3, 1), 'id')),
                'last_name' => 'required|max:50',
                'first_name' => 'required|max:50',
                'middle_name' => 'sometimes|nullable|max:50',
                'mobile_no' => 'required|max:50',
                'email_address' => 'required|email',
                'passport_no' => 'required|max:50',
                'ticket_booking_ref_no' => 'required|max:255',
                'date_ticket_issued' => 'required|date|date_format:Y-m-d',
                'country_designation' => 'required|in:'.implode(',', $this->get_value_by($this->get_countries_by(), 'id')),
                'date_validity' => 'required|date|date_format:Y-m-d',
                'date_flight' => 'required|date|date_format:Y-m-d',
                'airlines_name' => 'required|in:'.implode(',', $this->get_value_by($this->get_airlines_by(), 'id'))
                ];

                //$user_id = $request->get('applicant_name');

                if($id_picture_2x2 = Input::File('id_picture_2x2'))
                {
                    $rules['id_picture_2x2'] = 'sometimes|nullable|mimes:jpeg,png,jpg|max:5048';
                }

                if($passport_identification_page = Input::File('passport_identification_page'))
                {
                    $rules['passport_identification_page.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                {
                    $rules['ticket_booking_ref_no_.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($additional_file = Input::File('additional_file'))
                {
                    $rules['additional_file.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                $rules['type_applicant'] = 'required|in:'.implode(',', $this->get_value_by($this->get_type_applicants_by(), 'id'));

                if($file_0 = Input::File('file_0'))
                {
                    $rules['file_0.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_1 = Input::File('file_1'))
                {
                    $rules['file_1.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_2 = Input::File('file_2'))
                {
                    $rules['file_2.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_3 = Input::File('file_3'))
                {
                    $rules['file_3.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                $rules['status'] = 'required|in:'.implode(',', array_column($this->application_status, 'id'));

                if($status = $request->get('status'))
                {
                    if($status == '3') $rules['reason_denied'] = 'required|in:'.implode(',', $this->get_value_by($this->get_reason_denials_by(), 'id'));
                }

                if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin()) $rules['assigned_processor'] = 'required|in:'.implode(',', $this->get_value_by($this->get_user_account(null, 2, 1), 'id'));

                $this->validate_request($request->all(), $rules, ['passport_identification_page.*' => trans('page.passport_identification_page.*')]);


                /*********** File Upload **********/

                $file_token = Str::random(50);

                if($id_picture_2x2 = Input::File('id_picture_2x2'))
                {
                    $id_picture_2x2_file_extension = strtolower($id_picture_2x2->getClientOriginalExtension());

                    $id_picture_2x2_file_name = 'id_picture_2x2_'.date('Ymd').'_'.$file_token.'.'.$id_picture_2x2_file_extension;

                    $data['id_picture_2x2_fn'] = $id_picture_2x2_file_name;
                }

                $passport_identification_page_files = array();

                if($passport_identification_page = Input::File('passport_identification_page'))
                {
                    foreach($passport_identification_page as $key => $val)
                    {
                        $passport_identification_page_file_extension = strtolower($val->getClientOriginalExtension());

                        $passport_identification_page_file_name = 'passport_identification_page_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$passport_identification_page_file_extension;

                        $passport_identification_page_files[] = ['file' => Input::File('passport_identification_page.'.$key), 'name' => $passport_identification_page_file_name];
                    }
                }

                $ticket_booking_ref_no_files = array();

                if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                {
                    foreach($ticket_booking_ref_no_ as $key => $val)
                    {
                        $ticket_booking_ref_no_extension = strtolower($val->getClientOriginalExtension());

                        $ticket_booking_ref_no_file_name = 'ticket_booking_ref_no_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$ticket_booking_ref_no_extension;

                        $ticket_booking_ref_no_files[] = ['file' => Input::File('ticket_booking_ref_no_.'.$key), 'name' => $ticket_booking_ref_no_file_name];
                    }
                }

                $additional_files = array();

                if($additional_file = Input::File('additional_file'))
                {
                    foreach($additional_file as $key => $val)
                    {
                        $additional_file_extension = strtolower($val->getClientOriginalExtension());

                        $additional_file_name = 'additional_file_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$additional_file_extension;

                        $additional_files[] = ['file' => Input::File('additional_file.'.$key), 'name' => $additional_file_name];
                    }
                }

                /*********** Type of Applicant Uploads  **************/

                $files_0 = array();

                if($file_0 = Input::File('file_0'))
                {
                    foreach($file_0 as $key => $val)
                    {
                        $file_0_extension = strtolower($val->getClientOriginalExtension());

                        $file_0_name = 'file_0_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_0_extension;

                        $files_0[] = ['file' => Input::File('file_0.'.$key), 'name' => $file_0_name];
                    }
                }

                $files_1 = array();

                if($file_1 = Input::File('file_1'))
                {
                    foreach($file_1 as $key => $val)
                    {
                        $file_1_extension = strtolower($val->getClientOriginalExtension());

                        $file_1_name = 'file_1_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_1_extension;

                        $files_1[] = ['file' => Input::File('file_1.'.$key), 'name' => $file_1_name];
                    }
                }

                $files_2 = array();

                if($file_2 = Input::File('file_2'))
                {
                    foreach($file_2 as $key => $val)
                    {
                        $file_2_extension = strtolower($val->getClientOriginalExtension());

                        $file_2_name = 'file_2_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_2_extension;

                        $files_2[] = ['file' => Input::File('file_2.'.$key), 'name' => $file_2_name];
                    }
                }

                $files_3 = array();

                if($file_3 = Input::File('file_3'))
                {
                    foreach($file_3 as $key => $val)
                    {
                        $file_3_extension = strtolower($val->getClientOriginalExtension());

                        $file_3_name = 'file_3_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_3_extension;

                        $files_3[] = ['file' => Input::File('file_3.'.$key), 'name' => $file_3_name];
                    }
                }

                /************ Save to Database ************/

                $last_name = ucwords($request->get('last_name'));
                $first_name = ucwords($request->get('first_name'));
                $middle_name = ucwords($request->get('middle_name'));
                $country_id = $request->get('country_designation');
                $date_flight = $request->get('date_flight');

                $data = array_merge($data, [
                //'user_id' => $request->get('applicant_name'),
                'last_name' => $last_name,
                'first_name' => $first_name,
                'middle_name' => $middle_name,
                'email' => $request->get('email_address'),
                'mobile_no' => $request->get('mobile_no'),
                'passport_no' => $request->get('passport_no'),
                'ticket_no' => $request->get('ticket_booking_ref_no'),
                'date_ticket_issued' => $request->get('date_ticket_issued'),
                'country_id' => $country_id,
                'date_validity' => $request->get('date_validity'),
                'date_flight' => $date_flight,
                'applicant_type_id' => $request->get('type_applicant'),
                'airlines_id' => $request->get('airlines_name'),
                ]);

                if($status = $request->get('status'))
                {
                    $data['status'] = $status;
                    if($status == '3') $data['denial_id'] = $request->get('reason_denied');
                }

                if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                {
                    $data['assign_processor_id'] = $request->get('assigned_processor');

                    if(Auth::user()->isSuperAdmin())
                    {
                        $data['supervisor_id'] = $request->get('assigned_supervisor');
                    }
                }
                elseif(Auth::user()->isProcessor())
                {
                    $data['assign_processor_id'] = Auth::user()->id;
                }

                if($tec_id = $request->get('tec_id'))
                {
                    $data = array_merge($data, [
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);

                    // tec_application_upload

                    $tec_application = $this->check_exist_tec_application($tec_id);

                    // $user_id = $request->get('user_id');

                    // validate where in email address must not be existing as 'system' account

                    if($this->count_user_account(null, $request->get('email_address'), null, [0, 1, 2]) <> 0) throw new Exception(json_encode(['email_address' => ['The Email Address is already used by another user.']]));

                    if(Auth::user()->isProcessor() || Auth::user()->isRegularAdmin())
                    {
                        if($request->get('date_validity') < date('Y-m-d')) throw new Exception(json_encode(['date_validity' => ['TEC Validity must not be later than today.']]));
                    }

                    $this->check_exist_applied_tec_application(null, [$tec_id], implode('-', [$last_name, $first_name, $middle_name]), $country_id, $date_flight);

                    DB::beginTransaction();

                    $folder_path = storage_path('tieza/tec/attachment/'.$tec_id);

                    if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                    if($id_picture_2x2 = Input::File('id_picture_2x2')) $request->file('id_picture_2x2')->move($folder_path, $id_picture_2x2_file_name);

                    if($passport_identification_page = Input::File('passport_identification_page'))
                    {
                        foreach($passport_identification_page as $key => $val)
                        {
                            $val->move($folder_path, $passport_identification_page_files[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $tec_id,
                            'section_id' => 0,
                            'file_name' => $passport_identification_page_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                    {
                        foreach($ticket_booking_ref_no_ as $key => $val)
                        {
                            $val->move($folder_path, $ticket_booking_ref_no_files[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $tec_id,
                            'section_id' => 0,
                            'file_name' => $ticket_booking_ref_no_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }
                    
                    if($additional_file = Input::File('additional_file'))
                    {
                        foreach($additional_file as $key => $val)
                        {
                            $val->move($folder_path, $additional_files[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $tec_id,
                            'section_id' => 0,
                            'file_name' => $additional_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($tec_application->applicant_type_id <> $request->get('type_applicant'))
                    {   
                        DB::table('tec_application_upload as tau')
                        ->where('tau.tec_id', $tec_id)
                        ->where('section_id', $tec_application->applicant_type_id)
                        ->update([
                        'deleted_at' => DB::raw('now()')
                        ]);
                    }
                    else
                    {
                        if($file_0 = Input::File('file_0'))
                        {
                            foreach($file_0 as $key => $val)
                            {
                                $val->move($folder_path, $files_0[$key]['name']);  

                                DB::table('tec_application_upload')
                                ->insert([
                                'tec_id' => $tec_id,
                                'section_id' => $request->get('type_applicant'),
                                'file_name' => $files_0[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }

                        if($file_1 = Input::File('file_1'))
                        {
                            foreach($file_1 as $key => $val)
                            {
                                $val->move($folder_path, $files_1[$key]['name']);  

                                DB::table('tec_application_upload')
                                ->insert([
                                'tec_id' => $tec_id,
                                'section_id' => $request->get('type_applicant'),
                                'file_name' => $files_1[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }

                        if($file_2 = Input::File('file_2'))
                        {
                            foreach($file_2 as $key => $val)
                            {
                                $val->move($folder_path, $files_2[$key]['name']);  

                                DB::table('tec_application_upload')
                                ->insert([
                                'tec_id' => $tec_id,
                                'section_id' => $request->get('type_applicant'),
                                'file_name' => $files_2[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }

                        if($file_3 = Input::File('file_3'))
                        {
                            foreach($file_3 as $key => $val)
                            {
                                $val->move($folder_path, $files_3[$key]['name']);  

                                DB::table('tec_application_upload')
                                ->insert([
                                'tec_id' => $tec_id,
                                'section_id' => $request->get('type_applicant'),
                                'file_name' => $files_3[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }

                    DB::table('tec_application as ta')
                    ->where('ta.id', $tec_id)
                    ->update($data);

                    DB::commit();
                }
                else
                {
                    $this->check_user_level(Auth::user()->level, [0, 2]);

                    if($this->count_user_account(null, $request->get('email_address'), null, [0, 1, 2]) <> 0) throw new Exception(json_encode(['email_address' => ['The Email Address is already used by another user.']]));

                    if($request->get('date_validity') < date('Y-m-d')) throw new Exception(json_encode(['date_validity' => ['TEC Validity must not be later than today.']]));

                    $this->check_exist_applied_tec_application(null, null, implode('-', [$last_name, $first_name, $middle_name]), $country_id, $date_flight);

                    DB::beginTransaction();

                    $data = array_merge($data, [
                    'date_application' => DB::raw('now()'),
                    'created_by' => Auth::user()->id
                    ]);

                    DB::table('tec_application')
                    ->insert($data);

                    $LastID = DB::getPdo()->lastInsertId();

                    $tec_app = $this->check_exist_tec_application($LastID);

                    $folder_path = storage_path('tieza/tec/attachment/'.$LastID);

                    if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                    if($id_picture_2x2 = Input::File('id_picture_2x2')) $request->file('id_picture_2x2')->move($folder_path, $id_picture_2x2_file_name);

                    if($passport_identification_page = Input::File('passport_identification_page'))
                    {
                        foreach($passport_identification_page as $key => $val)
                        {
                            $val->move($folder_path, $passport_identification_page_files[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $passport_identification_page_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                    {
                        foreach($ticket_booking_ref_no_ as $key => $val)
                        {
                            $val->move($folder_path, $ticket_booking_ref_no_files[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $ticket_booking_ref_no_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($additional_file = Input::File('additional_file'))
                    {
                        foreach($additional_file as $key => $val)
                        {
                            $val->move($folder_path, $additional_files[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $additional_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_0 = Input::File('file_0'))
                    {
                        foreach($file_0 as $key => $val)
                        {
                            $val->move($folder_path, $files_0[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_0[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_1 = Input::File('file_1'))
                    {
                        foreach($file_1 as $key => $val)
                        {
                            $val->move($folder_path, $files_1[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_1[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_2 = Input::File('file_2'))
                    {
                        foreach($file_2 as $key => $val)
                        {
                            $val->move($folder_path, $files_2[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_2[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_3 = Input::File('file_3'))
                    {
                        foreach($file_3 as $key => $val)
                        {
                            $val->move($folder_path, $files_3[$key]['name']);  

                            DB::table('tec_application_upload')
                            ->insert([
                            'tec_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'file_name' => $files_3[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }   

                    DB::commit();

                    $response = [$this->success => $tec_app];
                }
            }
            elseif($option == 'tr_application')
            {
                $data = array();

                $rules = [
                'is_minor' => 'required|in:0,1',
                'last_name' => 'required|max:50',
                'first_name' => 'required|max:50',
                'middle_name' => 'sometimes|nullable|max:50'
                ];

                if($request->get('is_minor') == '1')
                {
                    $rules = array_merge($rules, [
                    'guardian_last_name' => 'required|max:50',
                    'guardian_first_name' => 'required|max:50',
                    'guardian_middle_name' => 'sometimes|nullable|max:50'
                    ]);
                }

                $rules = array_merge($rules, [
                'mobile_no' => 'required|max:50',
                'email_address' => 'required|email',
                'passport_no' => 'required|max:50',
                'ticket_booking_ref_no' => 'required|max:255',
                'date_ticket_issued' => 'required|date|date_format:Y-m-d',
                'country_designation' => 'required|in:'.implode(',', $this->get_value_by($this->get_countries_by(), 'id')),
                'date_flight' => 'required|date|date_format:Y-m-d',
                'airlines_name' => 'required|in:'.implode(',', $this->get_value_by($this->get_airlines_by(), 'id')),
                'amount_travel_tax_collected' => 'required',
                'list_type_applicant' => 'required|in:'.implode(',', array_column($this->type_applicant_refund_list, 'id'))
                ]);

                if($passport_identification_page = Input::File('passport_identification_page'))
                {
                    $rules['passport_identification_page.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                {
                    $rules['ticket_booking_ref_no_.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_tieza_official_receipt = Input::File('tieza_official_receipt'))
                {
                    $rules['tieza_official_receipt.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($additional_file = Input::File('additional_file'))
                {
                    $rules['additional_file.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                $rules['type_applicant'] = 'required|in:'.implode(',', $this->get_value_by($this->get_type_applicants_by(null, $request->get('list_type_applicant')), 'id'));

                if($file_0 = Input::File('file_0'))
                {
                    $rules['file_0.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_1 = Input::File('file_1'))
                {
                    $rules['file_1.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_2 = Input::File('file_2'))
                {
                    $rules['file_2.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_3 = Input::File('file_3'))
                {
                    $rules['file_3.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                if($file_4 = Input::File('file_4'))
                {
                    $rules['file_4.*'] = 'sometimes|nullable|mimes:jpeg,png,jpg,pdf|max:5048';
                }

                $rules['form_travel_tax_refund'] = 'required|in:'.implode(',', array_column($this->travel_tax_refund_form, 'id'));

                if($request->get('form_travel_tax_refund') == '2')
                {
                    $rules = array_merge($rules, [
                    'bank_name' => 'required|in:'.implode(',', $this->get_value_by($this->get_bank_by(), 'id')),
                    'bank_account_name' => 'required|max:255',
                    'bank_account_no' => 'required|max:255',
                    'refund_amount' => 'required',
                    'reason_refund' => 'required|in:'.implode(',', array_column($this->reason_refund, 'id')),
                    ]);

                    if($request->get('reason_refund') == '6') $rules = array_merge($rules, [
                    'other_reasons' => 'required|max:50',
                    ]);
                }

                $rules = array_merge($rules, [
                'tieza_official_receipt_no' => 'required',
                'payment_date' => 'required',
                ]);

                $rules['status'] = 'required|in:'.implode(',', array_column($this->application_status, 'id'));

                if($status = $request->get('status'))
                {
                    if($status == '3') $rules['reason_denied'] = 'required|in:'.implode(',', $this->get_value_by($this->get_reason_denials_by(), 'id'));
                }

                if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin()) $rules['assigned_processor'] = 'required|in:'.implode(',', $this->get_value_by($this->get_user_account(null, 2, 1), 'id'));

                $this->validate_request($request->all(), $rules, ['passport_identification_page.*' => trans('page.passport_identification_page.*')]);


                /*********** File Upload **********/

                $file_token = Str::random(50);

                $passport_identification_page_files = array();

                if($passport_identification_page = Input::File('passport_identification_page'))
                {
                    foreach($passport_identification_page as $key => $val)
                    {
                        $passport_identification_page_file_extension = strtolower($val->getClientOriginalExtension());

                        $passport_identification_page_file_name = 'passport_identification_page_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$passport_identification_page_file_extension;

                        $passport_identification_page_files[] = ['file' => Input::File('passport_identification_page.'.$key), 'name' => $passport_identification_page_file_name];
                    }
                }

                $ticket_booking_ref_no_files = array();

                if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                {
                    foreach($ticket_booking_ref_no_ as $key => $val)
                    {
                        $ticket_booking_ref_no_extension = strtolower($val->getClientOriginalExtension());

                        $ticket_booking_ref_no_file_name = 'ticket_booking_ref_no_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$ticket_booking_ref_no_extension;

                        $ticket_booking_ref_no_files[] = ['file' => Input::File('ticket_booking_ref_no_.'.$key), 'name' => $ticket_booking_ref_no_file_name];
                    }
                }

                $tieza_official_receipt_files = array();

                if($tieza_official_receipt = Input::File('tieza_official_receipt'))
                {
                    foreach($tieza_official_receipt as $key => $val)
                    {
                        $tieza_official_receipt_extension = strtolower($val->getClientOriginalExtension());

                        $tieza_official_receipt_file_name = 'tieza_official_receipt_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$tieza_official_receipt_extension;

                        $tieza_official_receipt_files[] = ['file' => Input::File('tieza_official_receipt_.'.$key), 'name' => $tieza_official_receipt_file_name];
                    }
                }

                $additional_files = array();

                if($additional_file = Input::File('additional_file'))
                {
                    foreach($additional_file as $key => $val)
                    {
                        $additional_file_extension = strtolower($val->getClientOriginalExtension());

                        $additional_file_name = 'additional_file_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$additional_file_extension;

                        $additional_files[] = ['file' => Input::File('additional_file.'.$key), 'name' => $additional_file_name];
                    }
                }

                /*********** Type of Applicant Uploads  **************/

                $files_0 = array();

                if($file_0 = Input::File('file_0'))
                {
                    foreach($file_0 as $key => $val)
                    {
                        $file_0_extension = strtolower($val->getClientOriginalExtension());

                        $file_0_name = 'file_0_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_0_extension;

                        $files_0[] = ['file' => Input::File('file_0.'.$key), 'name' => $file_0_name];
                    }
                }

                $files_1 = array();

                if($file_1 = Input::File('file_1'))
                {
                    foreach($file_1 as $key => $val)
                    {
                        $file_1_extension = strtolower($val->getClientOriginalExtension());

                        $file_1_name = 'file_1_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_1_extension;

                        $files_1[] = ['file' => Input::File('file_1.'.$key), 'name' => $file_1_name];
                    }
                }

                $files_2 = array();

                if($file_2 = Input::File('file_2'))
                {
                    foreach($file_2 as $key => $val)
                    {
                        $file_2_extension = strtolower($val->getClientOriginalExtension());

                        $file_2_name = 'file_2_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_2_extension;

                        $files_2[] = ['file' => Input::File('file_2.'.$key), 'name' => $file_2_name];
                    }
                }

                $files_3 = array();

                if($file_3 = Input::File('file_3'))
                {
                    foreach($file_3 as $key => $val)
                    {
                        $file_3_extension = strtolower($val->getClientOriginalExtension());

                        $file_3_name = 'file_3_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_3_extension;

                        $files_3[] = ['file' => Input::File('file_3.'.$key), 'name' => $file_3_name];
                    }
                }

                $files_4 = array();

                if($file_4 = Input::File('file_4'))
                {
                    foreach($file_4 as $key => $val)
                    {
                        $file_4_extension = strtolower($val->getClientOriginalExtension());

                        $file_4_name = 'file_4_'.$key.'_'.date('Ymd').'_'.$file_token.'.'.$file_4_extension;

                        $files_4[] = ['file' => Input::File('file_4.'.$key), 'name' => $file_4_name];
                    }
                }
                
                /************ Save to Database ************/

                $isMinor = $request->get('is_minor');
                $last_name = ucwords($request->get('last_name'));
                $first_name = ucwords($request->get('first_name'));
                $middle_name = ucwords($request->get('middle_name'));

                $guardian_last_name = ucwords($request->get('guardian_last_name'));
                $guardian_first_name = ucwords($request->get('guardian_first_name'));
                $guardian_middle_name = ucwords($request->get('guardian_middle_name'));

                $form_travel_tax_refund = $request->get('form_travel_tax_refund');

                $country_id = $request->get('country_designation');
                $date_flight = $request->get('date_flight');

                $data = array_merge($data, [
                'is_minor' => $isMinor,
                'last_name' => $last_name,
                'first_name' => $first_name,
                'middle_name' => $middle_name,
                'email' => $request->get('email_address'),
                'mobile_no' => $request->get('mobile_no'),
                'passport_no' => $request->get('passport_no'),
                'ticket_no' => $request->get('ticket_booking_ref_no'),
                'date_ticket_issued' => $request->get('date_ticket_issued'),
                'country_id' => $country_id,
                'date_flight' => $date_flight,
                'airlines_id' => $request->get('airlines_name'),
                'travel_tax_amount' => $request->get('amount_travel_tax_collected'),
                'section_id' => $request->get('list_type_applicant'),
                'applicant_type_id' => $request->get('type_applicant'),
                'type_travel_tax_refund' => $form_travel_tax_refund,
                'tieza_official_receipt_no' => $request->get('tieza_official_receipt_no'),
                'payment_date' => $request->get('payment_date'),
                ]);

                if($isMinor == '1')
                {
                    $data = array_merge($data, [
                    'guardian_last_name' => $guardian_last_name,
                    'guardian_first_name' => $guardian_first_name,
                    'guardian_middle_name' => $guardian_middle_name
                    ]);
                }

                if($form_travel_tax_refund == '2')
                {
                    $data = array_merge($data, [
                    'bank_id' => $request->get('bank_name'),
                    'bank_account_name' => $request->get('bank_account_name'),
                    'bank_account_number' => $request->get('bank_account_no'),
                    'refund_amount' => $request->get('refund_amount'),
                    'reason_refund' => $request->get('reason_refund'),
                    'reason_refund_others' => null
                    ]);

                    if($request->get('reason_refund') == 6) $data['reason_refund_others'] = $request->get('other_reasons');
                }

                if($status = $request->get('status'))
                {
                    $data['status'] = $status;
                    if($status == '3') $data['denial_id'] = $request->get('reason_denied');
                }

                if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                {
                    $data['assign_processor_id'] = $request->get('assigned_processor');

                    if(Auth::user()->isSuperAdmin())
                    {
                        $data['supervisor_id'] = $request->get('assigned_supervisor');
                    }
                }
                elseif(Auth::user()->isProcessor())
                {
                    $data['assign_processor_id'] = Auth::user()->id;
                }

                if($tr_id = $request->get('tr_id'))
                {
                    $data = array_merge($data, [
                    'updated_by' => Auth::user()->id,
                    'updated_at' => DB::raw('now()')
                    ]);

                    $tr_application = $this->check_exist_tr_application($tr_id);

                    // validate where in email address must not be existing as 'system' account

                    if($this->count_user_account(null, $request->get('email_address'), null, [0, 1, 2]) <> 0) throw new Exception(json_encode(['email_address' => ['The Email Address is already used by another user.']]));

                    $this->check_exist_applied_tr_application(null, [$tr_id], implode('-', [$last_name, $first_name, $middle_name]), $country_id, $date_flight);

                    DB::beginTransaction();

                    $folder_path = storage_path('tieza/tr/attachment/'.$tr_id);

                    if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                    if($passport_identification_page = Input::File('passport_identification_page'))
                    {
                        foreach($passport_identification_page as $key => $val)
                        {
                            $val->move($folder_path, $passport_identification_page_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $tr_id,
                            'section_id' => 0,
                            'file_name' => $passport_identification_page_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                    {
                        foreach($ticket_booking_ref_no_ as $key => $val)
                        {
                            $val->move($folder_path, $ticket_booking_ref_no_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $tr_id,
                            'section_id' => 0,
                            'file_name' => $ticket_booking_ref_no_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($tieza_official_receipt = Input::File('tieza_official_receipt'))
                    {
                        foreach($tieza_official_receipt as $key => $val)
                        {
                            $val->move($folder_path, $tieza_official_receipt_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $tr_id,
                            'section_id' => 0,
                            'file_name' => $tieza_official_receipt_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }
                    
                    if($additional_file = Input::File('additional_file'))
                    {
                        foreach($additional_file as $key => $val)
                        {
                            $val->move($folder_path, $additional_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $tr_id,
                            'section_id' => 0,
                            'file_name' => $additional_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($tr_application->applicant_type_id <> $request->get('type_applicant'))
                    {   
                        DB::table('tr_application_upload as tau')
                        ->where('tau.tr_id', $tr_id)
                        ->where('section_id', $tr_application->applicant_type_id)
                        ->update([
                        'deleted_at' => DB::raw('now()')
                        ]);
                    }
                    else
                    {
                        if($file_0 = Input::File('file_0'))
                        {
                            foreach($file_0 as $key => $val)
                            {
                                $val->move($folder_path, $files_0[$key]['name']);  

                                DB::table('tr_application_upload')
                                ->insert([
                                'tr_id' => $tr_id,
                                'section_id' => $request->get('type_applicant'),
                                'section_upload_id' => $request->get('file_0_id'),
                                'file_name' => $files_0[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }

                        if($file_1 = Input::File('file_1'))
                        {
                            foreach($file_1 as $key => $val)
                            {
                                $val->move($folder_path, $files_1[$key]['name']);  

                                DB::table('tr_application_upload')
                                ->insert([
                                'tr_id' => $tr_id,
                                'section_id' => $request->get('type_applicant'),
                                'section_upload_id' => $request->get('file_1_id'),
                                'file_name' => $files_1[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }

                        if($file_2 = Input::File('file_2'))
                        {
                            foreach($file_2 as $key => $val)
                            {
                                $val->move($folder_path, $files_2[$key]['name']);  

                                DB::table('tr_application_upload')
                                ->insert([
                                'tr_id' => $tr_id,
                                'section_id' => $request->get('type_applicant'),
                                'section_upload_id' => $request->get('file_2_id'),
                                'file_name' => $files_2[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }

                        if($file_3 = Input::File('file_3'))
                        {
                            foreach($file_3 as $key => $val)
                            {
                                $val->move($folder_path, $files_3[$key]['name']);  

                                DB::table('tr_application_upload')
                                ->insert([
                                'tr_id' => $tr_id,
                                'section_id' => $request->get('type_applicant'),
                                'section_upload_id' => $request->get('file_3_id'),
                                'file_name' => $files_3[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }

                        if($file_4 = Input::File('file_4'))
                        {
                            foreach($file_4 as $key => $val)
                            {
                                $val->move($folder_path, $files_4[$key]['name']);  

                                DB::table('tr_application_upload')
                                ->insert([
                                'tr_id' => $tr_id,
                                'section_id' => $request->get('type_applicant'),
                                'section_upload_id' => $request->get('file_4_id'),
                                'file_name' => $files_4[$key]['name'],
                                'upload_by' => Auth::user()->id
                                ]);
                            }
                        }
                    }

                    DB::table('tr_application as ta')
                    ->where('ta.id', $tr_id)
                    ->update($data);

                    DB::commit();
                }
                else
                {
                    $this->check_user_level(Auth::user()->level, [0, 2]);

                    if($this->count_user_account(null, $request->get('email_address'), null, [0, 1, 2]) <> 0) throw new Exception(json_encode(['email_address' => ['The Email Address is already used by another user.']]));

                    $this->check_exist_applied_tr_application(null, null, implode('-', [$last_name, $first_name, $middle_name]), $country_id, $date_flight);

                    DB::beginTransaction();

                    $data = array_merge($data, [
                    'date_application' => DB::raw('now()'),
                    'created_by' => Auth::user()->id
                    ]);

                    DB::table('tr_application')
                    ->insert($data);

                    $LastID = DB::getPdo()->lastInsertId();

                    $tr_app = $this->check_exist_tr_application($LastID);

                    $folder_path = storage_path('tieza/tr/attachment/'.$LastID);

                    if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

                    if($passport_identification_page = Input::File('passport_identification_page'))
                    {
                        foreach($passport_identification_page as $key => $val)
                        {
                            $val->move($folder_path, $passport_identification_page_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $passport_identification_page_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($ticket_booking_ref_no_ = Input::File('ticket_booking_ref_no_'))
                    {
                        foreach($ticket_booking_ref_no_ as $key => $val)
                        {
                            $val->move($folder_path, $ticket_booking_ref_no_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $ticket_booking_ref_no_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($tieza_official_receipt = Input::File('tieza_official_receipt'))
                    {
                        foreach($tieza_official_receipt as $key => $val)
                        {
                            $val->move($folder_path, $tieza_official_receipt_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $tieza_official_receipt_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($additional_file = Input::File('additional_file'))
                    {
                        foreach($additional_file as $key => $val)
                        {
                            $val->move($folder_path, $additional_files[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => 0,
                            'file_name' => $additional_files[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_0 = Input::File('file_0'))
                    {
                        foreach($file_0 as $key => $val)
                        {
                            $val->move($folder_path, $files_0[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'section_upload_id' => $request->get('file_0_id'),
                            'file_name' => $files_0[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_1 = Input::File('file_1'))
                    {
                        foreach($file_1 as $key => $val)
                        {
                            $val->move($folder_path, $files_1[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'section_upload_id' => $request->get('file_1_id'),
                            'file_name' => $files_1[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_2 = Input::File('file_2'))
                    {
                        foreach($file_2 as $key => $val)
                        {
                            $val->move($folder_path, $files_2[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'section_upload_id' => $request->get('file_2_id'),
                            'file_name' => $files_2[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_3 = Input::File('file_3'))
                    {
                        foreach($file_3 as $key => $val)
                        {
                            $val->move($folder_path, $files_3[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'section_upload_id' => $request->get('file_3_id'),
                            'file_name' => $files_3[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    if($file_4 = Input::File('file_4'))
                    {
                        foreach($file_4 as $key => $val)
                        {
                            $val->move($folder_path, $files_4[$key]['name']);  

                            DB::table('tr_application_upload')
                            ->insert([
                            'tr_id' => $LastID,
                            'section_id' => $request->get('type_applicant'),
                            'section_upload_id' => $request->get('file_4_id'),
                            'file_name' => $files_4[$key]['name'],
                            'upload_by' => Auth::user()->id
                            ]);
                        }
                    }

                    DB::commit();

                    $response = [$this->success => $tr_app];
                }
            }
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        if($LastID) return response($response, 201); 

        return response($this->success, 201); 
    }

    public function delete_application(request $request, $option)
    {
        try 
        { 
            $this->is_application_option_exist($option);

            if($option == 'tec_application')
            {
                $tec_id = $request->get('tec_id');

                $this->check_exist_tec_application($tec_id);

                DB::beginTransaction();

                DB::table('tec_application as ta')
                ->where('ta.id', $tec_id)
                ->update([
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
            elseif($option == 'tr_application')
            {
                $tr_id = $request->get('tr_id');

                $this->check_exist_tr_application($tr_id);

                DB::beginTransaction();

                DB::table('tr_application as tr')
                ->where('tr.id', $tr_id)
                ->update([
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }

    public function print_application(request $request, $option)
    {
        try
        {
            $this->is_application_option_exist($option);

           $data = ['module' => $this->module, 'option' => $option, 'application_url' => $this->application_url, 'based_url' => $this->based_url, 'icon' => $this->application_icon];

            $applications = array();

            if($option == 'tec_report')
            {
                $report_name = $request->get('report_name');
                $month = $request->get('month');
                $year = $request->get('year');
                $status = $request->get('status');
                $completed = $request->get('completed');

                if(!$report_name) throw new Exception('Please Select Report.');

                $this->is_application_report_exist($report_name);

                $view = 'application.tec_report.report';

                $chunk_by = 20;

                if(in_array($report_name, ['airlines_passenger']))
                {   
                    $query = DB::table('tec_application as ta')
                    ->leftjoin('airlines as al', 'al.id', 'ta.airlines_id')
                    ->leftjoin('reason_denials as rd', 'rd.id', 'ta.denial_id')
                    ->leftjoin('sections as sec', 'sec.id', 'ta.applicant_type_id')
                    ->select('ta.*', 'al.name as airline_name', DB::raw('CONCAT(ta.last_name,", ",ta.first_name," ", IFNULL(SUBSTRING(ta.middle_name, 1, 1), ""), IF(ISNULL(SUBSTRING(ta.middle_name, 1, 1)) or ta.middle_name != "", ".", "")) AS full_name'), 'rd.name as denied_desc', 'sec.code as sec_code')
                    ->where('ta.deleted_at', null);

                    if($month) 
                    {
                        $query->whereMonth('ta.date_application', $month);
                        $data['month'] = $month;
                    }

                    if($year) 
                    {
                        $query->whereYear('ta.date_application', $year);
                        $data['year'] = $year;
                    }

                    if($status) 
                    {
                        $query->where('ta.status', $status);
                        $data['status'] = $status;

                        if($status == '3') $chunk_by = 15;
                    }  

                    if(in_array($completed, ["0", "1"]))
                    {
                        if($completed == 1) 
                        {
                            $query->wherenotnull('ta.generate_to_email'); 
                        }
                        elseif($completed == 0)
                        {
                            $query->where('ta.generate_to_email', null); 
                        }
                    }

                    if($report_name)
                    {
                        if($report_name == 'airlines_passenger')
                        {
                            if($airlines = $request->get('airlines')) $query->where('ta.airlines_id', $airlines);

                            $data = array_merge($data, ['report_name' => $report_name]);
                        }

                        $applications = $query->get()->toarray(); 
                        $applications_chunk = array_chunk($applications, $chunk_by);
                        $count_applications_chunk = count($applications_chunk);
                    }
                }  
                elseif($report_name == 'summary_section_tec')
                {
                    $sections = DB::table('sections as sec')->where('sec.section_type', 1)->get();
                    $data = array_merge($data, ['report_name' => $report_name]);

                    $total_count = 0;

                    foreach($sections as $key => $val)
                    {
                        $query = DB::table('tec_application as ta')
                        ->where('ta.applicant_type_id', $val->id)
                        ->where('ta.deleted_at', null);

                        if($month) 
                        {
                            $query->whereMonth('ta.date_application', $month);
                            $data['month'] = $month;
                        }

                        if($year) 
                        {
                            $query->whereYear('ta.date_application', $year);
                            $data['year'] = $year;
                        }

                        if($status) 
                        {
                            $query->where('ta.status', $status);
                            $data['status'] = $status;
                        }  

                        if(in_array($completed, ["0", "1"]))
                        {
                            if($completed == 1) 
                            {
                                $query->wherenotnull('ta.generate_to_email'); 
                            }
                            elseif($completed == 0)
                            {
                                $query->where('ta.generate_to_email', null); 
                            }
                        }

                        $val->count = $count_application = $query->count();

                        $total_count += $val->count;
                    }
            
                    $applications = $sections->toarray(); 
                    $applications_chunk = array_chunk($applications, 13);
                    $count_applications_chunk = count($applications_chunk);

                    $data = array_merge($data, ['total_count' => $total_count]);
                } 

                if(count($applications) == 0) return back()->with('error', 'No Record found.'); 

                $data = array_merge($data, ['applications' => $applications, 'application_status' => $this->application_status, 'applications_chunk' => $applications_chunk, 'count_applications_chunk' => $count_applications_chunk]); 
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }
        
        return view($view, $data);
    }

    public function generate_application(request $request, $option)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);

        try
        {
            $this->is_application_option_exist($option);
        
            $data = ['module' => $this->module, 'option' => $option, 'application_url' => $this->application_url, 'based_url' => $this->based_url, 'icon' => $this->application_icon];

            if($option == 'tec_application')
            {   
                $tec_id = $request->get('tec_id');
                $status = $request->get('status');
                $supervisor = $request->get('supervisor');

                $this->check_user_level(Auth::user()->level, [0, 1]);

                $tec_application = $this->check_exist_tec_application($tec_id);

                if($status == 2)
                {
                    $view = 'mail.tec_certificate_pdf';

                    $file_name = $tec_application->tec_id.'_'.date('mdY');
                }
                elseif($status == 3)
                {
                    $view = 'mail.tec_denial_pdf';

                    $file_name = $tec_application->app_id.'_'.date('mdY');
                }      

                $update_data = [
                'generate_to_email' => $file_name.'.png',
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                if(Auth::user()->isSuperAdmin())
                {
                    $update_data['supervisor_id'] = $supervisor;
                }
                elseif(Auth::user()->isRegularAdmin()) 
                {
                    $update_data['supervisor_id'] = Auth::user()->id;
                }

                DB::beginTransaction();

                DB::table('tec_application as ta')
                ->where('ta.id', $tec_id)
                ->update($update_data);

                DB::commit(); 
            }
            elseif($option == 'tr_application')
            {
                $tr_id = $request->get('tr_id');
                $status = $request->get('status');
                $supervisor = $request->get('supervisor');

                $this->check_user_level(Auth::user()->level, [0, 1]);

                $tr_application = $this->check_exist_tr_application($tr_id);

                $update_data = array(
                'acknowledge_receipt_fn' => null,
                'form_353_airline_fn' => null,
                'generate_to_email' => null,
                'is_complete' => 1,
                );

                if($status == 2)
                {
                    $acknowledge_receipt = 'mail.tr_acknowledge_receipt';
                    $acknowledge_receipt_file_name = $tr_application->tr_id.'_acknowledge_receipt_'.date('mdY');

                    $form_353_a = 'mail.tr_form_353_a';
                    $form_353_a_file_name = $tr_application->tr_id.'_form_353_a_'.date('mdY');

                    $update_data['acknowledge_receipt_fn'] = $acknowledge_receipt_file_name.'.png';
                    $update_data['form_353_airline_fn'] = $form_353_a_file_name.'.png';
                }
                elseif($status == 3)
                {
                    $view = 'mail.tr_denial_pdf';
                    $file_name = $tr_application->app_id.'_'.date('mdY');

                    $update_data['generate_to_email'] = $file_name.'.png';
                }      

                $update_data = [
                'updated_by' => Auth::user()->id,
                'updated_at' => DB::raw('now()')
                ];

                if(Auth::user()->isSuperAdmin())
                {
                    $update_data['supervisor_id'] = $supervisor;
                }
                elseif(Auth::user()->isRegularAdmin()) 
                {
                    $update_data['supervisor_id'] = Auth::user()->id;
                }

                DB::beginTransaction();

                DB::table('tr_application as ta')
                ->where('ta.id', $tr_id)
                ->update($update_data);

                DB::commit();
            }
        }
        catch(Exception $e)
        {
            $request->session()->flash('error', $e->getMessage());

            return back();
        }

        /***********************  Additional method **********************************/

        if($option == 'tec_application')
        {
            /******************** QR CODE GENERATOR *************************/

            if($status == 2)
            {
                $tec_date = date("m-d-Y", strtotime($tec_application->date_application));
                $tec_validity = date("m-d-Y", strtotime($tec_application->date_validity));

                $qrcode_msg = 'Name of Applicant: '.$tec_application->ffull_name."\r\n".'Passport No.: '.$tec_application->passport_no."\r\n".'Ticket No.: '.$tec_application->ticket_no."\r\n".'TEC No.: '.$tec_application->tec_id."\r\n".'TEC Date Issued: '.$tec_date."\r\n".'TEC Validity: '.$tec_validity;

                $pngImage = QrCode::format('png')->merge('img/tieza-logo.png', 0.3, true)
                ->size(600)->errorCorrection('H')
                ->generate($qrcode_msg);

                $output_file = '/tieza/tec/qr-code/'.$tec_application->tec_id. '_qrcode.png';
                Storage::disk('storage')->put($output_file, $pngImage);
            }

            /******************** QR CODE GENERATOR *************************/
            
            $tec_application = $this->check_exist_tec_application($tec_id);

            $data = array_merge($data, ['ta' => $tec_application]); 

            $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(false);

            $folder_path = storage_path('tieza/tec/certificate/'.$tec_application->id);

            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

            $pdf->save($folder_path.'/'.$file_name.'.pdf');

            //return $pdf->stream($file_name, array("Attachment" => 0));

            $pdf = new SpatiePdf($folder_path.'/'.$file_name.'.pdf');

            $pdf->saveImage($folder_path.'/'.$file_name.'.png');

            $generate_image = $folder_path.'/'.$file_name.'.png';

            // client
            $to_email = $tec_application->email;

            // testing
            // $to_email = 'angiesimbulan.tieza@gmail.com';
            // $to_email = 'xybrielmiranda.tieza@gmail.com';

            Mail::send('vendor.mail', array(), function($message) use ($to_email, $generate_image){
                $message->from(env('MAIL_USERNAME'), env('APP_NAME')); 
                $message->to($to_email)->subject('TEC Evaluation');
                $message->attach($generate_image, ['as' => 'TEC Evaluation']);
            });

            $response = ['file_name' => $file_name.'.png', 'tec_id' => $tec_application->id];
        }
        elseif($option == 'tr_application')
        {
            $tr_application = $this->check_exist_tr_application($tr_id);

            $data = array_merge($data, [
            'ta' => $tr_application,
            'count_passport' => $this->count_attached_file_by('passport', $tr_id),
            'count_airline' => $this->count_attached_file_by('airline', $tr_id),
            'count_tieza_official' => $this->count_attached_file_by('tieza_official', $tr_id),
            'count_marriage' => $this->count_attached_file_by(3, $tr_id),
            'count_birth_certificate' => $this->count_attached_file_by(4, $tr_id),
            'count_ofw_certificate' => $this->count_attached_file_by(5, $tr_id),
            'count_proof_permanent' => $this->count_attached_file_by(6, $tr_id),
            'count_travel_authority' => $this->count_attached_file_by(7, $tr_id),
            'count_passport_applicable_arrival' => $this->count_attached_file_by(8, $tr_id),
            'count_letter_request' => $this->count_attached_file_by(9, $tr_id),
            'count_affidavit_name_descrepancy' => $this->count_attached_file_by(10, $tr_id),
            ]); 

            $folder_path = storage_path('tieza/tr/certificate/'.$tr_application->id);

            if(!file_exists($folder_path)) File::makeDirectory($folder_path, $mode = 0777, true, true);

            if($status == '2')
            {
                /*************************  generate ar & form 353 ******************************/
               
                $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($acknowledge_receipt, $data)->setWarnings(false);

                $pdf->save($folder_path.'/'.$acknowledge_receipt_file_name.'.pdf');

            
                $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($form_353_a, $data)->setWarnings(false);

                $pdf->save($folder_path.'/'.$form_353_a_file_name.'.pdf');
            }
            /**
            elseif($status == '3')
            {
                $pdf = PDF::setPaper('a4', 'portrait')->setOptions(['dpi' => 100, 'defaultFont' => 'cambria', 'isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true, 'isJavascriptEnabled' => true])->loadView($view, $data)->setWarnings(false);

                $pdf->save($folder_path.'/'.$file_name.'.pdf');

                $pdf = new SpatiePdf($folder_path.'/'.$file_name.'.pdf');

                $pdf->saveImage($folder_path.'/'.$file_name.'.png');

                $generate_image = $folder_path.'/'.$file_name.'.png';
            }

            

            // client
            $to_email = $tec_application->email;

            // testing
            // $to_email = 'angiesimbulan.tieza@gmail.com';
            // $to_email = 'xybrielmiranda.tieza@gmail.com';

            Mail::send('vendor.mail', array(), function($message) use ($to_email, $generate_image){
                $message->from(env('MAIL_USERNAME'), env('APP_NAME')); 
                $message->to($to_email)->subject('TEC Evaluation');
                $message->attach($generate_image, ['as' => 'TEC Evaluation']);
            });

            **/

            $response = ['acknowledge_receipt_file_name' => $acknowledge_receipt_file_name.'.pdf', 'form_353_a_file_name' => $form_353_a_file_name.'.pdf', 'tr_id' => $tr_application->id];
        }

        return response($response, 201); 
    }

    public function remove_application_attachment(request $request, $option)
    {
        try 
        { 
            $this->is_application_option_exist($option);

            if($option == 'tec_application')
            {
                $tau_id = $request->get('tau_id');
                
                DB::beginTransaction();

                DB::table('tec_application_upload as tau')
                ->where('tau.id', $tau_id)
                ->update([
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
            elseif($option == 'tr_application')
            {
                $tau_id = $request->get('tau_id');

                DB::beginTransaction();

                DB::table('tr_application_upload as tau')
                ->where('tau.id', $tau_id)
                ->update([
                'deleted_at' => DB::raw('now()')
                ]);

                DB::commit();
            }
        }
        catch(Exception $e) 
        {
            DB::rollback();

            $data = json_decode($e->getMessage(), true);

            if(!is_array($data)) return response(['errors' => $e->getMessage()], 422);
            
            return response(['errors' => $data], 422);
        }

        return response($this->success, 201); 
    }
}
