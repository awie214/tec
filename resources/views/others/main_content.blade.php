	   
       	<div class="row">
            <div class="col-12">
                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header card-header-divider">
                        <div class="row">
                            <div class="col-6">
                                @if(isset($has_icon)) 
                                    <span class="{{ $has_icon }}"></span>
                                @endif
                                @if(isset($title))
                                    <b>{{ __('page.'.$title) }}</b>
                                @endif
                                @if(isset($title_description))
                                    <span class="card-subtitle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ __('page.'.$title_description) }}</span>
                                @endif
                            </div>
                            <div class="col-6 text-right">
                                @if(isset($add_url))
                                    @include('others.add_button', ['add_url' => $add_url])
                                @endif 
                                @if(isset($delete_url))
                                    @include('others.delete_button', ['delete_url' => $delete_url])
                                @endif 
                                @if(isset($print_url))
                                    @include('others.print_button', ['add_url' => $print_url])
                                @endif 
                                @if(isset($print_dtr))
                                    @include('others.dtr_button', ['add_url' => $print_dtr])
                                @endif
                                @if(isset($recompute_url))
                                    @include('others.recompute_button', ['add_url' => $recompute_url])
                                @endif 
                                @if(isset($isSave) && !isset($has_footer))
                                    @include('others.save_button_up', ['cancel_url' => $cancel_url])
                                @endif
                                @if(isset($isCancel))
                                    @include('others.cancel_button', ['cancel_url' => $cancel_url])
                                @endif
                                
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @if(isset($has_frm))
                            @include('others.form_request', ['frm_method' => $frm_method, 'frm_action' => $frm_action, 'frm_id' => $frm_id])
                        @endif

                    	@if(isset($has_file)) 
                    		@include($has_file)
                    	@endif
                    </div>

                    @if(isset($has_footer))
                        <div class="card-divider"></div>
                    	<div class="card-footer">
                        <div class="row">
                            <div class="col-md-6">
                                {{-- Customize btn for the TEC & TR Application --}}
                                @if($option == 'tec_application')
                                    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                                        <div id="only_approved">
                                            <a href="javascript:void(0);" class="btn btn-space btn-secondary" title="{{ __('page.generate_certificate') }}" id="generate_certificate"><span class="icon icon-left mdi mdi-print"></span>&nbsp;{{ __('page.generate_certificate') }}</a>
                                        </div>

                                        <div id="only_denied">
                                            <a href="javascript:void(0);" class="btn btn-space btn-secondary" title="{{ __('page.generate_denial_letter') }}" id="generate_denial_letter"><span class="icon icon-left mdi mdi-print"></span>&nbsp;{{ __('page.generate_denial_letter') }}</a>
                                        </div>
                                    @endif
                                @elseif($option == 'tr_application')
                                    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                                        <div id="only_approved">
                                            <a href="javascript:void(0);" class="btn btn-space btn-secondary" title="{{ __('page.generate_ar_form_353') }}" id="generate_ar_form_353"><span class="icon icon-left mdi mdi-print"></span>&nbsp;{{ __('page.generate_ar_form_353') }}</a>
                                        </div>

                                        <div id="only_denied">
                                            <a href="javascript:void(0);" class="btn btn-space btn-secondary" title="{{ __('page.generate_denial_letter') }}" id="generate_denial_letter"><span class="icon icon-left mdi mdi-print"></span>&nbsp;{{ __('page.generate_denial_letter') }}</a>
                                        </div>
                                    @endif
                                @endif
                                &nbsp;
                            </div>

                            <div class="col-md-6 text-right">
                        		@if(isset($isSave))
                                    @php
                                        $incl_save_btn = array();
                                        if(isset($cancel_url)) $incl_save_btn['cancel_url'] = $cancel_url;
                                        if(isset($save_btn)) $incl_save_btn['save_btn'] = $save_btn;
                                    @endphp

                        			@include('others.save_button', $incl_save_btn)
                        		@endif

                                @if(isset($isPreview))
                                    @php
                                        $incl_preview_btn = array();
                                        if(isset($preview_btn)) $incl_preview_btn['preview_btn'] = $preview_btn;
                                    @endphp

                                    @include('others.preview_button', $incl_preview_btn)
                                @endif
                            </div>
                            </div>
                    	</div>
                    @endif
                </div>
            </div>
     	</div>
