				
				<div class="form-group row text-right">
                    <div class="col col-sm-12 col-lg-12 offset-sm-1 offset-lg-0">
                        <a href="javascript:void(0);" class="btn btn-space btn-primary" title="{{ __('page.save') }}" id="{{ isset($save_only_btn) ? $save_only_btn : 'save_btn' }}"><i class="icon icon-left mdi mdi-file-plus"></i>{{ __('page.save') }}</a>
                    </div>
                </div>