	@section('employees_css')
        @include('layouts.auth-partials.form-css')
    @endsection

    <div class="card">
        <div class="card-header">

            @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                <div class="row margin-top">
                    <label class="control-label">{{ __('page.filter_by') }}</label>
                    <select class="select2 select2-xs" id="filter_user" name="filter_user">
                        <option value="">{{ __('page.please_select') }}</option>
                        @foreach($list_processors as $key => $val)
                            <option value="{{ $val->id }}">{{ $val->username.' - '.$val->full_name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            <div class="row margin-top">
                <select class="select2 select2-xs" id="filter_list" name="filter_list">
                    <option value="all">{{ __('page.please_select') }}</option>
                    
                        @foreach($application_status as $key => $val)
                            <option value="{{ $val['id'] }}">{{ __('page.'.$val['name']) }}</option>
                        @endforeach
                  
                    <option value="4">{{ __('page.completed') }}</option>
                </select>
            </div>

            <div class="row margin-top">
                <select class="select2 select2-xs" id="filter_order" name="filter_order">
                    <option value="1">{{ __('page.ascending') }}</option>
                    <option value="0">{{ __('page.descending') }}</option>
                </select>
            </div>

            <div class="row margin-top">
                <input type="text" class="form-control form-control-xs" name="filter_value" id="filter_value" placeholder="{{ __('page.search_name') }}">
            </div>

            <div class="row margin-top">
                <a href="javascript:void(0);" class="btn btn-space btn-primary hover" id="filter_btn"><span class="mdi mdi-search"></span>&nbsp;{{ __('page.filter') }}</a>
                <a href="javascript:void(0);" class="btn btn-space btn-secondary hover" id="cancel_btn"><span class="mdi mdi-close"></span>&nbsp;{{ __('page.cancel') }}</a>
            </div>
        </div>  
        <div class="card-body">
            <div class="row" style="max-height: 350px; overflow: auto;">
                <div class="col-md-12">
                    <div id="list_tec_application"></div>
                </div>     
            </div>
        </div>
        <div class="card-divider"></div>
        <div class="card-footer">
            <div id="total_tec_application"></div>
        </div>        
    </div>

    @section('employees_scripts')
    	@include('layouts.auth-partials.form-scripts')

    	<script type="text/javascript">
            $(document).change(function(){
                $('input[type="checkbox"]').on('change', function() {
                    $(this).prop('checked', true);
                    $('input[type="checkbox"]').not(this).prop('checked', false);
                });
            });

            $(document).ready(function () {
                App.formElements();


                var filter_user;
                var filter_list;
                var filter_order;
                var filter_value;
                
                filter_user = localStorage.getItem("filter_user");
                filter_list = localStorage.getItem("filter_list");
                filter_order = localStorage.getItem("filter_order");
                filter_value = localStorage.getItem("filter_value");
                
                localStorage.clear();

                localStorage.setItem('filter_user', filter_user);
                localStorage.setItem('filter_list', filter_list);
                localStorage.setItem('filter_order', filter_order);
                localStorage.setItem('filter_value', filter_value);

                if(filter_user && filter_order)
                {  
                    if(filter_user)
                    {
                        $('#filter_user').val(filter_user).trigger('change');

                        $('#filter_list').val(filter_list).trigger('change');

                        $('#filter_order').val(filter_order).trigger('change');

                        $('#filter_value').val(filter_value);

                        filter_tec_application(filter_user, filter_list, filter_order, filter_value);
                    }
                    else
                    {
                        $('#filter_status').val(filter_status).trigger('change');

                        $('#filter_value').val(filter_value);

                        filter_employees('', '', filter_value, filter_status);   
                    }
                }
                else
                {
                    load_list_tec_application();
                }
            });

            $('#filter_value').keypress(function(e){
                if(e.which == 13){
                    $('#filter_btn').click();
                }
            });

            $('#filter_btn').click(function(){
                var filter_user = $('#filter_user').val();
                var filter_list = $('#filter_list').val();
                var filter_order = $('#filter_order').val();
                var filter_value = $('#filter_value').val();

                localStorage.setItem("filter_user", filter_user);
                localStorage.setItem("filter_list", filter_list);
                localStorage.setItem("filter_order", filter_order);
                localStorage.setItem("filter_value", filter_value);

                filter_tec_application(filter_user, filter_list, filter_order, filter_value);
            });

            $('#cancel_btn').click(function(){
                load_list_tec_application();
            });

            function load_list_tec_application()
            {
                var list;

                $("#list_tec_application").empty();
                $("#total_tec_application").empty();
                
                $('#filter_user').val('').trigger('change.select2');

                @if(Auth::user()->isProcessor())
                    $('#filter_list').val('1').trigger('change.select2');
                    list = 1;
                @elseif(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                    $('#filter_list').val('all').trigger('change.select2');
                    list = 'all';
                @endif

                $('#filter_order').val('1').trigger('change.select2');
                $('#filter_value').val('');
                
                localStorage.removeItem("filter_user");
                localStorage.removeItem("filter_list");
                localStorage.removeItem("filter_order");
                localStorage.removeItem("filter_value");

                axios.get("{{ $based_url.'/tec_application/json' }}", {
                    params: {
                        filter_order : 1,
                        filter_list : list,
                    }
                })
                .then(function(response){
                    const tec_applications = response.data.tec_applications;
                    const total_record = response.data.count_tec_application;

                    $.each(tec_applications, function( key, value ) {
                        $("#list_tec_application").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_application('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['full_name'] +"</label></div>")
                    });

                    $("#total_tec_application").append('<label class="control-label">Total Record: '+total_record+'</label>');
                })
                .catch((error) => {
                    //alert_warning(error.response.data.errors, 1500);
                })
            }

            function filter_tec_application(filter_user, filter_list, filter_order, filter_value)
            {
                axios.get("{{ $based_url.'/tec_application/json' }}", {
                    params: {
                        filter_user : filter_user,
                        filter_list : filter_list,
                        filter_order : filter_order,
                        filter_value : filter_value
                    }
                })
                .then(function(response) {
                    $("#list_tec_application").empty();
                    $("#total_tec_application").empty();

                    const tec_applications = response.data.tec_applications;
                    const total_record = response.data.count_tec_application;

                    if($.trim(tec_applications))
                    {   
                        $.each(tec_applications, function( key, value ) {
                            $("#list_tec_application").append("<div class='custom-control custom-checkbox'><input class='custom-control-input' type='checkbox' id='"+ value['id'] +"' data-id='"+ value['id'] +"' onclick=select_application('"+ value['id'] +"') name='employee'><label class='custom-control-label' for='"+ value['id'] +"'>"+ value['full_name'] +"</label></div>")
                        });

                        $("#total_tec_application").append('<label class="control-label">Total Record: '+total_record+'</label>');
                    }
                    else
                    {
                        load_list_tec_application();
                        alert_warning("{{ __('page.no_record_found') }}", 1500);
                    }
                })
            }

            @include('others.page_script')
        </script>
   	@endsection