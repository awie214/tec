<nav class="navbar navbar-expand fixed-top be-top-header event-background">
    <div class="container-fluid">
        <div class="be-navbar-header ">
            <a href="" >
                <img src="{{ asset('img/tieza-nav-3.png') }}" alt="Logo" height="65px" width="100%">
            </a>
        </div>
        <div class="page-title"><span>{{ __('page.'.$module) }}</span></div>
        <div class="be-right-navbar">
            <ul class="nav navbar-nav float-right be-user-nav">
                <li class="nav-item">
                    <a href="#" role="button" class="nav-link">{{ '('.trans('page.'.Auth::user()->getUserLevel()).') '.Auth::user()->username.' - '.Auth::user()->getFullName()}}</a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><img src="{{ asset('img/avatar-150.png') }}" alt="Avatar"></a>
                    <div role="menu" class="dropdown-menu">
                        <div class="user-info">
                            <div class="user-name">
                                {{ Auth::user()->getFullName() }}
                            </div>
                            <div class="user-position online">Available</div>
                        </div>
                        <a href="{{ url('/account') }}" class="dropdown-item"><span class="icon mdi mdi-account-circle"></span>&nbsp;{{ __('page.account') }}
                        </a>
                        <a href="{{ route('logout') }}"  class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <span class="icon mdi mdi-power"></span>&nbsp;{{ __('page.logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
