<!-- Left Sidebar -->
<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper">
        <a href="#" class="left-sidebar-toggle">{{ __('page.'.$module) }}</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">{{ __('page.menu') }}</li>
                        @if(Auth::user()->isSuperAdmin())
                            @php  
                                $include_application = ['selected_module' => $module,
                                    'menu_name' => 'application',
                                    'url' => '#', 
                                    'icon' => 'icon mdi mdi-file',
                                    'is_parent' => 'yes', 
                                    'sub_menu' => array(
                                        ['is_parent' => 'yes', '2nd_menu_name' => 'tec_', '2nd_sub_menu' => array(
                                            ['url' => '/application/tec_dashboard', 'menu' => 'tec_dashboard'],
                                            ['url' => '/application/tec_application', 'menu' => 'tec_application'],
                                            ['url' => '/application/tec_report', 'menu' => 'tec_report'],     
                                        )],
                                        ['is_parent' => 'yes', '2nd_menu_name' => 'tr_', '2nd_sub_menu' => array(
                                            ['url' => '/application/tr_dashboard', 'menu' => 'tr_dashboard'],
                                            ['url' => '/application/tr_application', 'menu' => 'tr_application'], 
                                            ['url' => '/application/tr_report', 'menu' => 'tr_report'],    
                                        )],
                                    )];

                                $include_system_config = ['selected_module' => $module,
                                    'menu_name' => 'system_config',
                                    'url' => '#', 
                                    'icon' => 'icon mdi mdi-settings',
                                    'is_parent' => 'yes', 
                                    'sub_menu' => array(
                                        ['url' => '/system_config/user_account', 'menu' => 'user_account']
                                    )];
                            @endphp


                            @include('others.sidebar_li', ['selected_module' => $module, 'menu_name' => 'home', 'url' => '/home', 'icon' => 'icon mdi mdi-home'])
                            @include('others.sidebar_li', $include_application)
                            @include('others.sidebar_li', $include_system_config)
                        @elseif(Auth::user()->isRegularAdmin())
                            @php  
                                $include_application = ['selected_module' => $module,
                                    'menu_name' => 'review_validation',
                                    'url' => '#', 
                                    'icon' => 'icon mdi mdi-file',
                                    'is_parent' => 'yes', 
                                    'sub_menu' => array(
                                        ['is_parent' => 'yes', '2nd_menu_name' => 'tec_', '2nd_sub_menu' => array(
                                            ['url' => '/supervisor/application/tec_dashboard', 'menu' => 'tec_dashboard'],
                                            ['url' => '/supervisor/application/tec_application', 'menu' => 'tec_application'],   
                                            ['url' => '/supervisor/application/tec_report', 'menu' => 'tec_report'],    
                                        )],
                                        ['is_parent' => 'yes', '2nd_menu_name' => 'tr_', '2nd_sub_menu' => array(
                                            ['url' => '/supervisor/application/tr_dashboard', 'menu' => 'tr_dashboard'],
                                            ['url' => '/supervisor/application/tr_application', 'menu' => 'tr_application'],  
                                            ['url' => '/supervisor/application/tr_report', 'menu' => 'tr_report'],   
                                        )],
                                    )];
                            @endphp

                            @include('others.sidebar_li', ['selected_module' => $module, 'menu_name' => 'home', 'url' => '/home', 'icon' => 'icon mdi mdi-home'])
                            @include('others.sidebar_li', $include_application)
                        @elseif(Auth::user()->isProcessor())
                            @php  
                                $include_application = ['selected_module' => $module,
                                    'menu_name' => 'application',
                                    'url' => '#', 
                                    'icon' => 'icon mdi mdi-file',
                                    'is_parent' => 'yes', 
                                    'sub_menu' => array(
                                        ['is_parent' => 'yes', '2nd_menu_name' => 'tec_', '2nd_sub_menu' => array(
                                            ['url' => '/processor/application/tec_dashboard', 'menu' => 'tec_dashboard'],
                                            ['url' => '/processor/application/tec_application', 'menu' => 'tec_application'],   
                                        )],
                                        ['is_parent' => 'yes', '2nd_menu_name' => 'tr_', '2nd_sub_menu' => array(
                                            ['url' => '/processor/application/tr_dashboard', 'menu' => 'tr_dashboard'],
                                            ['url' => '/processor/application/tr_application', 'menu' => 'tr_application'],    
                                        )],
                                    )];
                            @endphp

                            @include('others.sidebar_li', ['selected_module' => $module, 'menu_name' => 'home', 'url' => '/home', 'icon' => 'icon mdi mdi-home'])
                            @include('others.sidebar_li', $include_application)
                        @elseif(Auth::user()->isTravelTaxUser())
                            @php  
                                $include_application = ['selected_module' => $module,
                                    'menu_name' => 'review_approved_application',
                                    'url' => '#', 
                                    'icon' => 'icon mdi mdi-file',
                                    'is_parent' => 'yes', 
                                    'sub_menu' => array(
                                        ['url' => '/travel_tax/application/tr_application', 'menu' => 'tr_application'],
                                    )];
                            @endphp

                            @include('others.sidebar_li', ['selected_module' => $module, 'menu_name' => 'home', 'url' => '/home', 'icon' => 'icon mdi mdi-home'])
                            @include('others.sidebar_li', $include_application)
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

