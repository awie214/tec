<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')
    @include('layouts.auth-partials.css')
    @yield('css')
    @yield('employees_css')
    <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
</head>
<body class="event-background">
    <div class="be-wrapper be-fixed-sidebar be-color-header be-loading" id="main-content">
        @include('layouts.auth-partials.top-bar')
	    @include('layouts.auth-partials.left-sidebar')
	    @include('layouts.auth-partials.main-content')
	    {{-- @include('layouts.auth-partials.right-sidebar') --}}
    </div>
    @include('layouts.auth-partials.scripts')
    @yield('employees_scripts')
    @yield('scripts')
    <script src="{{ asset('js/custom.js')}}"></script>
</body>
</html>