<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    <style type="text/css">

        html { margin: 10px}
        body {
            font-family: cambria;
            font-size: 9pt;
            color: #000000;
            background: white;
            margin: 0;
        }

        th {
            vertical-align: top;
        }

        td {
            padding: 2px;
            font-size: 7pt;
            vertical-align: top;
        }

        .has-table-border {
           border: 2px solid black; 
        }

        .page-header, .page-header-space {
            height: 180px;
        }

        .page-footer, .page-footer-space {
        }

        table {
            table-layout: fixed;
        }

        thead {display: table-header-group;} 
        tfoot {display: table-footer-group;}

        .page-footer {
            display: block;
            position: fixed;
            width: 100%;
            bottom: 100px;
        }

        .page-header {
            display: block;
            position: fixed;
            top: 0;
            width: 100%;
        }

        .centered-address {
            display: inline-block;
            position: absolute;
            top: 3%;
            left: 59%;
            font-size: 9px;
            text-align: right;
            font-style: Gotham Book !important;
        }

        .centered-contact {
            display: inline-block;
            position: absolute;
            left:81%;
            font-size: 9px;
            font-style: Gotham Book !important;
            text-align: left;
        }

        #hide_when_print {
            display: none;
        }

        .remove-border {
            border-right:none;
            border-left:none;
            border-bottom:none;
            border-top:none;
        }

        img {
            border:0;
        }
    </style>
</head>
<body>
    <div class="page-header">
        <img src="{{ asset('img/sr_header.jpg') }}" width="100%" height="200" style="display:inline-block;" />
        <div class="centered-address" data-html="true">
            <b>6th & 7th Floors, Tower 1 <br>
            Double Dragon Plaza <br>
            Double Dragon Meridian Park <br>
            Macapagal Avenue corner <br>
            Edsa Extension<br>
            1302 Bay Area, Pasay City<br></b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 4.5%;">
            <b>(+632) 8249-5982</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 6.5%;color:#4285f4">
            &nbsp;<b>traveltax.helpdesk@tieza.gov.ph</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 8.5%;">
            &nbsp;<b>www.tieza.gov.ph</b>
        </div>     
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer.png') }}" width="100%" height="80px">
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="page">
                    <div class="page-header-space"></div>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <br>
                            <br>
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="10"> 
                                            <h3>Travel Tax Department</h3>
                                            <h5>PRIVILEGE ADMINISTRATION DIVISION</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="9"> 
                                            TIEZA Online Form 353
                                        </td>
                                        <td colspan="1"> 
                                            Revised 2020
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="10"> 
                                            <h5>APPLICATION FOR ONLINE TRAVEL TAX REFUND</h5>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7"> 
                                            &nbsp;
                                        </td>
                                        <td colspan="1"> 
                                            <h6>AR Number</h6>
                                        </td>
                                        <td colspan="2"> 
                                            <h6>ONREF-2020-0001</h6>
                                        </td>
                                    </tr>   
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="2">NAME OF PASSENGER</td>
                                        <td class="has-table-border" colspan="8">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="2" rowspan="2">PAYABLE TO<br>(FOR MINOR/ QUALIFIED PAYEE)</td>
                                        <td class="has-table-border" colspan="8" rowspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>&nbsp;</tr>
                                    <tr class="has-table-border  text-center">
                                        <td class="has-table-border" colspan="2" rowspan="2">BANK ACCOUNT DETAILS</td>
                                        <td class="has-table-border" colspan="3" rowspan="1">&nbsp;</td>
                                        <td class="has-table-border" colspan="3" rowspan="1">&nbsp;</td>
                                        <td class="has-table-border" colspan="2" rowspan="1">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border  text-center">
                                        <td class="has-table-border" colspan="3" rowspan="1">&nbsp;ACCOUNT NAME</td>
                                        <td class="has-table-border" colspan="3" rowspan="1">&nbsp;ACCOUNT NUMBER</td>
                                        <td class="has-table-border" colspan="2" rowspan="1">&nbsp;BANK BRANCH</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2">CONTACT NUMBER</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                        <td class="has-table-border" colspan="2">EMAIL ADDRESS</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2">AIRLINE TICKET/ CONFIRMATION NUMBER</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                        <td class="has-table-border" colspan="2">DATE ISSUED</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2">AMOUNT OF TRAVEL TAX</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                        <td class="has-table-border" colspan="2">AMOUNT OF REFUND</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border" colspan="2">TIEZA OFFICIAL RECEIPT NO.</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                        <td class="has-table-border" colspan="2">DATE OF PAYMENT</td>
                                        <td class="has-table-border" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="remove-border" colspan="10" ><br />REASON FOR REFUND</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center remove-border" colspan="1"> &nbsp;</td>
                                        <td class="has-table-border text-center" colspan="1" > &nbsp;</td>
                                        <td class="remove-border" colspan="2">&nbsp;EXEMPTION</td>
                                        <td class="has-table-border text-center" colspan="1" > &nbsp;</td>
                                        <td class="remove-border" colspan="2">&nbsp;REDUCED RATE</td>
                                        <td class="has-table-border text-center" colspan="1" > &nbsp;</td>
                                        <td class="remove-border" colspan="2">&nbsp;NON-COVERAGE</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center remove-border" colspan="1"> &nbsp;</td>
                                        <td class="has-table-border text-center" colspan="1" > &nbsp;</td>
                                        <td class="remove-border" colspan="2">&nbsp;UNUSED TICKET</td>
                                        <td class="has-table-border text-center" colspan="1" > &nbsp;</td>
                                        <td class="remove-border" colspan="2">&nbsp;DOUBLE PAYMENT</td>
                                        <td class="has-table-border text-center" colspan="1" > &nbsp;</td>
                                        <td class="remove-border" colspan="2">&nbsp;OTHERS</td>
                                    </tr>
                                    <tr>
                                        <td class="remove-border" colspan="10"><br></td>
                                    </tr>
                                    <tr>
                                        <td class="remove-border">NOTES:</td>
                                    </tr>
                                    <tr>
                                        <td class="remove-border" colspan="10">> Applications for travel tax refunds should be filed within two (2) years from the date of payment.</td>
                                    </tr>
                                    <tr>
                                        <td class="remove-border" colspan="10">> Travel tax collected on ticket must be remitted to TIEZA before proceeding with the refund.</td>
                                    </tr>
                                    <tr>
                                        <td class="remove-border" colspan="10">> By accomplishing this form, Passenger certify that the above-mentioned information are true and correct and that the travel tax collected on the above-mentioned ticket has not been previously refunded.</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <br>
                                        <td class="has-table-border text-center" colspan="10">PROCESSING OF ONLINE REFUND APPLICATION</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">PROCESS</td>
                                        <td class="has-table-border text-center" colspan="2">DATE</td>
                                        <td class="has-table-border text-center" colspan="3">POINT PERSON</td>
                                        <td class="has-table-border text-center" colspan="2">REMARKS</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Receipt/Evaluation of Application</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Review/Recommendation for Payment</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Print/ Collate of Documents</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Verification of Travel Tax Collection</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Preparation of Disbursement Voucher</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Certification for Payment</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>

                                    <tr class="has-table-border">
                                        <td class="has-table-border text-center" colspan="3">Transmittal of Voucher to Accounting Division</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="3">&nbsp;</td>
                                        <td class="has-table-border text-center" colspan="2">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        
                    </div> 
                </div>
            </div>
        </div>
        
    </div>
</body>

</html>