<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
     <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            font-family: cambria;
            font-size: 16pt;
            background: white;
        }
        .rpt-title {
            font-size: 16pt;
            letter-spacing: 2px;
            font-weight: 600;
        }
        th {
            vertical-align: top;
        }
        td {
            padding: 2px;
            font-size: 16pt;
            vertical-align: top;
        }

        .page-header, .page-header-space {
            height: 310px;
          }

          .page-footer, .page-footer-space {
          }

          .page-header {
            display: none;
          }

          .page-footer {
            display: none;
          }

          .centered-address{
            display: none;
          }

          .centered-contact{
            display: none;
          }



        @media print {
            thead {display: table-header-group;} 
            tfoot {display: table-footer-group;}

            .page-footer {
                display: block;
                position: fixed;
                bottom: 0;
                width: 100%;
            }

            .page-header {
                display: block;
                position: fixed;
                top: 0;
                width: 100%;
            }

            .centered-address {
                display: inline-block;
                position: absolute;
                top: 23%;
                left: 60%;
                font-size: 12px;
                text-align: right;
                font-style: Gotham Book !important;
            }

            .centered-contact {
                display: inline-block;
                position: absolute;
                left:81%;
                font-size: 12px;
                font-style: Gotham Book !important;
                text-align: left;
            }

            #hide_when_print{
                display: none;
            }

            body {margin: 0;}

            .page {
                page-break-after: always !important;;
                width: 100%;
            }
          
        }

        .underline {
            border-bottom: 1px solid #000;
        }

         
        .nextpage {
            page-break-after:always !important;
        }
          
        .border-bottom-black { border-bottom: 1px solid black; }
    </style>
</head>
<body>
    <div class="page-header">
        <img src="{{ asset('img/sr_header.jpg') }}" width="100%" height="300" />
        <div class="centered-address" data-html="true">
            <b>6th & 7th Floors, Tower 1 <br>
            Double Dragon Plaza <br>
            Double Dragon Meridian Park <br>
            Macapagal Avenue corner <br>
            Edsa Extension<br>
            1302 Bay Area, Pasay City<br></b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 25%;">
            <b>(+632) 8249-5982</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 35%;color:#4285f4">
            &nbsp;<b>traveltax.helpdesk@tieza.gov.ph</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 45%;">
            &nbsp;<b>www.tieza.gov.ph</b>
        </div>     
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer.png') }}" width="100%">
    </div>

    <div>
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="page">
                <div class="page-header-space"></div>
                    <div class="row">
            <div class="col-lg-12">
                <table width="100%">
                    <tbody>
                        <tr>
                            <td style="border: none; padding-left: 10%; padding-right: 10%;">
                                <div class="row">
                                    <div class="col-lg-12">
                                        {{ date('d M Y') }}
                                        <br>
                                        <br>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8">
                                        Dear <b>Mr. / Ms. {{ $ta->ffull_name }}</b>,
                                        <br>
                                        <br>
                                        Greetings from the TIEZA! 
                                        <br>
                                        <br>
                                        This concerns your online Travel Tax Exemption Certificate (TEC) Application with No. <u>{{ $ta->app_id }}</u> submitted on <u>{{ date("m/d/Y", strtotime($ta->date_application)) }}</u>, for which you are seeking exemption. 
                                        <br>
                                        <br>
                                        Upon review of your submitted information and documents, we regret to inform you that our Travel Tax Officer has determined that you are not eligible for exemption due to the following reasons:
                                        <br>
                                        <br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="mdi mdi-chevron-right"></span>&nbsp;&nbsp;{{ $ta->reason_denial }}
                                        <br>
                                        <br>
                                        <br>
                                        For any additional queries pertaining to this matter, please contact traveltax.helpdesk@tieza.gov.ph .
                                        <br>
                                        <br>
                                        Thank you.
                                        <br>
                                        <br>
                                        Best regards,
                                        <br>
                                        <br>
                                        {{ Auth::user()->getFullNameFML() }}
                                        <br>
                                        Travel Tax Officer 
                                        <br>
                                        Travel Tax Department 
                                    </div>
                                </div>
                                   
                            </td>
                            
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

                </div>
            </div>
        </div>
    </div>

    @include('layouts.auth-partials.scripts')
    @yield('scripts')
</body>

</html>