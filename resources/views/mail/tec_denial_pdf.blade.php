<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    <style type="text/css">
        body {
            font-family: cambria;
            font-size: 9pt;
            background: white;
            margin: 0;
        }

        th {
            vertical-align: top;
        }

        td {
            padding: 2px;
            font-size: 12pt;
            vertical-align: top;
        }

        .page-header, .page-header-space {
            height: 180px;
        }

        .page-footer, .page-footer-space {
        }

        thead {display: table-header-group;} 
        tfoot {display: table-footer-group;}

        .page-footer {
            display: block;
            position: fixed;
            width: 100%;
            bottom: 100px;
        }

        .page-header {
            display: block;
            position: fixed;
            top: 0;
            width: 100%;
        }

        .centered-address {
            display: inline-block;
            position: absolute;
            top: 3%;
            left: 59%;
            font-size: 9px;
            text-align: right;
            font-style: Gotham Book !important;
        }

        .centered-contact {
            display: inline-block;
            position: absolute;
            left:81%;
            font-size: 9px;
            font-style: Gotham Book !important;
            text-align: left;
        }

        #hide_when_print {
            display: none;
        }

        img {
            border:0;
        }
    </style>
</head>
<body>
    <div class="page-header">
        <img src="{{ asset('img/sr_header.jpg') }}" width="100%" height="200" />
        <div class="centered-address" data-html="true">
            <b>6th & 7th Floors, Tower 1 <br>
            Double Dragon Plaza <br>
            Double Dragon Meridian Park <br>
            Macapagal Avenue corner <br>
            Edsa Extension<br>
            1302 Bay Area, Pasay City<br></b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 4.5%;">
            <b>(+632) 8249-5982</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 6.5%;color:#4285f4">
            &nbsp;<b>traveltax.helpdesk@tieza.gov.ph</b>
        </div>
        <div class="centered-contact" data-html="true" style="top: 8.5%;">
            &nbsp;<b>www.tieza.gov.ph</b>
        </div>     
    </div>

    <div class="page-footer">
        <img src="{{ asset('img/footer.png') }}" width="100%" height="80px">
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
                <div class="page">
                    <div class="page-header-space"></div>
                    <div class="row">
                        <div class="col-xs-12">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td style="border: none; padding-left: 10%; padding-right: 10%;">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <br>
                                                    <br>
                                                    {{ date('d M Y') }}
                                                    <br>
                                                    <br>
                                                    Dear <b>Mr. / Ms. {{ $ta->ffull_name }}</b>,
                                                    <br>
                                                    <br>
                                                    Greetings from the TIEZA! 
                                                    <br>
                                                    <br>
                                                    This concerns your online Travel Tax Exemption Certificate (TEC) Application with No. <u>{{ $ta->app_id }}</u> submitted on <u>{{ date("m/d/Y", strtotime($ta->date_application)) }}</u>, for which you are seeking exemption. 
                                                    <br>
                                                    <br>
                                                    Upon review of your submitted information and documents, we regret to inform you that our Travel Tax Officer has determined that you are not eligible for exemption due to the following reasons:
                                                    <br>
                                                    <br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#8226;&nbsp;&nbsp;{{ $ta->reason_denial }}
                                                    <br>
                                                    <br>
                                                    For any additional queries pertaining to this matter, please contact traveltax.helpdesk@tieza.gov.ph .
                                                    <br>
                                                    <br>
                                                    Thank you.
                                                    <br>
                                                    <br>
                                                    Best regards,
                                                    <br>
                                                    <br>
                                                    {{ Auth::user()->getFullNameFML() }}
                                                    <br>
                                                    Travel Tax Officer 
                                                    <br>
                                                    Travel Tax Department 
                                                </div>
                                            </div>
                                               
                                        </td>
                                        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</body>

</html>