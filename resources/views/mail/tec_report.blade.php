<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('layouts.auth-partials.meta')
    @yield('meta')

    @include('layouts.auth-partials.css')
    @yield('css')
    <link rel="stylesheet" href="{{ asset('beagle-v1.7.1/src/assets/css/app.css') }}" type="text/css"/>
    <style type="text/css">
        body {
            background: white;
            font-size: 10pt;
            font-family: Cambria /**Courier New**/;
            color: black;
        }
        input {
            border: 1px solid black;
        }
        .canvas {
             background-image: url('{{ asset('img/watermark_0.png') }}');
             background-position: center;
             background-repeat: no-repeat;

        }

        .bg-triangle {
             background-image: url('{{ asset('img/bg-triangle.png') }}');
             -webkit-background-size: cover;
              -moz-background-size: cover;
              -o-background-size: cover;
              background-size: cover;
        }

        img {
            display:block;
        }

        .center {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 50%;
        }
    </style>
</head>
<body>
    <div class="container-fluid bg-triangle" style="border: 1px solid black; padding: 20px;">
        <div class="col-md-12">
            <div class="row" >
                <div class="col-md-2 col-sm-2"></div>
                <div class="col-md-8 col-sm-8 canvas">
                    <div class="row">
                        <div class="col-md-2">
                            
                        </div>
                        <div class="col-md-8 text-center" style="font-family: cambria; font-size: 14pt;position: relative;position: relative;top: 10px;"><b>TRAVEL TAX<br>EXEMPTION CERTIFICATE</b>
                        </div>
                        <div class="col-md-2">
                            <div style="height: 120px;width: 120px; border: 1px solid black;">
                                <image src="{{  url('images/'.$ta->user_id.'/'.$ta->id_picture_2x2_fn) }}" height="100%" width="100%" />
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top" style="text-align: justify; font-size: 13pt;">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    This is to certify that <b>Mr. / Mrs.</b>&nbsp;<span>&nbsp;<b><u>{{ strtoupper($ta->ffull_name) }}</u></b>&nbsp;</span> with passport number <span>&nbsp;<b><u>{{ $ta->passport_no }}</u></b>&nbsp;</span> is exempted from payment of travel tax pursuant to <b>Sec.</b>&nbsp;<span>&nbsp;<b><u>{{ $ta->section_code }}</u></b>&nbsp;</span> of Presidential Decree (P.D.) 1183, as amended.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top" style="text-align: justify; font-size: 13pt;letter-spacing: 1px;">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    Ticket Number/Booking Ref. Number <span>&nbsp;<b><u>{{ $ta->ticket_no }}</u></b>&nbsp;</span>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-12">
                                    Date Issued: <span>&nbsp;<b><u>{{ date("m/d/Y", strtotime($ta->date_application)) }}</u></b>&nbsp;</span>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-12">
                                    TEC No. <span>&nbsp;<b><u>{{ $ta->tec_id }}</u></b>&nbsp;</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="height: 120px;width: 120px;">
                                        <div id="qrcode"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <B>IMPORTANT REMINDERS:</B>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row margin-top">
                                <div class="col-md-12">
                                    <b>1. Valid only for travel to:&nbsp;&nbsp;<span>&nbsp;<b><u>{{ strtoupper($ta->country_name) }}</u></b>&nbsp;</span></b>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-12">
                                    <b>2. Not valid if departing after:&nbsp;&nbsp;<span>&nbsp;<b><u>{{ date("m/d/Y", strtotime($ta->date_validity)) }}</u></b>&nbsp;</span></b>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-12">
                                    <b>3. Not valid with erasures/alterations</b>
                                </div>
                            </div>
                            <div class="row margin-top">
                                <div class="col-md-12">
                                    <b>4. Valid only for one departure/issuance of ticket</b>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 text-center">
                            <div class="row" style="font-size: 12pt;">
                                <div class="col-md-12" style="left: 30px;">
                                    <span style="font-weight: 800;"><img class="center" src="{{ $based_url.'/esignature/cristeto_ocampo_.png' }}" width="190em" height="190em" style="position: relative;position: relative;top: 80px;left: 10px"/>CRISTETO G. OCAMPO</span>
                                    <br>
                                    <i>Officer-in-charge</i>
                                    <br>
                                    <i>Travel Tax Department</i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row margin-top">

                        <div class="col-md-6">{{ date('m/d/Y H:i:s') }}
                        </div>
                        <div class="col-md-6 text-right">{{ Auth::user()->username.'-'.$ta->processor_code }}
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-md-2 col-sm-2"></div>
            </div>
        </div>
    </div>
    @include('layouts.auth-partials.scripts')
    @include('layouts.auth-partials.form-scripts')

    <script type="text/javascript" src="{{ asset('qrcodejs/qrcode.js')}}"></script>
    <script type="text/javascript" src="{{ asset('easyqrcodejs/src/easy.qrcode.js')}}"></script>


    <script type="text/javascript">
       
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            text: 'Name of Applicant: ' + "{{ $ta->ffull_name }}" + '\n' + 'Passport No.: ' + "{{ $ta->passport_no }}" + '\n' + 'Ticket No.: ' + "{{ $ta->ticket_no }}" + '\n' + 'TEC No.: ' + "{{ $ta->tec_id }}" + '\n' + 'TEC Date Issued: ' + "{{ date("m/d/Y", strtotime($ta->date_application)) }}" + '\n' + 'TEC Validity: ' + "{{ date("m/d/Y", strtotime($ta->date_validity)) }}",
            logo: "{{ asset('img/tieza-logo.png') }}",
            logoWidth: 80,
            logoHeight: 80,
            width : 190,
            height : 190,
            logoBackgroundColor: '#ffffff',
            logoBackgroundTransparent: true
        });

        {{--
        var qrcode = new QRCode(document.getElementById("qrcode"), {
            width : 120,
            height : 120
        });

        function makeCode () {      
            qrcode.makeCode('Name of Applicant: ' + "{{ $ta->ffull_name }}" + '\n' + 'Passport No.: ' + "{{ $ta->passport_no }}" + '\n' + 'Ticket No.: ' + "{{ $ta->ticket_no }}" + '\n' + 'TEC No.: ' + "{{ $ta->tec_id }}" + '\n' + 'TEC Date Issued: ' + "{{ date("m/d/Y", strtotime($ta->date_application)) }}" + '\n' + 'TEC Validity: ' + "{{ date("m/d/Y", strtotime($ta->date_validity)) }}");
        }

        $(qrcode._el).find('img').on('load', function() {
            var srcimg = this.src;
            console.log(srcimg);
            $("#downloadqr").attr('href', srcimg);
            $("#downloadqr").attr('download', "QR.png");
        });

        makeCode();
        --}}
    </script>

    <script src="{{ asset('js/custom.js')}}"></script>
</body>

</html>