        
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                    <div class="form-group row" id="frm_input_report_name">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.report_name')}}</label>
                            <select class="select2 select2-xs criteria" 
                                    name="report_name" 
                                    id="report_name">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($tec_reports as $key => $val)
                                        <option value="{{ $val }}">{{ trans('page.'.$val) }}</option>
                                    @endforeach
                            </select>
                            <div class="select_error" id="error-report_name"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                    <div id="show_list"></div>
                </div>

                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                    <div class="form-group" id="frm_input_month">
                        <label class="control-label">{{ __('page.month')}}</label>
                        <select class="select2 select2-xs criteria" 
                                name="month" 
                                id="month">   
                                <option value="">{{ __('page.please_select') }}</option>
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                        </select>
                        <div class="select_error" id="error-month"></div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                    <div class="form-group" id="frm_input_year">
                        <label class="control-label">{{ __('page.year')}}</label>
                        <select class="select2 select2-xs criteria" 
                                name="year" 
                                id="year">   
                                <option value="">{{ __('page.please_select') }}</option>
                                @for($i = 2020; $i <= 2025; $i++)
                                    <option valae="{{ $i }}">{{ $i }}</option>
                                @endfor
                        </select>
                        <div class="select_error" id="error-year"></div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                    <div class="form-group" id="frm_input_status">
                        <label class="control-label">{{ __('page.status')}}</label>
                        <select class="select2 select2-xs criteria" 
                                name="status" 
                                id="status">   
                                <option value="">{{ __('page.please_select') }}</option>
                                @foreach($application_status as $key => $val)
                                    <option value="{{ $val['id'] }}">{{ __('page.'.$val['name']) }}</option>
                                @endforeach
                        </select>
                        <div class="select_error" id="error-status"></div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                    <div class="form-group" id="frm_input_completed">
                        <label class="control-label">{{ __('page.completed')}}</label>
                        <select class="select2 select2-xs criteria" 
                                name="completed" 
                                id="completed">   
                                <option value="">{{ __('page.please_select') }}</option>
                                <option value="1">{{ __('page.yes') }}</option>
                                <option value="0">{{ __('page.no') }}</option>
                        </select>
                        <div class="select_error" id="error-completed"></div>
                    </div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
                $(document).ready(function () {
                });

                $('#report_name').on('change', function() {
                    $('#show_list').empty();

                    var report_name = $('#report_name').val();

                    if($(this).val().length)
                    {
                        if($(this).val() == 'airlines_passenger')
                        {
                            axios.get("{{ $based_url.'/airlines/json' }}")
                            .then(function(response){
                                const airlines = response.data.airlines;

                                if(airlines)
                                {
                                    var airlines_select_start = "<div class='form-group' id='frm_input_airlines'><label class='control-label'>{{ __('page.list_airlines') }}</label><select class='select2 select2-xs criteria' name='airlines' id='airlines'><option value=''>{{ __('page.please_select') }}</option>";

                                    var airlines_select_end = "</select><div class='select_error' id='error-airlines'></div></div>";

                                    $.each(airlines, function( key, value ) {
                                        airlines_select_start = airlines_select_start + "<option value='"+ value['id'] +"'>"+ value['name'] +"</option>";
                                    });

                                    $("#show_list").append(airlines_select_start + airlines_select_end);

                                    $(".select2").select2({
                                      width: '100%'
                                    });
                                }                        
                            })
                            .catch((error) => {
                                alert_warning(error.response.data.errors, 1500);
                            });
                        }
                    }
                });


                $("#preview_btn").click(function() {
                    load_report();
                });

                function load_report()
                {
                    /**
                    var report_name, frm, formData, month, year, status, completed;

                    frm = document.querySelector('#print_form');

                    formData = new FormData();

                    report_name = $("#report_name").val();

                    formData.append("report_name", $("#report_name").val());
                    formData.append("month", $("#month").val());
                    formData.append("year", $("#year").val());
                    formData.append("status", $("#status").val());
                    formData.append("completed", $("#completed").val());

                    if(report_name == 'airlines_passenger')
                    {
                        formData.append("airlines", $("#airlines").val());
                    }

                    axios.post(frm.action, formData)
                    .then((response) => {
                        
                    })
                    .catch((error) => {
                        const errors = error.response.data.errors;

                        if(typeof(errors) == 'string')
                        {
                            alert_warning(errors);
                        }
                    });
                    **/

                    var params = '';

                    $("[class*='criteria']").each(function (key) {
                        var tag_name    = $(this).prop('tagName');
                        var id          = $(this).attr('id');
                        var value       = $(this).val();
                        var type        = $(this).attr('type'); 

                        if(value.length) params += id + '=' + value + '&';
                    });

                    var href = '{{ $application_url }}' + '/tec_report/print' + '?' + params;
                    window.open(href, '_blank');
                }
            </script>
        @endsection