

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.application_no') }}</label>
                                <input type="text" class="form-control form-control-xs" id="application_no" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.date_application') }}</label>
                                <input type="text" class="form-control form-control-xs" value="{{ date("d M Y") }}" id="date_application" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.last_name') }}<span class="text-red">&nbsp;*</span></label>
                                <input type="text" class="form-control form-control-xs" name="last_name" id="last_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.first_name') }}<span class="text-red">&nbsp;*</span></label>
                                <input type="text" class="form-control form-control-xs" name="first_name" id="first_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.middle_name') }}</label>
                                <input type="text" class="form-control form-control-xs" name="middle_name" id="middle_name">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group row" id="frm_input_list_type_applicant">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.list_type_applicant')}}<span class="text-red">&nbsp;*</span></label>
                        <select class="select2 select2-xs" 
                                name="list_type_applicant" 
                                id="list_type_applicant">   
                                <option value="">{{ __('page.please_select') }}</option>    
                                @foreach($type_applicant_refund_list as $key => $val)
                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                @endforeach        
                        </select>
                        <div class="select_error" id="error-list_type_applicant"></div>
                    </div>
                </div>

                <div id="show_type_applicant">
                </div>

                <div id="show_applicant_to_upload">               
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <label class="control-label">Please see the scanned copies requirement below:</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.passport_identification_page') }}
                        </label>
                        <div class="pull-right" id="float_passport_identification_link"></div>
                        <div id="link_passport_identification_page"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="passport_identification_page_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.airline_ticket_issued') }}
                        </label>
                        <div class="pull-right" id="float_airline_ticket_link"></div>
                        <div id="link_ticket_booking_ref_no_"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="ticket_booking_ref_no_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.tieza_official_receipt_note_0') }}
                        </label>
                        <label class="control-label">{{ __('page.tieza_official_receipt_note_1') }}
                        </label>
                        <div class="pull-right" id="float_tieza_official_receipt_link"></div>
                        <div id="link_tieza_official_receipt"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="tieza_official_receipt_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.additional_file') }}
                        </label>
                        <div class="pull-right" id="float_additional_file_link"></div>
                        <div id="link_additional_file"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="additional_file_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.generated_form_353') }}
                        </label>
                        <div class="pull-right" id="float_form_353_link"></div>
                        <div id="link_form_353"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="form_353_fn"></div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="tr_id" id="tr_id" />

        <div class="modal fade colored-header colored-header-primary" id="passport_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.passport_identification_page') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close">       </span></button>
                    </div>
                    <div class="modal-body">
                        <table id="passport_files_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="airline_ticket_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.airline_ticket_issued') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close">       </span></button>
                    </div>
                    <div class="modal-body">
                        <table id="airline_ticket_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="tieza_official_receipt_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.tieza_official_receipt') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="travel_tax_receipt_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="additional_file_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.additional_file') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="additional_file_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="file_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.type_applicant') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="file_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="form_353_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.generated_form_353') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center" id="load_form_353"></div>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#last_name').prop('disabled', true);
                    $('#first_name').prop('disabled', true);
                    $('#middle_name').prop('disabled', true);
                    $('#list_type_applicant').prop('disabled', true);
                    $('#type_applicant').prop('disabled', true);
                    $('#id_picture_2x2').prop('disabled', true);
                    $('#passport_identification_page').prop('disabled', true);
                    $('#ticket_booking_ref_no_').prop('disabled', true);
                    $('#tieza_official_receipt').prop('disabled', true);
                    $('#additional_file').prop('disabled', true);
                    $('#file_0').prop('disabled', true);
                    $('#file_1').prop('disabled', true);
                    $('#file_2').prop('disabled', true);
                });

                $('#list_type_applicant').on('change', function() {
                    $('#show_type_applicant').empty();

                    if($(this).val().length)
                    {
                        axios.get("{{ $based_url.'/section/json' }}", {
                            params: {
                                section_type : $(this).val()
                            }
                        })
                        .then(function(response){
                            const sections = response.data.sections;

                            if(sections)
                            {
                                var section_select_start = "<div class='form-group row' id='frm_input_type_applicant'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><label class='control-label'>{{ __('page.type_applicant') }}<span class='text-red'>&nbsp;*</span></label><select class='select2 select2-xs' name='type_applicant' id='type_applicant' onchange='load_section_upload()' disabled><option value=''>{{ __('page.please_select') }}</option>";

                                var section_select_end = "</select><div class='select_error' id='error-type_applicant'></div></div></div>";

                                $.each(sections, function( key, value ) {
                                    section_select_start = section_select_start + "<option value='"+ value['id'] +"'>"+ value['name'] +"</option>";
                                });

                                $("#show_type_applicant").append(section_select_start + section_select_end);

                                $(".select2").select2({
                                  width: '100%'
                                });
                            }                        
                        })
                        .catch((error) => {
                            alert_warning(error.response.data.errors, 1500);
                        }).finally(function(){
                            axios.get("{{ $based_url.'/tr_application/json' }}", {
                                params: {
                                    tr_id : $('#tr_id').val(),
                                }
                            }).then(function(response){
                                const tr_application = response.data.tr_application;

                                $("#type_applicant").val('').trigger('change');

                                if(tr_application['section_id'] == $('#list_type_applicant').val())
                                {
                                   $("#type_applicant").val(tr_application['applicant_type_id']).trigger('change'); 
                                }
                            });
                        });
                    }   
                });

                function load_section_upload(type_applicant = null)
                {
                    $('#show_applicant_to_upload').empty();

                    if(type_applicant)
                    {
                        $('#type_applicant').val(type_applicant).trigger('change');
                    }
                    else
                    {
                        var type_applicant = $('#type_applicant').val();
                    }

                    if($('#type_applicant').val())
                    {
                        axios.get("{{ $based_url.'/section_upload/json' }}", {
                            params: {
                                section_id : type_applicant
                            }
                        })
                        .then(function(response){
                            const sections_uploads = response.data.sections_uploads;

                            if(sections_uploads)
                            {
                                $.each(sections_uploads, function( key, value ) {
                                    $("#show_applicant_to_upload").append("<div class='form-group row'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><label class='control-label'>"+ value['description'] +"</label><div class='pull-right' id='link_file_" + key + "'><a class='' data-toggle='modal' data-target='#file_modal' id='additional_file_files' onclick=load_file('file_"+ key +"') href='javascript:void(0);'><span class='mdi mdi-hc-2x mdi-more'></span></a></div></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><div id='file_fn_"+ key +"'></div></div>");
                                });
                            }                        
                        })
                        .catch((error) => {
                            alert_warning(error.response.data.errors, 1500);
                        });
                    }
                }

                @php
                    $upload_columns = array(['data' => 'action', 'sortable' => false], ['data' => 'file_name'], ['data' => 'file_type'], ['name' => 'upload_at.timestamp', 'type' => 'num', 'data' => [ "_" => 'upload_at.display', "sort" => 'upload_at' ]], ['data' => 'file']);
                @endphp

                function load_passport_attachment()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#passport_files_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=passport&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_airline_attachment()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#airline_ticket_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=airline&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_travel_tax_receipt()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#travel_tax_receipt_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=tieza_official_receipt&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_additional_attachment()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#additional_file_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=additional&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_file(file)
                {
                    var tr_id = $('#tr_id').val();

                    var section = $('#type_applicant').val();

                    load_datables('#file_tbl', "{!! $based_url.'/tr_application/datatables/attachment?' !!}selected_option="+ file +"&section_id="+ section +"&tec_id=" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_form_353(file)
                {
                    $('#load_form_353').empty();

                    var tr_id = $('#tr_id').val();

                    $('#load_form_353').append('<img src="'+ "{{ $based_url.'/tr/certificate' }}/" + tr_id + "/" + file +'" width=500px height=500px />');
                }

             
                function select_application(tr_id)
                {
                    clearErrors();

                    $('#float_passport_identification_link').empty();
                    $('#float_airline_ticket_link').empty();
                    $('#float_additional_file_link').empty();
                    $('#float_tieza_official_receipt_link').empty();
                    $('#float_form_353_link').empty();
                    
                    axios.get("{{ $based_url.'/tr_application/json' }}", {
                        params: {
                            tr_id : tr_id,
                        }
                    })
                   .then(function(response){
                        const tr_application = response.data.tr_application;

                        $("#tr_id").val(tr_application['id']);
                        $("#application_no").val(tr_application['app_id']);

                        $('#last_name').val(tr_application['last_name']);
                        $('#first_name').val(tr_application['first_name']);
                        $('#middle_name').val(tr_application['middle_name']);

                        if(tr_application['form_353_airline_fn'])
                        {
                            $('#float_form_353_link').append('<a class="" data-toggle="modal" data-target="#form_353_modal" id="2x2_file" onclick=load_form_353("'+tr_application['form_353_airline_fn'] +'") href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');
                        }

                        $('#list_type_applicant').val(tr_application['section_id']).trigger('change');

                        $('#float_passport_identification_link').append('<a class="" data-toggle="modal" data-target="#passport_modal" id="passport_files" onclick="load_passport_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                        $('#float_airline_ticket_link').append('<a class="" data-toggle="modal" data-target="#airline_ticket_modal" id="airline_ticket_files" onclick="load_airline_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                        $('#float_tieza_official_receipt_link').append('<a class="" data-toggle="modal" data-target="#tieza_official_receipt_modal" id="tieza_official_receipt_files" onclick="load_travel_tax_receipt()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                        $('#float_additional_file_link').append('<a class="" data-toggle="modal" data-target="#additional_file_modal" id="additional_file_files" onclick="load_additional_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');
                    }).catch((error) => {
                        alert_warning(error.response.data.errors, 1500);
                    });
                }
            </script>
        @endsection 