

        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <label class="control-label text-red">{{ __('page.required_fields') }}</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.application_no') }}</label>
                                <input type="text" class="form-control form-control-xs" id="application_no" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.date_application') }}</label>
                                <input type="text" class="form-control form-control-xs" value="{{ date("d M Y") }}" id="date_application" readonly>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row" id="frm_input_is_minor">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <label class="control-label">{{ __('page.is_minor')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="is_minor" 
                                    id="is_minor">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    <option value="1">{{ __('page.yes') }}</option>
                                    <option value="0" selected>{{ __('page.no') }}</option>
                            </select>
                            <div class="select_error" id="error-is_minor"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.last_name') }}<span class="text-red">&nbsp;*</span></label>
                                <input type="text" class="form-control form-control-xs" name="last_name" id="last_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.first_name') }}<span class="text-red">&nbsp;*</span></label>
                                <input type="text" class="form-control form-control-xs" name="first_name" id="first_name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label">{{ __('page.middle_name') }}</label>
                                <input type="text" class="form-control form-control-xs" name="middle_name" id="middle_name">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div id="guardian_group">
                            <div class="form-group row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label">{{ __('page.guardian_last_name') }}<span class="text-red">&nbsp;*</span></label>
                                    <input type="text" class="form-control form-control-xs" name="guardian_last_name" id="guardian_last_name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label">{{ __('page.guardian_first_name') }}<span class="text-red">&nbsp;*</span></label>
                                    <input type="text" class="form-control form-control-xs" name="guardian_first_name" id="guardian_first_name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label">{{ __('page.guardian_middle_name') }}</label>
                                    <input type="text" class="form-control form-control-xs" name="guardian_middle_name" id="guardian_middle_name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.mobile_no') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="mobile_no" id="mobile_no">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.email_address') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="email_address" id="email_address">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.passport_no') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="passport_no" id="passport_no">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.ticket_booking_ref_no') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="ticket_booking_ref_no" id="ticket_booking_ref_no">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.date_ticket_issued') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="date" class="form-control form-control-xs" name="date_ticket_issued" id="date_ticket_issued">
                    </div>
                </div>

                <div class="form-group row" id="frm_input_country_designation">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.country_designation')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="country_designation" 
                                    id="country_designation">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($countries as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-country_designation"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.date_flight') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="date" class="form-control form-control-xs" name="date_flight" id="date_flight">
                    </div>
                </div>

                <div class="form-group row" id="frm_input_airlines_name">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.airlines_name')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="airlines_name" 
                                    id="airlines_name">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($list_airlines as $key => $val)
                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-airlines_name"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="form-group row" id="frm_input_amount_travel_tax_collected">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.amount_travel_tax_collected')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="amount_travel_tax_collected" 
                                    id="amount_travel_tax_collected">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($get_refund_amount_by as $key => $val)
                                        <option value="{{ number_format($val->amount, 2, '.', '') }}">{{ $val->description }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-amount_travel_tax_collected"></div>
                    </div>
                </div>

                <div class="form-group row" id="frm_input_list_type_applicant">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.list_type_applicant')}}<span class="text-red">&nbsp;*</span></label>
                        <select class="select2 select2-xs" 
                                name="list_type_applicant" 
                                id="list_type_applicant">   
                                <option value="">{{ __('page.please_select') }}</option>    
                                @foreach($type_applicant_refund_list as $key => $val)
                                    <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                @endforeach        
                        </select>
                        <div class="select_error" id="error-list_type_applicant"></div>
                    </div>
                </div>

                <div id="show_type_applicant">
                </div>

                <div id="show_applicant_to_upload">               
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <label class="control-label">Please upload scanned copies of the requirement below:</label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.passport_identification_page') }}
                        </label>
                        <div class="pull-right" id="float_passport_identification_link"></div>
                        <div id="link_passport_identification_page"></div>
                        <input class="form-control form-control-xs" type="file" name="passport_identification_page" id="passport_identification_page" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="passport_identification_page_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.airline_ticket_issued') }}
                        </label>
                        <div class="pull-right" id="float_airline_ticket_link"></div>
                        <div id="link_ticket_booking_ref_no_"></div>
                        <input class="form-control form-control-xs" type="file" name="ticket_booking_ref_no_" id="ticket_booking_ref_no_" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="ticket_booking_ref_no_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.tieza_official_receipt_note_0') }}
                        </label>
                        <label class="control-label">{{ __('page.tieza_official_receipt_note_1') }}
                        </label>
                        <div class="pull-right" id="float_tieza_official_receipt_link"></div>
                        <div id="link_tieza_official_receipt"></div>
                        <input class="form-control form-control-xs" type="file" name="tieza_official_receipt" id="tieza_official_receipt" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="tieza_official_receipt_fn"></div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.additional_file') }}
                        </label>
                        <div class="pull-right" id="float_additional_file_link"></div>
                        <div id="link_additional_file"></div>
                        <input class="form-control form-control-xs" type="file" name="additional_file" id="additional_file" multiple="multiple">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="" id="additional_file_fn"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>

                <div class="form-group row" id="frm_input_form_travel_tax_refund">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.form_travel_tax_refund')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="form_travel_tax_refund" 
                                    id="form_travel_tax_refund">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($travel_tax_refund_form as $key => $val)
                                        <option value="{{ $val['id'] }}">{{ __('page.'.$val['name']) }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-form_travel_tax_refund"></div>
                    </div>
                </div>

                <div id="cash_refund_fields">
                    <div class="form-group row" id="frm_input_bank_name">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.bank_name')}}<span class="text-red">&nbsp;*</span></label>
                                <select class="select2 select2-xs" 
                                        name="bank_name" 
                                        id="bank_name">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_banks as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                                        @endforeach
                                </select>
                            <div class="select_error" id="error-bank_name"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.bank_account_name') }}<span class="text-red">&nbsp;*</span></label>
                            <input type="text" class="form-control form-control-xs" name="bank_account_name" id="bank_account_name">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.bank_account_no') }}<span class="text-red">&nbsp;*</span></label>
                            <input type="text" class="form-control form-control-xs" name="bank_account_no" id="bank_account_no">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.refund_amount') }}<span class="text-red">&nbsp;*</span></label>
                            <input type="number" class="form-control form-control-xs" name="refund_amount" id="refund_amount">
                        </div>
                    </div>

                    <div class="form-group row" id="frm_input_reason_refund">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.reason_refund')}}<span class="text-red">&nbsp;*</span></label>
                                <select class="select2 select2-xs" 
                                        name="reason_refund" 
                                        id="reason_refund">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_refund_reason as $key => $val)
                                            <option value="{{ $val['id'] }}">{{ $val['name'] }}</option>
                                        @endforeach
                                </select>
                            <div class="select_error" id="error-reason_refund"></div>
                        </div>
                    </div>

                    <div class="form-group row" id="div_other_reason">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.other_reasons') }}<span class="text-red">&nbsp;*</span></label>
                            <input type="text" class="form-control form-control-xs" name="other_reasons" id="other_reasons">
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.tieza_official_receipt_no') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="text" class="form-control form-control-xs" name="tieza_official_receipt_no" id="tieza_official_receipt_no">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.date_payment') }}<span class="text-red">&nbsp;*</span></label>
                        <input type="date" class="form-control form-control-xs" name="date_payment" id="date_payment">
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <hr />
                    </div>
                </div>

                @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                    <div class="form-group row" id="frm_input_assigned_processor">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.assigned_processor')}}<span class="text-red">&nbsp;*</span></label>
                                <select class="select2 select2-xs" 
                                        name="assigned_processor" 
                                        id="assigned_processor">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_processors as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->username.' - '.$val->full_name }}</option>
                                        @endforeach
                                </select>
                            <div class="select_error" id="error-assigned_processor"></div>
                        </div>
                    </div>
                @endif

                @if(Auth::user()->isProcessor() || Auth::user()->isSuperAdmin())
                    <div class="form-group row" id="frm_input_assigned_supervisor">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.assigned_supervisor')}}<span class="text-red">&nbsp;*</span></label>
                                <select class="select2 select2-xs" 
                                        name="assigned_supervisor" 
                                        id="assigned_supervisor">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_supervisor as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->username.' - '.$val->full_name }}</option>
                                        @endforeach
                                </select>
                                <div class="select_error" id="error-assigned_supervisor"></div>
                        </div>
                    </div>
                @endif

                <div class="form-group row" id="frm_input_status">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="control-label">{{ __('page.status')}}<span class="text-red">&nbsp;*</span></label>
                            <select class="select2 select2-xs" 
                                    name="status" 
                                    id="status">   
                                    <option value="">{{ __('page.please_select') }}</option>
                                    @foreach($application_status as $key => $val)
                                        <option value="{{ $val['id'] }}">{{ __('page.'.$val['name']) }}</option>
                                    @endforeach
                            </select>
                        <div class="select_error" id="error-status"></div>
                    </div>
                </div>

                <div id="only_if_denied">
                    <div class="form-group row" id="frm_input_reason_denied">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="control-label">{{ __('page.reason_denied')}}<span class="text-red">&nbsp;*</span></label>
                                <select class="select2 select2-xs" 
                                        name="reason_denied" 
                                        id="reason_denied">   
                                        <option value="">{{ __('page.please_select') }}</option>
                                        @foreach($list_reason_denials as $key => $val)
                                            <option value="{{ $val->id }}">{{ $val->name }}</option>
                                        @endforeach
                                </select>
                            <div class="select_error" id="error-reason_denied"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="tr_id" id="tr_id" />

        <div class="modal fade colored-header colored-header-primary" id="passport_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.passport_identification_page') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close">       </span></button>
                    </div>
                    <div class="modal-body">
                        <table id="passport_files_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="airline_ticket_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.airline_ticket_issued') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close">       </span></button>
                    </div>
                    <div class="modal-body">
                        <table id="airline_ticket_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="tieza_official_receipt_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.tieza_official_receipt') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="travel_tax_receipt_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="additional_file_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.additional_file') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="additional_file_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        <div class="modal fade colored-header colored-header-primary" id="file_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog full-width">
                <div class="modal-content">
                    <div class="modal-header modal-header-colored">
                        <h3 class="modal-title">{{ __('page.type_applicant') }}</h3>
                        <button class="close md-close" type="button" data-dismiss="modal" aria-hidden="true"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <table id="file_tbl" class="table table-striped table-hover table-fw-widget" style="width:100%;">
                            <thead>
                                <tr class="text-center">
                                    <th style="width: 10%;">{{ __('page.action') }}</th>
                                    <th style="width: 60%;">{{ __('page.file_name') }}</th>
                                    <th style="width: 10%;">{{ __('page.file_type') }}</th>
                                    <th style="width: 10%;">{{ __('page.uploaded_at') }}</th>
                                    <th style="width: 10%;">{{ __('page.file') }}</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="modal-footer">&nbsp;</div>
                </div>
            </div>
        </div>

        @section('additional-scripts')
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#guardian_group').hide();
                    $('#cash_refund_fields').hide();
                    $('#only_if_denied').hide();
                    $('#div_other_reason').hide();

                    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                        $('#only_approved').hide();
                        $('#only_denied').hide();

                        @if(Auth::user()->isRegularAdmin())
                            $('#passport_identification_page').prop('disabled', true);
                            $('#ticket_booking_ref_no_').prop('disabled', true);
                            $('#additional_file').prop('disabled', true);
                            $('#assigned_processor').prop('disabled', true);
                            $('#assigned_supervisor').prop('disabled', true);
                            $('#file_0').prop('disabled', true);
                            $('#file_1').prop('disabled', true);
                            $('#file_2').prop('disabled', true);
                        @endif
                    @elseif(Auth::user()->isProcessor())
                        $('#frm_input_assigned_supervisor').hide();
                    @endif
                });

                $('#is_minor').on('change', function() {
                    $('#guardian_group').hide();

                    if($(this).val() == 1) $('#guardian_group').show();
                });

                $('#form_travel_tax_refund').on('change', function() {
                    $('#cash_refund_fields').hide();

                    if($(this).val() == 2) $('#cash_refund_fields').show();
                });

                $('#reason_refund').on('change', function() {
                    $('#div_other_reason').hide();

                    if($(this).val() == 6) $('#div_other_reason').show();
                });

                $('#list_type_applicant').on('change', function() {
                    $('#show_type_applicant').empty();

                    if($(this).val().length)
                    {
                        axios.get("{{ $based_url.'/section/json' }}", {
                            params: {
                                section_type : $(this).val()
                            }
                        })
                        .then(function(response){
                            const sections = response.data.sections;

                            if(sections)
                            {
                                var section_select_start = "<div class='form-group row' id='frm_input_type_applicant'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><label class='control-label'>{{ __('page.type_applicant') }}<span class='text-red'>&nbsp;*</span></label><select class='select2 select2-xs' name='type_applicant' id='type_applicant' onchange='load_section_upload()'><option value=''>{{ __('page.please_select') }}</option>";

                                var section_select_end = "</select><div class='select_error' id='error-type_applicant'></div></div></div>";

                                $.each(sections, function( key, value ) {
                                    section_select_start = section_select_start + "<option value='"+ value['id'] +"'>"+ value['name'] +"</option>";
                                });

                                $("#show_type_applicant").append(section_select_start + section_select_end);

                                $(".select2").select2({
                                  width: '100%'
                                });
                            }                        
                        })
                        .catch((error) => {
                            alert_warning(error.response.data.errors, 1500);
                        }).finally(function(){
                            axios.get("{{ $based_url.'/tr_application/json' }}", {
                                params: {
                                    tr_id : $('#tr_id').val(),
                                }
                            }).then(function(response){
                                const tr_application = response.data.tr_application;

                                $("#type_applicant").val('').trigger('change');

                                if(tr_application['section_id'] == $('#list_type_applicant').val())
                                {
                                   $("#type_applicant").val(tr_application['applicant_type_id']).trigger('change'); 
                                }

                                @if(Auth::user()->isRegularAdmin())
                                    $('#list_type_applicant').prop('disabled', true);
                                    $('#type_applicant').prop('disabled', true);
                                @elseif(Auth::user()->isProcessor())
                                    if(tr_application['is_complete'] == '1')
                                    {
                                        $('#list_type_applicant').prop('disabled', true);
                                        $('#type_applicant').prop('disabled', true);
                                    }
                                @endif
                            });
                        });
                    }   
                });

                function load_section_upload(type_applicant = null)
                {
                    $('#show_applicant_to_upload').empty();

                    if(type_applicant)
                    {
                        $('#type_applicant').val(type_applicant).trigger('change');
                    }
                    else
                    {
                        var type_applicant = $('#type_applicant').val();
                    }

                    if($('#type_applicant').val())
                    {
                        axios.get("{{ $based_url.'/section_upload/json' }}", {
                            params: {
                                section_id : type_applicant
                            }
                        })
                        .then(function(response){
                            const sections_uploads = response.data.sections_uploads;

                            if(sections_uploads)
                            {
                                $.each(sections_uploads, function( key, value ) {
                                    $("#show_applicant_to_upload").append("<div class='form-group row'><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><label class='control-label'>"+ value['description'] +"</label><div class='pull-right' id='link_file_" + key + "'><a class='' data-toggle='modal' data-target='#file_modal' id='additional_file_files' onclick=load_file('file_"+ key +"') href='javascript:void(0);'><span class='mdi mdi-hc-2x mdi-more'></span></a></div><input class='form-control form-control-xs' type='file' name='file_"+ key +"' id='file_"+ key +"' multiple='multiple' data-id='"+ value['id'] +"'></div><div class='col-lg-6 col-md-6 col-sm-12 col-xs-12'><div id='file_fn_"+ key +"'></div></div>");
                                });
                            }     

                            if($('#type_applicant').is(':disabled'))
                            {
                                $('#file_0').prop('disabled', true);
                                $('#file_1').prop('disabled', true);
                                $('#file_2').prop('disabled', true);
                                $('#file_3').prop('disabled', true);
                            }        
                        })
                        .catch((error) => {
                            alert_warning(error.response.data.errors, 1500);
                        });
                    }
                }

                @php
                    $upload_columns = array(['data' => 'action', 'sortable' => false], ['data' => 'file_name'], ['data' => 'file_type'], ['name' => 'upload_at.timestamp', 'type' => 'num', 'data' => [ "_" => 'upload_at.display', "sort" => 'upload_at' ]], ['data' => 'file']);
                @endphp

                function load_passport_attachment()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#passport_files_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=passport&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_airline_attachment()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#airline_ticket_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=airline&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_travel_tax_receipt()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#travel_tax_receipt_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=tieza_official_receipt&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_additional_attachment()
                {
                    var tr_id = $('#tr_id').val();

                    load_datables('#additional_file_tbl', "{!! $based_url.'/tr_application/datatables/attachment?selected_option=additional&section_id=0&tec_id=' !!}" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function load_file(file)
                {
                    var tr_id = $('#tr_id').val();

                    var section = $('#type_applicant').val();

                    load_datables('#file_tbl', "{!! $based_url.'/tr_application/datatables/attachment?' !!}selected_option="+ file +"&section_id="+ section +"&tec_id=" + tr_id, {!! json_encode($upload_columns) !!}, null);
                }

                function remove_item(option, id)
                {
                    var frm, text, title, success, formData;

                    frm = document.querySelector('#remove_form');

                    formData = new FormData();
                    formData.append('tau_id', id);

                    title = "{{ __('page.remove_attachment') }}";
                    text = "{{ __('page.remove_this') }}";
                    success = "{{ __('page.removed_successfully', ['attribute' => trans('page.remove_attachment')]) }}";
                        
                    const swal_continue = alert_continue(title, text, 'btn btn-danger', 'colored-header colored-header-danger');
                    swal_continue.then((result) => {
                        if(result.value){
                            axios.post(frm.action, formData)
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                    if(option == 'passport')
                                    {
                                        load_passport_attachment();
                                    }
                                    else if(option == 'airline')
                                    {
                                        load_airline_attachment();
                                    }
                                    else if(option == 'tieza_official_receipt')
                                    {
                                        load_travel_tax_receipt();
                                    }
                                    else if(option == 'additional')
                                    {
                                        load_additional_attachment();
                                    }
                                    else if(option == 'file_0')
                                    {
                                        load_file(option);
                                    }
                                    else if(option == 'file_1')
                                    {
                                        load_file(option);
                                    }
                                    else if(option == 'file_2')
                                    {
                                        load_file(option);
                                    }
                                    else if(option == 'file_3')
                                    {
                                        load_file(option);
                                    }
                                    else if(option == 'file_4')
                                    {
                                        load_file(option);
                                    }
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                            });
                        }
                    });
                }

                @if(Auth::user()->isSuperAdmin() || Auth::user()->isProcessor())
                    $("#add_record").click(function() {
                        var title, text;

                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.continue_this') }}";

                        const swal_continue = alert_continue(title, text);
                        swal_continue.then((result) => {
                            if(result.value){
                                window.location.href="{{ $cancel_url }}";
                            }
                        });
                    });
                @endif

                @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                    $("#delete_record").click(function() {
                        var frm, text, title, success, tr_id, formData;

                        tr_id = $('#tr_id').val();
                        frm = document.querySelector('#delete_form');

                        formData = new FormData();
                        formData.append('tr_id', tr_id);

                        title = "{{ __('page.delete_'.$option) }}";
                        text = "{{ __('page.delete_this') }}";
                        success = "{{ __('page.deleted_successfully', ['attribute' => trans('page.'.$option)]) }}";
                        
                        const swal_continue = alert_continue(title, text, 'btn btn-danger', 'colored-header colored-header-danger');
                        swal_continue.then((result) => {
                            if(result.value){
                                axios.post(frm.action, formData)
                                .then((response) => {
                                    const swal_success = alert_success(success, 1500);
                                    swal_success.then((value) => {
                                        window.location.href="{{ $cancel_url }}";
                                    });
                                })
                                .catch((error) => {
                                    const errors = error.response.data.errors;

                                    if(typeof(errors) == 'string')
                                    {
                                        alert_warning(errors);
                                    }
                                });
                            }
                        });
                    });

                    $("#generate_ar_form_353, #generate_denial_letter").click(function() {
                        var tr_id, status, file, supervisor;

                        tr_id = $('#tr_id').val();
                        status = $('#status').val();

                        @if(Auth::user()->isSuperAdmin())
                            supervisor = $('#assigned_supervisor').val();
                        @elseif(Auth::user()->isRegularAdmin())
                            supervisor = {{ Auth::user()->id }};
                        @endif

                        const swal_continue = alert_continue('Generate AR & Form 353 / Letter', 'Note: after generating file, it will automatically send to the client. <br><br>Are you sure you want to continue?');
                        swal_continue.then((result) => {
                            if(result.value){
                                Swal.fire({
                                    title: 'Generate File...',
                                    customClass: 'content-actions-center',
                                    buttonsStyling: true,
                                    allowOutsideClick: false,
                                    onOpen: function() {
                                        swal.showLoading();

                                        axios.get("{{ $application_url.'/tr_application/generate' }}", {
                                            params: {
                                                tr_id : tr_id,
                                                status : status,
                                                supervisor : supervisor
                                            }
                                        }).then(function(response){
                                            const acknowledge_receipt_file_name = response.data.acknowledge_receipt_file_name;
                                            const form_353_a_file_name = response.data.form_353_a_file_name;


                                            const tr_id = response.data.tr_id;
                                            
                                            const swal_success = alert_success('Generate File & Sent Successfully', 1500);
                                            swal_success.then((value) => {
                                                select_application(tr_id);
                                                window.open("{{ $based_url.'/tr/certificate/' }}" + tr_id + '/' + acknowledge_receipt_file_name, '_blank');
                                                window.open("{{ $based_url.'/tr/certificate/' }}" + tr_id + '/' + form_353_a_file_name, '_blank');
                                            });
                                        }).catch((error) => {
                                            alert_warning(error.response.data.errors, 1500);
                                        });
                                    }
                                });
                            }
                        });
                    });
                @endif

                $('#status').on('change', function() {
                    $('#only_if_denied').hide();

                    @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                        $('#only_approved').hide();
                        $('#only_denied').hide();
                    @endif

                    if($(this).val() == '3') 
                    {
                        $('#only_if_denied').show();
                    }
                });

                $("#save_btn").click(function() {
                    var frm, text, title, success, tr_id, formData, file_tieza_official_receipt, file_passport_identification_page, file_ticket_booking_ref_no_, file_0, file_1, file_2, file_3, file_0_id, file_1_id, file_2_id, file_3_id, file_4_id;

                    tr_id = $('#tr_id').val();

                    frm = document.querySelector('#save_form');

                    formData = new FormData();

                    if(tr_id)
                    {
                        formData.append('tr_id', tr_id);
                        title = "{{ __('page.edit_'.$option) }}";
                        text = "{{ __('page.update_this') }}";
                        success = "{{ __('page.updated_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    }
                    else
                    {
                        title = "{{ __('page.add_'.$option) }}";
                        text = "{{ __('page.add_this') }}";
                        success = "{{ __('page.added_successfully', ['attribute' => trans('page.'.$option)]) }}";
                    }

                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {
                        clearErrors();
                        if(result.value){
                            formData.append("date_application", $('#date_application').val());

                            formData.append("last_name", $("#last_name").val());
                            formData.append("first_name", $("#first_name").val());
                            formData.append("middle_name", $("#middle_name").val());

                            formData.append("is_minor", $("#is_minor").val());

                            if($('#is_minor').val() == '1')
                            {
                                formData.append("guardian_last_name", $("#guardian_last_name").val());
                                formData.append("guardian_first_name", $("#guardian_first_name").val());
                                formData.append("guardian_middle_name", $("#guardian_middle_name").val());
                            }

                            formData.append("mobile_no", $("#mobile_no").val());
                            formData.append("email_address", $("#email_address").val());

                            formData.append("passport_no", $("#passport_no").val());
                            formData.append("ticket_booking_ref_no", $("#ticket_booking_ref_no").val());
                            formData.append("date_ticket_issued", $("#date_ticket_issued").val());
                            formData.append("country_designation", $("#country_designation").val());
                            formData.append("date_flight", $("#date_flight").val());
                            formData.append("airlines_name", $("#airlines_name").val());
                            formData.append("amount_travel_tax_collected", $("#amount_travel_tax_collected").val());
                            formData.append("list_type_applicant", $("#list_type_applicant").val());

                            if($('#type_applicant').length) formData.append("type_applicant", $("#type_applicant").val());

                            file_passport_identification_page = document.querySelector('#passport_identification_page');
                            for(var i = 0; i < file_passport_identification_page.files.length; i++ ){
                                let file = file_passport_identification_page.files[i];
                                formData.append('passport_identification_page[]', file);
                            }

                            file_ticket_booking_ref_no_ = document.querySelector('#ticket_booking_ref_no_');
                            for(var i = 0; i < file_ticket_booking_ref_no_.files.length; i++ ){
                                let file = file_ticket_booking_ref_no_.files[i];
                                formData.append('ticket_booking_ref_no_[]', file);
                            }

                            file_tieza_official_receipt = document.querySelector('#tieza_official_receipt');
                            for(var i = 0; i < file_tieza_official_receipt.files.length; i++ ){
                                let file = file_tieza_official_receipt.files[i];
                                formData.append('tieza_official_receipt[]', file);
                            }

                            file_additional_file = document.querySelector('#additional_file');
                            for(var i = 0; i < file_additional_file.files.length; i++ ){
                                let file = file_additional_file.files[i];
                                formData.append('additional_file[]', file);
                            }

                            if($('#file_0').length)
                            {
                                file_0 = document.querySelector('#file_0');
                                for(var i = 0; i < file_0.files.length; i++ ){
                                    let file = file_0.files[i];
                                    formData.append('file_0[]', file);
                                }

                                file_0_id = $('#file_0').data('id');
                                formData.append('file_0_id', file_0_id);
                            }

                            if($('#file_1').length)
                            {
                                file_1 = document.querySelector('#file_1');
                                for(var i = 0; i < file_1.files.length; i++ ){
                                    let file = file_1.files[i];
                                    formData.append('file_1[]', file);
                                }

                                file_1_id = $('#file_1').data('id');
                                formData.append('file_1_id', file_1_id);
                            }

                            if($('#file_2').length)
                            {
                                file_2 = document.querySelector('#file_2');
                                for(var i = 0; i < file_2.files.length; i++ ){
                                    let file = file_2.files[i];
                                    formData.append('file_2[]', file);
                                }

                                file_2_id = $('#file_2').data('id');
                                formData.append('file_2_id', file_2_id);
                            }

                            if($('#file_3').length)
                            {
                                file_3 = document.querySelector('#file_3');
                                for(var i = 0; i < file_3.files.length; i++ ){
                                    let file = file_3.files[i];
                                    formData.append('file_3[]', file);
                                }

                                file_3_id = $('#file_3').data('id');
                                formData.append('file_3_id', file_3_id);
                            }

                            if($('#file_4').length)
                            {
                                file_4 = document.querySelector('#file_4');
                                for(var i = 0; i < file_4.files.length; i++ ){
                                    let file = file_4.files[i];
                                    formData.append('file_4[]', file);
                                }

                                file_4_id = $('#file_4').data('id');
                                formData.append('file_4_id', file_4_id);
                            }

                            formData.append("form_travel_tax_refund", $("#form_travel_tax_refund").val());

                            if($("#form_travel_tax_refund").val() == '2')
                            {
                                formData.append("bank_name", $("#bank_name").val());
                                formData.append("bank_account_name", $("#bank_account_name").val());
                                formData.append("bank_account_no", $("#bank_account_no").val());

                                formData.append("refund_amount", $("#refund_amount").val());
                                formData.append("reason_refund", $("#reason_refund").val());

                                if($("#reason_refund").val() == '6') formData.append("other_reasons", $("#other_reasons").val());
                            }

                            formData.append("tieza_official_receipt_no", $("#tieza_official_receipt_no").val());
                            formData.append("payment_date", $("#date_payment").val());

                            @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                                formData.append("assigned_processor", $("#assigned_processor").val());

                                @if(Auth::user()->isSuperAdmin())
                                    formData.append("assigned_supervisor", $("#assigned_supervisor").val());
                                @endif
                            @endif

                            formData.append("status", $("#status").val());
                            if($("#status").val() == '3') formData.append("reason_denied", $("#reason_denied").val());

                            axios.post(frm.action, formData, {
                                headers: {
                                  'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                    clearInputs();

                                    if(tr_id)
                                    {
                                        $('#filter_btn').click();
                                        select_application(tr_id);
                                    }
                                    else
                                    {
                                        swal_success0 = alert_success("Application No : " + response.data.success.app_id);
                                        swal_success0.then((value) => {
                                            const swal_continue = alert_continue("{{ __('page.add_'.$option) }}", "Do you want to add another {{ trans('page.'.$option) }}?");
                                            swal_continue.then((result) => {
                                                if(!result.value) {
                                                    window.location.href = "{{ $cancel_url }}";
                                                }
                                            });
                                        });
                                    }
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    const firstItem = Object.keys(errors)[0];

                                    const split_firstItem = firstItem.split('.');
                                    const firstItemSplit = split_firstItem[0];

                                    const firstItemDOM = document.getElementById(firstItemSplit);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();

                                    alert_warning("{{ __('page.check_inputs') }}", 1500);

                                    showErrors(firstItemSplit, firstErrorMessage, firstItemDOM, ['is_minor', 'applicant_name', 'country_designation', 'airlines_name', 'amount_travel_tax_collected', 'list_type_applicant', 'type_applicant', 'form_travel_tax_refund', 'bank_name', 'reason_refund', 'assigned_processor', 'status', 'reason_denied', 'assigned_supervisor']);
                                }
                            });   
                        }
                    })
                });

                function select_application(tr_id)
                {
                    clearErrors();

                    is_complete(false);

                    $('#float_passport_identification_link').empty();
                    $('#float_airline_ticket_link').empty();
                    $('#float_additional_file_link').empty();
                    $('#float_tieza_official_receipt_link').empty();
                    
                    axios.get("{{ $based_url.'/tr_application/json' }}", {
                        params: {
                            tr_id : tr_id,
                        }
                    })
                   .then(function(response){
                        const tr_application = response.data.tr_application;

                        $("#tr_id").val(tr_application['id']);
                        $("#application_no").val(tr_application['app_id']);

                        $('#is_minor').val(tr_application['is_minor']).trigger('change');

                        $('#last_name').val(tr_application['last_name']);
                        $('#first_name').val(tr_application['first_name']);
                        $('#middle_name').val(tr_application['middle_name']);

                        if(tr_application['is_minor'] == '1')
                        {
                            $('#guardian_last_name').val(tr_application['guardian_last_name']);
                            $('#guardian_first_name').val(tr_application['guardian_first_name']);
                            $('#guardian_middle_name').val(tr_application['guardian_middle_name']);
                        }

                        $('#mobile_no').val(tr_application['mobile_no']);
                        $('#email_address').val(tr_application['email']);

                        $('#passport_no').val(tr_application['passport_no']);
                        $("#date_application").val(tr_application['date_application']).trigger('change');
                        $("#date_validity").val(tr_application['date_validity']).trigger('change');

                        $("#ticket_booking_ref_no").val(tr_application['ticket_no']);
                        $("#date_ticket_issued").val(tr_application['date_ticket_issued']).trigger('change'); 
                        $("#country_designation").val(tr_application['country_id']).trigger('change');
                        $("#date_flight").val(tr_application['date_flight']).trigger('change');
                        $('#airlines_name').val(tr_application['airlines_id']).trigger('change');
                        $('#amount_travel_tax_collected').val(tr_application['travel_tax_amount']).trigger('change');
                        $('#list_type_applicant').val(tr_application['section_id']).trigger('change');

                        $('#bank_name').val(tr_application['bank_id']).trigger('change');
                        $('#bank_account_name').val(tr_application['bank_account_name']).trigger('change');
                        $('#bank_account_no').val(tr_application['bank_account_number']).trigger('change');
                        $('#refund_amount').val(tr_application['refund_amount']);
                        $('#reason_refund').val(tr_application['reason_refund']).trigger('change');
                        $('#other_reasons').val(tr_application['reason_refund_others']);

                        $('#tieza_official_receipt_no').val(tr_application['tieza_official_receipt_no']);
                        $('#date_payment').val(tr_application['payment_date']).trigger('change');

                        $('#form_travel_tax_refund').val(tr_application['type_travel_tax_refund']).trigger('change');

                        $("#status").val(tr_application['status']).trigger('change');
                        $("#reason_denied").val(tr_application['denial_id']).trigger('change');

                        $('#float_passport_identification_link').append('<a class="" data-toggle="modal" data-target="#passport_modal" id="passport_files" onclick="load_passport_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                        $('#float_airline_ticket_link').append('<a class="" data-toggle="modal" data-target="#airline_ticket_modal" id="airline_ticket_files" onclick="load_airline_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                        $('#float_tieza_official_receipt_link').append('<a class="" data-toggle="modal" data-target="#tieza_official_receipt_modal" id="tieza_official_receipt_files" onclick="load_travel_tax_receipt()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');

                        $('#float_additional_file_link').append('<a class="" data-toggle="modal" data-target="#additional_file_modal" id="additional_file_files" onclick="load_additional_attachment()" href="javascript:void(0);"><span class="mdi mdi-hc-2x mdi-more"></span></a>');
        
                        $('#add_record').addClass('disabled');  

                        $("#assigned_supervisor").val(tr_application['supervisor_id']).trigger('change');

                        @if(Auth::user()->isSuperAdmin() || Auth::user()->isRegularAdmin())
                            $("#assigned_processor").val(tr_application['assign_processor_id']).trigger('change');

                             @if(Auth::user()->isSuperAdmin())
                                $('#delete_record').removeClass('disabled');   
                             @endif
                        @endif

                        if(tr_application['status'] == 3) $('#only_denied').show();

                        if(tr_application['status'] == 2) $('#only_approved').show();

                        @if(Auth::user()->isRegularAdmin() || Auth::user()->isProcessor())
                            @if(Auth::user()->isRegularAdmin())
                                if(tr_application['status'] == 1) 
                                {
                                    is_complete(true);
                                }
                                else
                                {
                                    if(tr_application['is_complete'] == '1') is_complete(true);
                                }
                            @else
                                if(tr_application['is_complete'] == '1') is_complete(true);
                            @endif
                        @endif
                    }).catch((error) => {
                        alert_warning(error.response.data.errors, 1500);
                    });
                }

                function is_complete(status)
                {
                    $('#last_name').prop('disabled', status);
                    $('#first_name').prop('disabled', status);
                    $('#middle_name').prop('disabled', status);
                    $('#is_minor').prop('disabled', status);
                    $('#guardian_first_name').prop('disabled', status);
                    $('#guardian_middle_name').prop('disabled', status);
                    $('#guardian_last_name').prop('disabled', status);
                    $('#passport_no').prop('disabled', status);
                    $('#mobile_no').prop('disabled', status);
                    $('#email_address').prop('disabled', status);
                    $('#ticket_booking_ref_no').prop('disabled', status);
                    $('#date_ticket_issued').prop('disabled', status);
                    $('#country_designation').prop('disabled', status);
                    $('#date_validity').prop('disabled', status);
                    $('#date_flight').prop('disabled', status);
                    $('#airlines_name').prop('disabled', status);
                    $('#amount_travel_tax_collected').prop('disabled', status);
                    $('#list_type_applicant').prop('disabled', status);

                    $('#bank_name').prop('disabled', status);
                    $('#bank_account_name').prop('disabled', status);
                    $('#bank_account_no').prop('disabled', status);
                    $('#refund_amount').prop('disabled', status);
                    $('#reason_refund').prop('disabled', status);
                    $('#other_reasons').prop('disabled', status);

                    $('#tieza_official_receipt_no').prop('disabled', status);
                    $('#date_payment').prop('disabled', status);

                    $('#form_travel_tax_refund').prop('disabled', status);

                    $('#status').prop('disabled', status);
                    $('#reason_denied').prop('disabled', status);

                    @if(Auth::user()->isRegularAdmin())
                        $('#passport_identification_page').prop('disabled', true);
                        $('#tieza_official_receipt').prop('disabled', true);
                        $('#ticket_booking_ref_no_').prop('disabled', true);
                        $('#additional_file').prop('disabled', true);
                        $('#assigned_processor').prop('disabled', true);
                        $('#assigned_supervisor').prop('disabled', true);
                    @else
                        $('#passport_identification_page').prop('disabled', status);
                        $('#tieza_official_receipt').prop('disabled', status);
                        $('#ticket_booking_ref_no_').prop('disabled', status);
                        $('#additional_file').prop('disabled', status);
                        $('#assigned_processor').prop('disabled', status);
                    @endif

                    @if(Auth::user()->isProcessor())
                        $('#assigned_supervisor').prop('disabled', true);
                        $('#frm_input_assigned_supervisor').show();
                    @endif
                    
                    if(status == true)
                    {
                        $('#only_approved').hide();
                        $('#only_denied').hide();
                        $('#save_btn').hide();
                    }
                    else
                    {
                        $('#save_btn').show();
                    } 
                }
            </script>
        @endsection 