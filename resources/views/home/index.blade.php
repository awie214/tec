@extends('layouts.master-auth')

@section('content')
    <div class="page-head">
        <div class="page-head-title">Welcome! {{ Auth::user()->getFullNameFML() }}</div>
        <div class="page-description">@if(!Auth::user()->isTravelTaxUser()) Online Travel Tax Certificate Application & @endif Online Travel Tax Refund Application</div>
    </div>
    <div class="row">
        <div class="col-xl-9 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center">
        	<img src="{{ asset('img/tieza-logo.png') }}" alt="logo" class="ml-0 img-fluid" style="height: 35em;">
        </div>
    </div>
@endsection