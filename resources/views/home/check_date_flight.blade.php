@extends('layouts.master-guest')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle-v1.7.1/src/assets/lib/sweetalert2/sweetalert2.min.css') }}"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-xl-10 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center">
            <div class="splash-container">
                <div class="card card-border-color card-border-color-primary">
                    <div class="card-header">
                        <span class="splash-title">Please enter date of flight to validate.</span>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="date" class="form-control form-control-xs" name="date_flight" id="date_flight" placeholder="Date of Flight">
                            </div>
                        </div>

                        <a href="javascript:void(0);" class="btn btn-space btn-primary" title="Validate" id="validate_btn"><i class="icon icon-left mdi mdi-check"></i>Validate</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url('/check_date_flight'), 'frm_id' => 'validate_form'])
@endsection

@section('scripts')
    <script src="{{ asset('beagle-v1.7.1/src/assets/lib/sweetalert2/sweetalert2.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('beagle-v1.7.1/src/js/app-ui-sweetalert2.js') }}"></script>
    <script src="{{ asset('axios/axios.min.js')}}"></script>
    <script src="{{ asset('js/custom.js')}}"></script>

    @yield('additional-scripts')

    <script type="text/javascript">
        $("#validate_btn").click(function() {
                    var frm, text, title, success, formData;

                    frm = document.querySelector('#validate_form');

                    formData = new FormData();

                    title = "Validate Date of Flight";
                    text = "{{ __('page.continue_this') }}";
                    success = "The Date of flight is valid";
                    
                    const swal_continue = alert_continue(title, text);
                    swal_continue.then((result) => {
                        clearErrors();
                        if(result.value){
                            formData.append("date_flight", $("#date_flight").val());
                    
                            axios.post(frm.action, formData, {
                                headers: {
                                  'Content-Type': 'multipart/form-data'
                                }
                            })
                            .then((response) => {
                                const swal_success = alert_success(success, 1500);
                                swal_success.then((value) => {
                                    clearInputs();

                                    window.open("https://docs.google.com/forms/d/e/1FAIpQLSc346n84LFYw0A-22PFnjPbSRthUXYPlGQwleoFeEFSVI38cw/viewform");
                                });
                            })
                            .catch((error) => {
                                const errors = error.response.data.errors;

                                console.log(error);

                                if(typeof(errors) == 'string')
                                {
                                    alert_warning(errors);
                                }
                                else
                                {
                                    const firstItem = Object.keys(errors)[0];

                                    const split_firstItem = firstItem.split('.');
                                    const firstItemSplit = split_firstItem[0];

                                    const firstItemDOM = document.getElementById(firstItemSplit);
                                    const firstErrorMessage = errors[firstItem][0];

                                    firstItemDOM.scrollIntoView();

                                    alert_warning("{{ __('page.check_inputs') }}", 1500);

                                    showErrors(firstItemSplit, firstErrorMessage, firstItemDOM);
                                }
                            });   
                        }
                    })
                });
    </script>
@endsection