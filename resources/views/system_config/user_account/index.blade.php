@extends('layouts.master-auth')

@section('css')
    @include('layouts.auth-partials.datatables-css')
@endsection

@section('content')
    <aside class="page-aside">
        <div class="aside-content">
            <div class="aside-header py-2 d-xl-none d-lg-none">
                    <button class="navbar-toggle" data-target="#aside-spy" data-toggle="collapse" type="button"><span class="icon mdi mdi-caret-down"></span></button>&nbsp;&nbsp;{{ __('page.list_accounts') }}
            </div>
            <div class="aside-nav collapse" id="aside-spy">
                @include('others.list_user_account')
            </div>
        </div>
    </aside>

    @php 
        $data = [
        'option' => $option, 
        'title' => $option, 
        'has_icon' => $icon, 
        'has_file' => $file,
        'has_footer' => 'yes', 
        'isSave' => 'yes',
        'cancel_url' => url('system_config/'.$option)
        ];

        $data = array_merge($data, ['add_url' => 'javascript:void(0);', 'delete_url' => 'javascript:void(0);'])
    @endphp

    @include('others.main_content', $data)
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url('system_config/'.$option.'/save'), 'frm_id' => 'save_form'])
    @include('others.form_request', ['frm_method' => 'POST', 'frm_action' => url('system_config/'.$option.'/delete'), 'frm_id' => 'delete_form'])
@endsection

@section('scripts')
    @yield('additional-scripts')

    @include('layouts.auth-partials.datatables-scripts')

    <script type="text/javascript">
        $(document).ready(function(){
            $('#main-content').addClass('be-aside');
        });
    </script>
@endsection