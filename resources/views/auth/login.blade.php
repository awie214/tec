@extends('layouts.master-guest')

@section('css')
    <style type="text/css">
        .event-background{
            background: url("img/bg-triangle-0.png");
            background-position: center;
            background-size: cover;
            height: 100%;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col text-center text-light h-25"><h2 class="mb-4">Online TEC Processing System</h2></div>
        </div>

        <div class="row">
            <div class="col-xl-6 offset-xl-1 col-lg-7 col-md-7 col-sm-12 col-xs-12 mt-8 text-center">
                <img src="{{ asset('img/tieza-logo.png') }}" alt="logo" class="ml-0 img-fluid" style="height: 35em;">
            </div>
            <div class="col-xl-3 col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="splash-container">
                    <div class="card card-border-color card-border-color-primary">
                        <div class="card-header">
                            <span class="splash-title">Please enter your user information.</span>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <input id="username" type="text" placeholder="Username" autocomplete="off" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                                    @if($errors->has('username'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('username') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                                    @if($errors->has('password'))
                                        <ul class="parsley-errors-list filled" id="parsley-id-5">
                                            <li class="parsley-required">{{ $errors->first('password') }}</li>
                                        </ul>
                                    @endif
                                </div>

                                <div class="form-group row login-tools">
                                    <div class="col-6 login-remember">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" {{ old('remember') ? 'checked' : '' }} id="checkbox1">
                                            <label class="custom-control-label" for="checkbox1">Remember Me</label>
                                        </div>
                                    </div>
                                    <div class="col-6 login-forgot-password">
                                        <p><a href="{{ url('password/reset') }}">Forgot Password?</a></p>
                                    </div>
                                </div>

                                <div class="form-group login-submit">
                                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl"><span class="mdi mdi-sign-in"></span>&nbsp;Sign me in</button>
                                </div>

                                <hr />

                                <p class="pt-1 pb-0">Trouble logging in? Please contact <a href="#">traveltax.helpdesk@tieza.gov.ph</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection