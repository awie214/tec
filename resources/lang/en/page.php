<?php

return [

// page
'add_new_record' => 'Add New Record',
'update_record' => 'Update Record',
'delete_record' => 'Delete Record',
'action' => 'Action',
'options' => 'Options',
'view' => 'View',
'edit' => 'Edit',
'delete' => 'Delete',
'save' => 'Save',
'cancel' => 'Cancel',
'filter' => 'Filter',
'verify' => 'Verify',
'send_mail' => 'Send Mail',

// alerts
'added_successfully' => 'The :attribute has been added successfully!',
'updated_successfully' => 'The :attribute has been updated successfully!',
'deleted_successfully' => 'The :attribute has been deleted successfully!',
'removed_successfully' => 'The :attribute has been removed successfully!',


'check_inputs' => 'Please check input fields.',
'no_record_found' => 'No Record Found.',

'add_this' => 'Are you sure you want to add this?',
'delete_this' => 'Are you sure you want to delete this?',
'update_this' => 'Are you sure you want to update this?',
'continue_this' => 'Are you sure you want to continue?',
'download_this' => 'Are you sure you want to download this?',
'logout_this' => 'Are you sure you want to logout this?',
'exit_this' => 'Are you sure you want to exit?',
'deactivate_this' => 'Are you sure you want to deactivate this?',

'apply_this' => 'Are you sure you want to apply this?',
'approve_this' => 'Are you sure you want to approve this?',
'cancel_this' => 'Are you sure you want to cancel?',
'continue_this' => 'Do you want to continue?',

'menu' => 'Menu',
'home' => 'Home',
'account' => 'Account',
'system_config' => 'System Config',
'user_account' => 'User Account',

'required_fields' => 'Required Fields (*)',

'filter_by' => 'Filter by',
'search_name' => 'Search Name',
'active' => 'Active',
'inactive' => 'Inactive',


'logout' => 'Logout',

'please_select' => 'Please select option',

'edit_account' => 'Edit Account',

'list_accounts' => 'List of Accounts',

'add_user_account' => 'Add New User Account',
'edit_user_account' => 'Edit User Account',
'delete_user_account' => 'Delete User Account',


'full_name' => 'Full Name',
'applicant_name' => 'Applicant (Full Name)',
'last_name' => 'Last Name',
'first_name' => 'First Name',
'middle_name' => 'Middle Name',
'mobile_no' => 'Mobile Number',
'username' => 'Username',
'username_generated' => 'Username (System Generated)',
'email_address' => 'Email Address',
'old_password' => 'Old Password',
'new_password' => 'New Password',
'confirm_password' => 'Confirm New Password',
'user_level' => 'User Level',
'user_status' => 'User Status',

'type_applicant' => 'Type of Applicant',
'date_ticket_issued' => 'Date when Ticket was issued',

'super_admin' => 'System Admin',
'regular_admin' => 'Supervisor',
'processor' => 'Processor',
'user' => 'Applicant',


'review_validation' => 'Review / Validation',
'application' => 'Application',
'overview' => 'Overview',
'tec_application' => 'TEC Application',
'tec_application_description' => 'Travel Tax Exemption Certificate Application',

'list_application' => 'List of Application',
'add_tec_application' => 'Add New TEC Application',
'edit_tec_application' => 'Edit TEC Application',
'delete_tec_application' => 'Delete TEC Application',

'date_application' => 'Date of Application',
'passport_no' => 'Passport Number',
'ticket_booking_ref_no' => 'Ticket No. / Booking Ref. No.',
'date_flight' => 'Date of Flight',
'section' => 'Section',
'country_designation' => 'Country of Destination',
'status' => 'Status',
'upload_addtl_documents' => 'Upload Additional Documents',

'pending' => 'Pending',
'approved' => 'Approved',
'awaiting_documents' => 'Awaiting Additional Documents',
'awaiting_approval' => 'Awaiting Approval',
'completed' => 'Completed / Processed',

'denied' => 'Denied',
'reason_denied' => 'Reason of Denial',

'application_no' => 'Application No.',

'id_picture_2x2' => '2x2 ID Picture',
'passport_identification_page' => 'Passport Identification Page',
'passport_identification_page.*' => 'Passport Identification Page',

'ascending' => 'Sort by Ascending',
'descending' => 'Sort by Descending',

'oldest' => 'Sort by oldest',
'newest' => 'Sort by newest',

'search_application' => 'Search Application',
'assigned_processor' => 'Assigned Processor',

'print_preview' => 'Print Preview',
'send_mail' => 'Send Mail',

'airline_ticket_issued' => 'Airline Ticket Issued',
'airline_ticket_issued.*' => 'Airline Ticket Issued',


'airlines_name' => 'Airlines Name',

'date_validity' => 'TEC Validity',

'generate_certificate' => 'Generate Certificate',
'generate_denial_letter' => 'Generate Denial Letter',

'search_name' => 'Search Name',
'assigned_supervisor' => 'Assigned Supervisor',
'additional_file' => 'Additional File',
'additional_file.*' => 'Additional File',

'tr_application' => 'TR Application',
'tr_application_description' => 'Travel Tax Refund Application',
'add_tr_application' => 'Add New TR Application',
'edit_tr_application' => 'Edit TR Application',
'delete_tr_application' => 'Delete TR Application',

'parent_guadian' => 'Parents Name/Guardian',
'is_minor' => 'Is Minor?',
'yes' => 'Yes',
'no' => 'No',

'guardian_last_name' => 'Guardian Last Name',
'guardian_first_name' => 'Guardian First Name',
'guardian_middle_name' => 'Guardian Middle Name',

'tieza_official_receipt' => 'TIEZA Offical Receipt',

'tieza_official_receipt_note_0' => 'If paid directly to TIEZA (Copy of the TIEZA Official Receipt passenger copy)',
'tieza_official_receipt_note_1' => 'If paid through the Online Travel Tax Payments (OTTPS) (Acknowledgement Receipt)',

'amount_travel_tax_collected' => 'Amount of Travel Tax Collected',

'tec' => 'Travel Tax Exemption Certificate',


'form_travel_tax_refund' => 'Please select your preferred form of the Travel Tax Refund:',
'travel_tax_certificate' => 'Travel Tax Certificate',
'cash_refund' => 'Cash Refund',

'bank_name' => 'Bank Name',
'bank_account_name' => 'Bank Account Name',
'bank_account_no' => 'Bank Account No.',

'file_name' => 'File Name',
'file' => 'File',
'file_type' => 'File Type',
'uploaded_at' => 'Date Uploaded',

'remove_attachment' => 'Remove Attachment',
'remove_this' => 'Are you sure you want to remove this?',
'remove_successfully' => 'Remove Successfully',

'list_type_applicant' => 'Section',

'tec_dashboard' => 'Dashboard',
'tec_dashboard_description' => 'Travel Tax Exemption Certificate Dashboard',

'tr_dashboard' => 'Dashboard',
'tr_dashboard_description' => 'Travel Tax Refund Dashboard',

'tec_' => 'TEC',
'tr_' => 'TR',

'refund_amount' => 'Refund Amount',
'reason_refund' => 'Reason for Refund',
'other_reasons' => 'Other Reasons',


'tieza_official_receipt_no' => 'TIEZA Official Receipt No.',
'date_payment' => 'Date of Payment',


'generate_ar_form_353' => 'Generate AR & Form 353-A',

'travel_tax_user' => 'Travel Tax',
'review_approved_application' => 'Approved Application',

'generated_form_353' => 'Generated Form 353',
'form_353' => 'Form 353',

'tec_report' => 'Report',
'tec_report_description' => 'Travel Tax Exemption Certificate Report',
'airlines_passenger' => 'Airlines Passenger',

'report_name' => 'Report Name',

'tr_report' => 'Report',

'month' => 'Month',
'year' => 'Year',

'preview' => 'Preview',

'list_airlines' => 'List of Airlines',
'show_all' => 'Show All',
'report' => 'Report',

'summary_section_tec' => 'Summary Report'
];  