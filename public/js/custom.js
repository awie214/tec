	
	$(document).ready(function(){
        App.init();

        $(".2nd_sub, .3rd_sub").on('click', function(){
            localStorage.clear();
        })
    });

    function clearErrors() 
    {
        const errorMessages = document.querySelectorAll('.text-danger')
        errorMessages.forEach((element) => element.textContent = '')

        const formControls = document.querySelectorAll('.form-control')
        formControls.forEach((element) => element.classList.remove('border', 'border-danger'))

        const formGroups = document.querySelectorAll('.form-group')
        formGroups.forEach((element) => element.classList.remove('has-error'))
    }

    function clearInputs()
    {
        $(".form-group input[type=text]").val("");
        $(".form-group input[type=email]").val("");
        $(".form-group input[type=password]").val("");
        $(".form-group input[type=number]").val("");
        $(".form-group input[type=hidden]").val("");
        $(".form-group input[type=date]").val("");
        $(".form-group input[type=file]").val("");
        $(".form-group select").val("").trigger('change');
    }

    function showErrors(firstItem, firstErrorMessage, firstItemDOM, fields = [])
    {
        if(jQuery.inArray(firstItem, fields) !== -1)
        {
            $('#frm_input_' + firstItem).addClass('has-error')
            document.getElementById('error-' + firstItem).insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
        }
        else
        {
            firstItemDOM.insertAdjacentHTML('afterend', `<div class="text-danger">${firstErrorMessage}</div>`)
                            firstItemDOM.classList.add('border', 'border-danger')
        }
    }

    function scrollto(id)
    {
        $('html, body').animate({
            scrollTop: $(id).offset().top
        }, 2000);
    }