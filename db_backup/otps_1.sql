-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2020 at 03:14 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `otps`
--

-- --------------------------------------------------------

--
-- Table structure for table `airlines`
--

CREATE TABLE `airlines` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `airlines`
--

INSERT INTO `airlines` (`id`, `name`) VALUES
(1, 'Aero Mexico'),
(2, 'Aeroflot Russian Airlines'),
(3, 'Air Asia Berhad'),
(4, 'Air Busan'),
(5, 'Air Canada'),
(6, 'Air China Limited'),
(7, 'Air Macau'),
(8, 'Air Mauritius'),
(9, 'Air New Zealand'),
(10, 'Air Niugini'),
(11, 'Aleson Shipping Lines, Inc.'),
(12, 'All Nippon Airways'),
(13, 'American Airlines'),
(14, 'Asiana Airlines'),
(15, 'Austrian Airlines'),
(16, 'Bangkok Airways'),
(17, 'British Airways'),
(18, 'Cathay Pacific Airways'),
(19, 'Cebu Pacific Air'),
(20, 'China Airlines'),
(21, 'China Eastern Airlines'),
(22, 'China Southern Airlines'),
(23, 'Delta Airlines'),
(24, 'El-Al Israel Airlines'),
(25, 'Emirates Airlines'),
(26, 'Ethiopian Airlines'),
(27, 'Etihad Airways'),
(28, 'EVA Airways'),
(29, 'Fiji Airways'),
(30, 'Finnair'),
(31, 'Garuda Indonesia Airlines'),
(32, 'Gulf Air'),
(33, 'Hahn Air'),
(34, 'Hawaiian Airlines'),
(35, 'Hongkong Airlines'),
(36, 'Hongkong Dragon Airlines Ltd.'),
(37, 'Japan Airlines'),
(38, 'Jeju Air'),
(39, 'Jin Air - Cebu'),
(40, 'Jin Air - Clark'),
(41, 'KLM Royal Dutch Airlines'),
(42, 'Kenya Airways'),
(43, 'Korean Air'),
(44, 'Kuwait Airways'),
(45, 'Lufthansa German Airlines'),
(46, 'Malaysia Airlines'),
(47, 'Oman Air'),
(48, 'Pakistan Airlines'),
(49, 'PAL  Express'),
(50, 'Philippine Air Asia'),
(51, 'Philippine Airlines'),
(52, 'Qantas Airways'),
(53, 'Qatar Aiways'),
(54, 'Royal Brunei Airlines'),
(55, 'Royal Jordanian Airlines'),
(56, 'Saudi Arabian Airlines'),
(57, 'Scoot Tiger Air'),
(58, 'Silkair'),
(59, 'Singapore Airlines'),
(60, 'South African Airways'),
(61, 'Swiss International Air Lines'),
(62, 'Tap Potugal'),
(63, 'Thai Airways'),
(64, 'Turkish Airlines'),
(65, 'United Airlines'),
(66, 'Ukraine International Airlines'),
(67, 'Vietnam Airlines'),
(68, 'Wallem Philippine Shipping'),
(69, 'Xiamen Air');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PH', 'Philippines', NULL, 1, NULL, NULL, NULL, NULL),
(2, 'AF', 'Afghanistan', NULL, 1, NULL, NULL, NULL, NULL),
(3, 'AL', 'Albania', NULL, 1, NULL, NULL, NULL, NULL),
(4, 'DZ', 'Algeria', NULL, 1, NULL, NULL, NULL, NULL),
(5, 'AS', 'American Samoa', NULL, 1, NULL, NULL, NULL, NULL),
(6, 'AD', 'Andorra', NULL, 1, NULL, NULL, NULL, NULL),
(7, 'AO', 'Angola', NULL, 1, NULL, NULL, NULL, NULL),
(8, 'AI', 'Anguilla', NULL, 1, NULL, NULL, NULL, NULL),
(9, 'AQ', 'Antarctica', NULL, 1, NULL, NULL, NULL, NULL),
(10, 'AG', 'Antigua and/or Barbuda', NULL, 1, NULL, NULL, NULL, NULL),
(11, 'AR', 'Argentina', NULL, 1, NULL, NULL, NULL, NULL),
(12, 'AM', 'Armenia', NULL, 1, NULL, NULL, NULL, NULL),
(13, 'AW', 'Aruba', NULL, 1, NULL, NULL, NULL, NULL),
(14, 'AU', 'Australia', NULL, 1, NULL, NULL, NULL, NULL),
(15, 'AT', 'Austria', NULL, 1, NULL, NULL, NULL, NULL),
(16, 'AZ', 'Azerbaijan', NULL, 1, NULL, NULL, NULL, NULL),
(17, 'BS', 'Bahamas', NULL, 1, NULL, NULL, NULL, NULL),
(18, 'BH', 'Bahrain', NULL, 1, NULL, NULL, NULL, NULL),
(19, 'BD', 'Bangladesh', NULL, 1, NULL, NULL, NULL, NULL),
(20, 'BB', 'Barbados', NULL, 1, NULL, NULL, NULL, NULL),
(21, 'BY', 'Belarus', NULL, 1, NULL, NULL, NULL, NULL),
(22, 'BE', 'Belgium', NULL, 1, NULL, NULL, NULL, NULL),
(23, 'BZ', 'Belize', NULL, 1, NULL, NULL, NULL, NULL),
(24, 'BJ', 'Benin', NULL, 1, NULL, NULL, NULL, NULL),
(25, 'BM', 'Bermuda', NULL, 1, NULL, NULL, NULL, NULL),
(26, 'BT', 'Bhutan', NULL, 1, NULL, NULL, NULL, NULL),
(27, 'BO', 'Bolivia', NULL, 1, NULL, NULL, NULL, NULL),
(28, 'BA', 'Bosnia and Herzegovina', NULL, 1, NULL, NULL, NULL, NULL),
(29, 'BW', 'Botswana', NULL, 1, NULL, NULL, NULL, NULL),
(30, 'BV', 'Bouvet Island', NULL, 1, NULL, NULL, NULL, NULL),
(31, 'BR', 'Brazil', NULL, 1, NULL, NULL, NULL, NULL),
(32, 'IO', 'British lndian Ocean Territory', NULL, 1, NULL, NULL, NULL, NULL),
(33, 'BN', 'Brunei Darussalam', NULL, 1, NULL, NULL, NULL, NULL),
(34, 'BG', 'Bulgaria', NULL, 1, NULL, NULL, NULL, NULL),
(35, 'BF', 'Burkina Faso', NULL, 1, NULL, NULL, NULL, NULL),
(36, 'BI', 'Burundi', NULL, 1, NULL, NULL, NULL, NULL),
(37, 'KH', 'Cambodia', NULL, 1, NULL, NULL, NULL, NULL),
(38, 'CM', 'Cameroon', NULL, 1, NULL, NULL, NULL, NULL),
(39, 'CA', 'Canada', NULL, 1, NULL, NULL, NULL, NULL),
(40, 'CV', 'Cape Verde', NULL, 1, NULL, NULL, NULL, NULL),
(41, 'KY', 'Cayman Islands', NULL, 1, NULL, NULL, NULL, NULL),
(42, 'CF', 'Central African Republic', NULL, 1, NULL, NULL, NULL, NULL),
(43, 'TD', 'Chad', NULL, 1, NULL, NULL, NULL, NULL),
(44, 'CL', 'Chile', NULL, 1, NULL, NULL, NULL, NULL),
(45, 'CN', 'China', NULL, 1, NULL, NULL, NULL, NULL),
(46, 'CX', 'Christmas Island', NULL, 1, NULL, NULL, NULL, NULL),
(47, 'CC', 'Cocos (Keeling) Islands', NULL, 1, NULL, NULL, NULL, NULL),
(48, 'CO', 'Colombia', NULL, 1, NULL, NULL, NULL, NULL),
(49, 'KM', 'Comoros', NULL, 1, NULL, NULL, NULL, NULL),
(50, 'CG', 'Congo', NULL, 1, NULL, NULL, NULL, NULL),
(51, 'CK', 'Cook Islands', NULL, 1, NULL, NULL, NULL, NULL),
(52, 'CR', 'Costa Rica', NULL, 1, NULL, NULL, NULL, NULL),
(53, 'HR', 'Croatia (Hrvatska)', NULL, 1, NULL, NULL, NULL, NULL),
(54, 'CU', 'Cuba', NULL, 1, NULL, NULL, NULL, NULL),
(55, 'CY', 'Cyprus', NULL, 1, NULL, NULL, NULL, NULL),
(56, 'CZ', 'Czech Republic', NULL, 1, NULL, NULL, NULL, NULL),
(57, 'CD', 'Democratic Republic of Congo', NULL, 1, NULL, NULL, NULL, NULL),
(58, 'DK', 'Denmark', NULL, 1, NULL, NULL, NULL, NULL),
(59, 'DJ', 'Djibouti', NULL, 1, NULL, NULL, NULL, NULL),
(60, 'DM', 'Dominica', NULL, 1, NULL, NULL, NULL, NULL),
(61, 'DO', 'Dominican Republic', NULL, 1, NULL, NULL, NULL, NULL),
(62, 'TP', 'East Timor', NULL, 1, NULL, NULL, NULL, NULL),
(63, 'EC', 'Ecudaor', NULL, 1, NULL, NULL, NULL, NULL),
(64, 'EG', 'Egypt', NULL, 1, NULL, NULL, NULL, NULL),
(65, 'SV', 'El Salvador', NULL, 1, NULL, NULL, NULL, NULL),
(66, 'GQ', 'Equatorial Guinea', NULL, 1, NULL, NULL, NULL, NULL),
(67, 'ER', 'Eritrea', NULL, 1, NULL, NULL, NULL, NULL),
(68, 'EE', 'Estonia', NULL, 1, NULL, NULL, NULL, NULL),
(69, 'ET', 'Ethiopia', NULL, 1, NULL, NULL, NULL, NULL),
(70, 'FK', 'Falkland Islands (Malvinas)', NULL, 1, NULL, NULL, NULL, NULL),
(71, 'FO', 'Faroe Islands', NULL, 1, NULL, NULL, NULL, NULL),
(72, 'FJ', 'Fiji', NULL, 1, NULL, NULL, NULL, NULL),
(73, 'FI', 'Finland', NULL, 1, NULL, NULL, NULL, NULL),
(74, 'FR', 'France', NULL, 1, NULL, NULL, NULL, NULL),
(75, 'FX', 'France, Metropolitan', NULL, 1, NULL, NULL, NULL, NULL),
(76, 'GF', 'French Guiana', NULL, 1, NULL, NULL, NULL, NULL),
(77, 'PF', 'French Polynesia', NULL, 1, NULL, NULL, NULL, NULL),
(78, 'TF', 'French Southern Territories', NULL, 1, NULL, NULL, NULL, NULL),
(79, 'GA', 'Gabon', NULL, 1, NULL, NULL, NULL, NULL),
(80, 'GM', 'Gambia', NULL, 1, NULL, NULL, NULL, NULL),
(81, 'GE', 'Georgia', NULL, 1, NULL, NULL, NULL, NULL),
(82, 'DE', 'Germany', NULL, 1, NULL, NULL, NULL, NULL),
(83, 'GH', 'Ghana', NULL, 1, NULL, NULL, NULL, NULL),
(84, 'GI', 'Gibraltar', NULL, 1, NULL, NULL, NULL, NULL),
(85, 'GR', 'Greece', NULL, 1, NULL, NULL, NULL, NULL),
(86, 'GL', 'Greenland', NULL, 1, NULL, NULL, NULL, NULL),
(87, 'GD', 'Grenada', NULL, 1, NULL, NULL, NULL, NULL),
(88, 'GP', 'Guadeloupe', NULL, 1, NULL, NULL, NULL, NULL),
(89, 'GU', 'Guam', NULL, 1, NULL, NULL, NULL, NULL),
(90, 'GT', 'Guatemala', NULL, 1, NULL, NULL, NULL, NULL),
(91, 'GN', 'Guinea', NULL, 1, NULL, NULL, NULL, NULL),
(92, 'GW', 'Guinea-Bissau', NULL, 1, NULL, NULL, NULL, NULL),
(93, 'GY', 'Guyana', NULL, 1, NULL, NULL, NULL, NULL),
(94, 'HT', 'Haiti', NULL, 1, NULL, NULL, NULL, NULL),
(95, 'HM', 'Heard and Mc Donald Islands', NULL, 1, NULL, NULL, NULL, NULL),
(96, 'HN', 'Honduras', NULL, 1, NULL, NULL, NULL, NULL),
(97, 'HK', 'Hong Kong', NULL, 1, NULL, NULL, NULL, NULL),
(98, 'HU', 'Hungary', NULL, 1, NULL, NULL, NULL, NULL),
(99, 'IS', 'Iceland', NULL, 1, NULL, NULL, NULL, NULL),
(100, 'IN', 'India', NULL, 1, NULL, NULL, NULL, NULL),
(101, 'ID', 'Indonesia', NULL, 1, NULL, NULL, NULL, NULL),
(102, 'IR', 'Iran (Islamic Republic of)', NULL, 1, NULL, NULL, NULL, NULL),
(103, 'IQ', 'Iraq', NULL, 1, NULL, NULL, NULL, NULL),
(104, 'IE', 'Ireland', NULL, 1, NULL, NULL, NULL, NULL),
(105, 'IL', 'Israel', NULL, 1, NULL, NULL, NULL, NULL),
(106, 'IT', 'Italy', NULL, 1, NULL, NULL, NULL, NULL),
(107, 'CI', 'Ivory Coast', NULL, 1, NULL, NULL, NULL, NULL),
(108, 'JM', 'Jamaica', NULL, 1, NULL, NULL, NULL, NULL),
(109, 'JP', 'Japan', NULL, 1, NULL, NULL, NULL, NULL),
(110, 'JO', 'Jordan', NULL, 1, NULL, NULL, NULL, NULL),
(111, 'KZ', 'Kazakhstan', NULL, 1, NULL, NULL, NULL, NULL),
(112, 'KE', 'Kenya', NULL, 1, NULL, NULL, NULL, NULL),
(113, 'KI', 'Kiribati', NULL, 1, NULL, NULL, NULL, NULL),
(114, 'KP', 'Korea, Democratic People\'s Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(115, 'KR', 'Korea, Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(116, 'KW', 'Kuwait', NULL, 1, NULL, NULL, NULL, NULL),
(117, 'KG', 'Kyrgyzstan', NULL, 1, NULL, NULL, NULL, NULL),
(118, 'LA', 'Lao People\'s Democratic Republic', NULL, 1, NULL, NULL, NULL, NULL),
(119, 'LV', 'Latvia', NULL, 1, NULL, NULL, NULL, NULL),
(120, 'LB', 'Lebanon', NULL, 1, NULL, NULL, NULL, NULL),
(121, 'LS', 'Lesotho', NULL, 1, NULL, NULL, NULL, NULL),
(122, 'LR', 'Liberia', NULL, 1, NULL, NULL, NULL, NULL),
(123, 'LY', 'Libyan Arab Jamahiriya', NULL, 1, NULL, NULL, NULL, NULL),
(124, 'LI', 'Liechtenstein', NULL, 1, NULL, NULL, NULL, NULL),
(125, 'LT', 'Lithuania', NULL, 1, NULL, NULL, NULL, NULL),
(126, 'LU', 'Luxembourg', NULL, 1, NULL, NULL, NULL, NULL),
(127, 'MO', 'Macau', NULL, 1, NULL, NULL, NULL, NULL),
(128, 'MK', 'Macedonia', NULL, 1, NULL, NULL, NULL, NULL),
(129, 'MG', 'Madagascar', NULL, 1, NULL, NULL, NULL, NULL),
(130, 'MW', 'Malawi', NULL, 1, NULL, NULL, NULL, NULL),
(131, 'MY', 'Malaysia', NULL, 1, NULL, NULL, NULL, NULL),
(132, 'MV', 'Maldives', NULL, 1, NULL, NULL, NULL, NULL),
(133, 'ML', 'Mali', NULL, 1, NULL, NULL, NULL, NULL),
(134, 'MT', 'Malta', NULL, 1, NULL, NULL, NULL, NULL),
(135, 'MH', 'Marshall Islands', NULL, 1, NULL, NULL, NULL, NULL),
(136, 'MQ', 'Martinique', NULL, 1, NULL, NULL, NULL, NULL),
(137, 'MR', 'Mauritania', NULL, 1, NULL, NULL, NULL, NULL),
(138, 'MU', 'Mauritius', NULL, 1, NULL, NULL, NULL, NULL),
(139, 'TY', 'Mayotte', NULL, 1, NULL, NULL, NULL, NULL),
(140, 'MX', 'Mexico', NULL, 1, NULL, NULL, NULL, NULL),
(141, 'FM', 'Micronesia, Federated States of', NULL, 1, NULL, NULL, NULL, NULL),
(142, 'MD', 'Moldova, Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(143, 'MC', 'Monaco', NULL, 1, NULL, NULL, NULL, NULL),
(144, 'MN', 'Mongolia', NULL, 1, NULL, NULL, NULL, NULL),
(145, 'MS', 'Montserrat', NULL, 1, NULL, NULL, NULL, NULL),
(146, 'MA', 'Morocco', NULL, 1, NULL, NULL, NULL, NULL),
(147, 'MZ', 'Mozambique', NULL, 1, NULL, NULL, NULL, NULL),
(148, 'MM', 'Myanmar', NULL, 1, NULL, NULL, NULL, NULL),
(149, 'NA', 'Namibia', NULL, 1, NULL, NULL, NULL, NULL),
(150, 'NR', 'Nauru', NULL, 1, NULL, NULL, NULL, NULL),
(151, 'NP', 'Nepal', NULL, 1, NULL, NULL, NULL, NULL),
(152, 'NL', 'Netherlands', NULL, 1, NULL, NULL, NULL, NULL),
(153, 'AN', 'Netherlands Antilles', NULL, 1, NULL, NULL, NULL, NULL),
(154, 'NC', 'New Caledonia', NULL, 1, NULL, NULL, NULL, NULL),
(155, 'NZ', 'New Zealand', NULL, 1, NULL, NULL, NULL, NULL),
(156, 'NI', 'Nicaragua', NULL, 1, NULL, NULL, NULL, NULL),
(157, 'NE', 'Niger', NULL, 1, NULL, NULL, NULL, NULL),
(158, 'NG', 'Nigeria', NULL, 1, NULL, NULL, NULL, NULL),
(159, 'NU', 'Niue', NULL, 1, NULL, NULL, NULL, NULL),
(160, 'NF', 'Norfork Island', NULL, 1, NULL, NULL, NULL, NULL),
(161, 'MP', 'Northern Mariana Islands', NULL, 1, NULL, NULL, NULL, NULL),
(162, 'NO', 'Norway', NULL, 1, NULL, NULL, NULL, NULL),
(163, 'OM', 'Oman', NULL, 1, NULL, NULL, NULL, NULL),
(164, 'PK', 'Pakistan', NULL, 1, NULL, NULL, NULL, NULL),
(165, 'PW', 'Palau', NULL, 1, NULL, NULL, NULL, NULL),
(166, 'PA', 'Panama', NULL, 1, NULL, NULL, NULL, NULL),
(167, 'PG', 'Papua New Guinea', NULL, 1, NULL, NULL, NULL, NULL),
(168, 'PY', 'Paraguay', NULL, 1, NULL, NULL, NULL, NULL),
(169, 'PE', 'Peru', NULL, 1, NULL, NULL, NULL, NULL),
(170, 'PN', 'Pitcairn', NULL, 1, NULL, NULL, NULL, NULL),
(171, 'PL', 'Poland', NULL, 1, NULL, NULL, NULL, NULL),
(172, 'PT', 'Portugal', NULL, 1, NULL, NULL, NULL, NULL),
(173, 'PR', 'Puerto Rico', NULL, 1, NULL, NULL, NULL, NULL),
(174, 'QA', 'Qatar', NULL, 1, NULL, NULL, NULL, NULL),
(175, 'SS', 'Republic of South Sudan', NULL, 1, NULL, NULL, NULL, NULL),
(176, 'RE', 'Reunion', NULL, 1, NULL, NULL, NULL, NULL),
(177, 'RO', 'Romania', NULL, 1, NULL, NULL, NULL, NULL),
(178, 'RU', 'Russian Federation', NULL, 1, NULL, NULL, NULL, NULL),
(179, 'RW', 'Rwanda', NULL, 1, NULL, NULL, NULL, NULL),
(180, 'KN', 'Saint Kitts and Nevis', NULL, 1, NULL, NULL, NULL, NULL),
(181, 'LC', 'Saint Lucia', NULL, 1, NULL, NULL, NULL, NULL),
(182, 'VC', 'Saint Vincent and the Grenadines', NULL, 1, NULL, NULL, NULL, NULL),
(183, 'WS', 'Samoa', NULL, 1, NULL, NULL, NULL, NULL),
(184, 'SM', 'San Marino', NULL, 1, NULL, NULL, NULL, NULL),
(185, 'ST', 'Sao Tome and Principe', NULL, 1, NULL, NULL, NULL, NULL),
(186, 'SA', 'Saudi Arabia', NULL, 1, NULL, NULL, NULL, NULL),
(187, 'SN', 'Senegal', NULL, 1, NULL, NULL, NULL, NULL),
(188, 'RS', 'Serbia', NULL, 1, NULL, NULL, NULL, NULL),
(189, 'SC', 'Seychelles', NULL, 1, NULL, NULL, NULL, NULL),
(190, 'SL', 'Sierra Leone', NULL, 1, NULL, NULL, NULL, NULL),
(191, 'SG', 'Singapore', NULL, 1, NULL, NULL, NULL, NULL),
(192, 'SK', 'Slovakia', NULL, 1, NULL, NULL, NULL, NULL),
(193, 'SI', 'Slovenia', NULL, 1, NULL, NULL, NULL, NULL),
(194, 'SB', 'Solomon Islands', NULL, 1, NULL, NULL, NULL, NULL),
(195, 'SO', 'Somalia', NULL, 1, NULL, NULL, NULL, NULL),
(196, 'ZA', 'South Africa', NULL, 1, NULL, NULL, NULL, NULL),
(197, 'GS', 'South Georgia South Sandwich Islands', NULL, 1, NULL, NULL, NULL, NULL),
(198, 'ES', 'Spain', NULL, 1, NULL, NULL, NULL, NULL),
(199, 'LK', 'Sri Lanka', NULL, 1, NULL, NULL, NULL, NULL),
(200, 'SH', 'St. Helena', NULL, 1, NULL, NULL, NULL, NULL),
(201, 'PM', 'St. Pierre and Miquelon', NULL, 1, NULL, NULL, NULL, NULL),
(202, 'SD', 'Sudan', NULL, 1, NULL, NULL, NULL, NULL),
(203, 'SR', 'Suriname', NULL, 1, NULL, NULL, NULL, NULL),
(204, 'SJ', 'Svalbarn and Jan Mayen Islands', NULL, 1, NULL, NULL, NULL, NULL),
(205, 'SZ', 'Swaziland', NULL, 1, NULL, NULL, NULL, NULL),
(206, 'SE', 'Sweden', NULL, 1, NULL, NULL, NULL, NULL),
(207, 'CH', 'Switzerland', NULL, 1, NULL, NULL, NULL, NULL),
(208, 'SY', 'Syrian Arab Republic', NULL, 1, NULL, NULL, NULL, NULL),
(209, 'TW', 'Taiwan', NULL, 1, NULL, NULL, NULL, NULL),
(210, 'TJ', 'Tajikistan', NULL, 1, NULL, NULL, NULL, NULL),
(211, 'TZ', 'Tanzania, United Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(212, 'TH', 'Thailand', NULL, 1, NULL, NULL, NULL, NULL),
(213, 'TG', 'Togo', NULL, 1, NULL, NULL, NULL, NULL),
(214, 'TK', 'Tokelau', NULL, 1, NULL, NULL, NULL, NULL),
(215, 'TO', 'Tonga', NULL, 1, NULL, NULL, NULL, NULL),
(216, 'TT', 'Trinidad and Tobago', NULL, 1, NULL, NULL, NULL, NULL),
(217, 'TN', 'Tunisia', NULL, 1, NULL, NULL, NULL, NULL),
(218, 'TR', 'Turkey', NULL, 1, NULL, NULL, NULL, NULL),
(219, 'TM', 'Turkmenistan', NULL, 1, NULL, NULL, NULL, NULL),
(220, 'TC', 'Turks and Caicos Islands', NULL, 1, NULL, NULL, NULL, NULL),
(221, 'TV', 'Tuvalu', NULL, 1, NULL, NULL, NULL, NULL),
(222, 'US', 'United States', NULL, 1, NULL, NULL, NULL, NULL),
(223, 'UG', 'Uganda', NULL, 1, NULL, NULL, NULL, NULL),
(224, 'UA', 'Ukraine', NULL, 1, NULL, NULL, NULL, NULL),
(225, 'AE', 'United Arab Emirates', NULL, 1, NULL, NULL, NULL, NULL),
(226, 'GB', 'United Kingdom', NULL, 1, NULL, NULL, NULL, NULL),
(227, 'UM', 'United States minor outlying islands', NULL, 1, NULL, NULL, NULL, NULL),
(228, 'UY', 'Uruguay', NULL, 1, NULL, NULL, NULL, NULL),
(229, 'UZ', 'Uzbekistan', NULL, 1, NULL, NULL, NULL, NULL),
(230, 'VU', 'Vanuatu', NULL, 1, NULL, NULL, NULL, NULL),
(231, 'VA', 'Vatican City State', NULL, 1, NULL, NULL, NULL, NULL),
(232, 'VE', 'Venezuela', NULL, 1, NULL, NULL, NULL, NULL),
(233, 'VN', 'Vietnam', NULL, 1, NULL, NULL, NULL, NULL),
(234, 'VG', 'Virgin Islands (British)', NULL, 1, NULL, NULL, NULL, NULL),
(235, 'VI', 'Virgin Islands (U.S.)', NULL, 1, NULL, NULL, NULL, NULL),
(236, 'WF', 'Wallis and Futuna Islands', NULL, 1, NULL, NULL, NULL, NULL),
(237, 'EH', 'Western Sahara', NULL, 1, NULL, NULL, NULL, NULL),
(238, 'YE', 'Yemen', NULL, 1, NULL, NULL, NULL, NULL),
(239, 'YU', 'Yugoslavia', NULL, 1, NULL, NULL, NULL, NULL),
(240, 'ZR', 'Zaire', NULL, 1, NULL, NULL, NULL, NULL),
(241, 'ZM', 'Zambia', NULL, 1, NULL, NULL, NULL, NULL),
(242, 'ZW', 'Zimbabwe', NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2020_06_09_194234_create_user_level', 2),
(6, '2020_06_10_084854_create_tec_application', 3),
(7, '2020_06_10_104057_create_countries', 3),
(8, '2020_06_10_141620_create_profile', 4),
(9, '2020_06_11_172604_create_sections', 5),
(10, '2020_06_11_172758_create_reason_denials', 5),
(11, '2020_06_11_224850_create_type_applicants', 6),
(12, '2020_06_12_215539_create_airlines', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `passport_no` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `passport_no`, `user_id`) VALUES
(1, '1234567asdasd', 1),
(2, '123123', 3),
(3, 'PN123456789', 5),
(4, '123456789', 6);

-- --------------------------------------------------------

--
-- Table structure for table `reason_denials`
--

CREATE TABLE `reason_denials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reason_denials`
--

INSERT INTO `reason_denials` (`id`, `name`) VALUES
(1, 'The passenger failed to submit required documents in time.'),
(2, 'The passenger is not qualified for the Section being applied for'),
(3, 'The submitted documents are not qualified for the Section you are applying for.'),
(4, 'The passenger stayed in the Philippines for more than 1 year.'),
(5, 'The passenger is not covered by the Travel Tax law.'),
(6, 'The documents presented passed its date of expiry.');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `code`, `name`) VALUES
(1, '2A', 'Foreign Diplomatic and Consular Officials and staff members duly accredited to the Philippines Note: This includes the immediate members of their families and household domestics, with authorization from the Philippine Government.'),
(2, '2B1', 'Officials, Consultants, Experts and Employees of the United Nations (UN) organization and its agencies and those exempted under existing Laws, Treaties and International Agreements.\r\nNote: This includes the dependents whose travel is funded by the UN organization and its agencies and by those exempted under existing Laws Treaties and International Agreements.'),
(3, '2B2', 'Special laws'),
(4, '2B2A', 'Athletes, coaches and representatives endorsed by Philippine Sports Commission'),
(5, '2B2B', 'Balik- Scientist endorsed by DOST'),
(6, '2C', 'United States (US) military personnel and their dependents and other US nationals with fares paid for by the US government or on US government-owned or chartered transport facilities'),
(7, '2D', 'Overseas Filipino Workers (OFWs)'),
(8, '2E', 'Crewmembers of airplanes plying international routes'),
(9, '2F', 'Filipino permanent residents abroad whose stay in the Philippines is less than one (1) year'),
(10, '2G', 'Philippine Foreign Service personnel officially assigned abroad and their dependents'),
(11, '2H', 'Officials and employees of the Philippine Government traveling on official business (excluding Government-Owned and Controlled Corporations)'),
(12, '2I', 'Grantees of foreign government funded trips'),
(13, '2J', 'Bona-fide students with approved scholarships by appropriate Philippine government agency'),
(14, '2K', 'Infants who are two (2) years old and below'),
(15, '2L', 'Personnel (and their dependents) of multinational companies with regional headquarters, but not engaged in business, in the Philippines'),
(16, '2M', 'Those authorized by the President of the Philippines for reason of national interest'),
(17, '2M1', 'Filipino Muslim Pilgrimage going to Mecca'),
(18, '2MX', 'Exporter endorsed by EDC'),
(19, '2MB', 'Filipino leaving the country exited Mindanao and Palawan bound to BIMP-EAGA'),
(20, '2B3', 'Balikbayan (as defined under Republic Act No. 6768, As amended by Republic Act No. 9174), who is a Filipino citizen, whose stay in the Philippines is not more than one (1) year'),
(21, '2B4', 'Balikbayan (as defined under Republic Act No. 6768, As amended by Republic Act No 9174), who is a former Filipino citizen naturalized in a foreign country, travels to the Philippines and stays for not more than one (1) year.Note: The spouse and children (who are Filipino passport holders traveling with the Balikbayan) of the above-mentioned Balikbayan are also qualified for exemption.');

-- --------------------------------------------------------

--
-- Table structure for table `sections_upload`
--

CREATE TABLE `sections_upload` (
  `id` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections_upload`
--

INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES
(1, 7, 'IF HIRED THROUGH POEA, the Overseas Employment Certificate (OEC) serves as the <br>Travel Tax Excemption (TEC) and THERE IS NO NEED TO APPLY FOR A TEC. <br>If directed hired abroad, please upload your Certificate of Employment issued by <br>the Philippine Embassy / Consulate in the place of work or an Employment Contract <br>authenticated by the Philippine Embassy / Consulate.', NULL),
(2, 1, 'Certification from the Office of Protocol, Department of Foreign Affairs or their respective<br>Embassy / Consulate<br>', NULL),
(3, 2, 'Certification from the Office of Protocol, Department of Foreign Affairs or their respective<br>Embassy / Consulate NOTE: Dependents are also exempted if fare is paid for and<br> certified by the UN. For dependents of employees of other UN agencies<br> a certification and proof from the organization / agency is required<br>', NULL),
(4, 6, 'Government Transport Request (GTR) for a plane ticket or certification from the<br> US Embassy that the fare is paid from the US Government funds<br>', NULL),
(5, 8, 'Certification from the Civil Aeronautics Board that the crewmember is joining his aircraft,<br> indicating the crewmember’s name, position, and location of aircraft<br>', NULL),
(6, 9, 'Copies of the identification pages of passport and stamp of last arrival<br> in the Philippines.<br>', NULL),
(7, 9, 'Proof of permanent residence in foreign country (e.g. US Green card, Canadian<br> Form 1000, etc)or Certification of Residence issued by the Philippine Embassy<br> / Consulate in the country which does not grant permanent resident status or<br> appropriate entries in the Passport. FRONT PAGE<br>', NULL),
(8, 9, 'Proof of permanent residence in foreign country (e.g. US Green card, Canadian<br> Form 1000, etc)or Certification of Residence issued by the Philippine Embassy<br> / Consulate in the country which does not grant permanent resident status or<br> appropriate entries in the Passport. BACK PAGE<br>', NULL),
(9, 10, 'Certification to this effect from the Department of Foreign Affairs<br>', NULL),
(10, 11, 'Certified true copy of Travel Authority or Travel Order from the Department Secretary<br> concerned to the effect that such officials / employees are traveling on official business<br>', NULL),
(11, 12, 'Proof that travel is funded or provided by a foreign government<br>', NULL),
(12, 13, 'Certification to this effect from concerned government agency NOTE: “Student” is defined<br> as a person attending formal classes in an educational institution for the purpose<br> of taking up a course leading to a diploma, the duration of which<br> is not less than one (1) year<br>', NULL),
(13, 14, 'Birth certificate of the infant<br>', NULL),
(14, 15, 'Certification to this effect from the Board of Investments.<br>', NULL),
(15, 16, 'Written authorization from the Office of the President expressly entitling the passenger<br> to travel tax exemption<br>', NULL),
(16, 20, 'All pages of your current Passport. Please combined all scanned pages of the<br> passport in one PDF file.<br>', NULL),
(17, 21, 'Foreign passport of former Filipino or other evidence of former Philippine Citizenship<br>', NULL),
(18, 21, 'Birth certificate or adoption papers of children and / or marriage contract of<br> accompanying spouse.<br>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tec_application`
--

CREATE TABLE `tec_application` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_application` datetime NOT NULL,
  `date_validity` date DEFAULT NULL,
  `passport_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_ticket_issued` date DEFAULT NULL,
  `ticket_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_flight` date NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `denial_id` int(11) DEFAULT NULL,
  `denial_msg` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_processor_id` int(11) DEFAULT NULL,
  `id_picture_2x2_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_identification_page_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket_booking_ref_no_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applicant_type_id` int(11) DEFAULT NULL,
  `airlines_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tec_application`
--

INSERT INTO `tec_application` (`id`, `user_id`, `date_application`, `date_validity`, `passport_no`, `date_ticket_issued`, `ticket_no`, `date_flight`, `section_id`, `country_id`, `status`, `denial_id`, `denial_msg`, `assign_processor_id`, `id_picture_2x2_fn`, `passport_identification_page_fn`, `ticket_booking_ref_no_fn`, `applicant_type_id`, `airlines_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2020-06-11 17:55:12'),
(2, 5, '2020-06-12 01:13:33', NULL, 'PN12345678901', '2020-06-12', '123456789', '2020-06-13', 1, 14, 2, 1, NULL, 4, 'id_picture_2x2_20200612_Msade76UxKjQRinebaaWY6qhPkMcC8dXL9CwtTZjDwxa5JuIWp.png', 'passport_identification_page_20200612_MdOUGc1oW8s2z184i7ZbHDEFWOklBZz4gEglb5m4sRrl8Tyh0X.png', 'ticket_booking_ref_no_20200612_MdOUGc1oW8s2z184i7ZbHDEFWOklBZz4gEglb5m4sRrl8Tyh0X.png', 1, NULL, 2, 2, NULL, '2020-06-12 01:20:38', NULL),
(3, 6, '2020-06-12 08:17:39', NULL, 'PN123321123321', '2020-06-12', '123456789', '2020-06-15', 1, 2, 3, 1, NULL, 4, 'id_picture_2x2_20200612_rGVJMWOGVDaVXaS5anFHdouVHmnia16Jr9e6hOxFME4FfNI3kA.png', 'passport_identification_page_20200612_rGVJMWOGVDaVXaS5anFHdouVHmnia16Jr9e6hOxFME4FfNI3kA.jpg', 'ticket_booking_ref_no_20200612_rGVJMWOGVDaVXaS5anFHdouVHmnia16Jr9e6hOxFME4FfNI3kA.jpg', 1, NULL, 2, 2, NULL, '2020-06-12 01:21:13', NULL),
(4, 5, '2020-06-12 09:33:16', NULL, '123445', '2020-06-12', '123456', '2020-06-13', 1, 1, 1, NULL, NULL, 4, NULL, NULL, NULL, 1, NULL, 2, 2, NULL, '2020-06-12 01:55:23', NULL),
(5, 6, '2020-06-12 09:33:59', NULL, '123123', '2020-06-13', '123123', '2020-06-13', 1, 2, 1, NULL, NULL, 4, NULL, NULL, NULL, 1, NULL, 2, NULL, NULL, NULL, NULL),
(6, 6, '2020-06-12 09:36:44', NULL, '123123123', '2020-06-13', '123123123', '2020-06-15', 1, 2, 1, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, 2, NULL, NULL, NULL, NULL),
(7, 5, '2020-06-12 09:40:14', NULL, '123123', '2020-06-12', '123123', '2020-06-12', 1, 2, 1, NULL, NULL, 4, NULL, NULL, NULL, 1, NULL, 2, NULL, NULL, NULL, NULL),
(8, 6, '2020-06-12 10:23:51', NULL, 'PN123456789', '2020-06-12', '12345678912345678912345678912', '2020-06-16', 2, 2, 2, 1, NULL, 9, 'id_picture_2x2_20200612_ah2Yze9sOpoZQqIKy5eETMGPbugP9s7gNc6HXwa0GMVc04ujKO.png', 'passport_identification_page_20200612_ah2Yze9sOpoZQqIKy5eETMGPbugP9s7gNc6HXwa0GMVc04ujKO.jpg', 'ticket_booking_ref_no_20200612_ah2Yze9sOpoZQqIKy5eETMGPbugP9s7gNc6HXwa0GMVc04ujKO.png', 2, 1, 2, 2, NULL, '2020-06-12 20:45:39', NULL),
(9, 5, '2020-06-13 07:19:23', NULL, 'PN123456789', '2020-06-13', '12345678912345678912345678912', '2020-06-13', NULL, 2, 2, NULL, NULL, 9, NULL, NULL, NULL, 1, 1, 9, 2, NULL, '2020-06-13 00:59:42', NULL),
(10, 6, '2020-06-13 09:06:17', NULL, 'PN123456789', '2020-06-13', '12345678912345678', '2020-06-13', NULL, 2, 2, NULL, NULL, 9, 'id_picture_2x2_20200613_AkJ6CoObElPqC0JeMSoOkOr2l5jSTkl7BzVdJLdpi4UGZJGQYi.png', 'passport_identification_page_20200613_AkJ6CoObElPqC0JeMSoOkOr2l5jSTkl7BzVdJLdpi4UGZJGQYi.jpg', 'ticket_booking_ref_no_20200613_AkJ6CoObElPqC0JeMSoOkOr2l5jSTkl7BzVdJLdpi4UGZJGQYi.png', 7, 50, 9, 2, NULL, '2020-06-13 01:09:22', NULL),
(11, 6, '2020-06-13 14:34:58', '2020-06-15', 'PN123456789', '2020-06-13', '12345678912345678', '2020-06-16', NULL, 2, 2, 1, NULL, 20, 'id_picture_2x2_20200613_v1dr5McD6GFsnfMsEX9RlqzYGYFRyagmtIIZlUZ3vBXw9rSIgG.png', NULL, NULL, 7, 1, 20, 2, NULL, '2020-06-14 01:07:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tec_application_upload`
--

CREATE TABLE `tec_application_upload` (
  `id` int(11) NOT NULL,
  `tec_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `upload_by` int(11) DEFAULT NULL,
  `upload_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tec_application_upload`
--

INSERT INTO `tec_application_upload` (`id`, `tec_id`, `section_id`, `file_name`, `upload_by`, `upload_at`, `deleted_at`) VALUES
(1, 11, 7, 'file_0_20200614_WCthPbpCVMb2humxd5azs44MuKZPw1pe3hBVAj2fT3ByWnDboq.jpg', 2, '2020-06-13 22:08:04', '2020-06-14 08:45:25'),
(2, 11, 7, 'file_0_20200614_iTmpJPWYqNnCfo0LP2tAg5UQOrBtV3iN2BlE3DOOUfgdoIRfop.jpg', 2, '2020-06-13 22:15:12', '2020-06-14 08:45:25'),
(3, 11, 7, 'file_0_20200614_U8duIuWolcuWWnMOHCDcgfIXuTOe4SEMtbtYlDwImH3LZbZNWW.jpg', 2, '2020-06-13 22:25:54', '2020-06-14 08:45:25'),
(4, 11, 9, 'file_0_20200614_FtwcQjFM3FYboRPZy5JK7hbDW3GQmiG6YO8T4bgRDqXwQnYDEW.png', 2, '2020-06-13 22:59:06', '2020-06-14 08:45:25'),
(5, 11, 9, 'file_0_20200614_FtwcQjFM3FYboRPZy5JK7hbDW3GQmiG6YO8T4bgRDqXwQnYDEW.jpg', 2, '2020-06-13 22:59:06', '2020-06-14 08:45:25'),
(6, 11, 9, 'file_0_20200614_FtwcQjFM3FYboRPZy5JK7hbDW3GQmiG6YO8T4bgRDqXwQnYDEW.png', 2, '2020-06-13 22:59:06', '2020-06-14 08:45:25'),
(7, 11, 9, 'file_0_20200614_ToLlccgvBD2HCjNLRLJrRSsQ1yGKF5clWIECT47oWPYTbvXu3V.png', 2, '2020-06-13 22:59:22', '2020-06-14 08:45:25'),
(8, 11, 9, 'file_0_20200614_M9JOvLOQOLhhjCf7z6VcFndXGC1CBfejrtbnrCDXv2MheExwMe.png', 2, '2020-06-13 23:04:17', '2020-06-14 08:45:25'),
(9, 11, 9, 'file_0_20200614_3OEVWGgr59StCy0kMLSHub4VatQTsMA0LhUeVwPl0jv8gTlc1E.jpg', 2, '2020-06-13 23:04:27', '2020-06-14 08:45:25'),
(10, 11, 7, 'file_0_20200614_0Z2VE5HqslKY5WlIj2KS2sPZtEPaETT0FZbrMiuQ5TyKc6ZLge.png', 2, '2020-06-13 23:04:52', '2020-06-14 08:45:25'),
(11, 11, 9, 'file_0_20200614_wJtzrmJp42TG2Ge4CjhFeW6Z3LeQ7AbIyIFKcU0sbZrHjIFBKu.png', 2, '2020-06-13 23:07:34', '2020-06-14 08:45:25'),
(12, 11, 9, 'file_0_20200614_wJtzrmJp42TG2Ge4CjhFeW6Z3LeQ7AbIyIFKcU0sbZrHjIFBKu.jpg', 2, '2020-06-13 23:07:34', '2020-06-14 08:45:25'),
(13, 11, 9, 'file_0_20200614_wJtzrmJp42TG2Ge4CjhFeW6Z3LeQ7AbIyIFKcU0sbZrHjIFBKu.png', 2, '2020-06-13 23:07:34', '2020-06-14 08:45:25'),
(14, 11, 9, 'file_0_20200614_iTsdL7vFVNqTnORRqGQDD0OVjrVcYW0GP49fy0YmeRHqKOs2d6.png', 2, '2020-06-13 23:08:07', '2020-06-14 08:45:25'),
(15, 11, 9, 'file_0_20200614_DrxMoml06G2aGr0azeev1zcCIbLIdSQwCfmPZqtIfj4Go14oxd.jpg', 2, '2020-06-13 23:09:26', '2020-06-14 08:45:25'),
(16, 11, 9, 'file_0_20200614_WLvaeX7DwFY9QlPsasyYCLcLn3xSykGmtuAp3WNkgsw85YFAAm.jpg', 2, '2020-06-13 23:09:40', '2020-06-14 08:45:25'),
(17, 11, 9, 'file_0_20200614_zrXAPazHr6SxNRQqF05lW7TFraSNUObkFonxZDmCxDKfCUtUEO.png', 2, '2020-06-13 23:09:49', '2020-06-14 08:45:25'),
(18, 11, 9, 'file_0_20200614_IZfNmfq9mFDD5h0y8V2Cmik6n7bSb8TxsBkCLsnHm423sBliJq.jpg', 2, '2020-06-13 23:10:52', '2020-06-14 08:45:25'),
(19, 11, 9, 'file_0_20200614_IZfNmfq9mFDD5h0y8V2Cmik6n7bSb8TxsBkCLsnHm423sBliJq.png', 2, '2020-06-13 23:10:52', '2020-06-14 08:45:25'),
(20, 11, 9, 'file_0_20200614_uDOd108nln2lXLLDNq4SxAeVGgJSqM3zUnwf49pWQjPT5sYF0l.png', 2, '2020-06-13 23:11:01', '2020-06-14 08:45:25'),
(21, 11, 7, 'file_0_20200614_tAIaq1qx5yA0oQL43eOwegHWGqosD89cyJF5NkSaZl6cLebYBJ.png', 2, '2020-06-13 23:15:38', '2020-06-14 08:45:25'),
(22, 11, 8, 'file_0_20200614_ylyWJThaDHimj73mxlMDTn0o6jbLpbINAPzDmQotV2BSPviQ8k.png', 2, '2020-06-13 23:19:43', '2020-06-14 08:45:25'),
(23, 11, 8, 'file_0_20200614_0hMDSiIbJQhpGqP6wLlhGCluoAMQxHOeikserLbLyCI53cCwv5.png', 2, '2020-06-13 23:20:31', '2020-06-14 08:45:25'),
(24, 11, 8, 'file_0_20200614_SWpDBGIc8ocYw7svEiT4BaY5ZaOUgwYW65ryZaqFW5JklzvPK8.png', 2, '2020-06-13 23:33:46', '2020-06-14 08:45:25'),
(25, 11, 8, 'file_0_20200614_PKOEQLMV73a5xWFZ3xf33rhM392fav9JjBQ3QlzF4CxZxkcwsH.png', 2, '2020-06-13 23:34:14', '2020-06-14 08:45:25'),
(26, 11, 9, 'file_0_20200614_aNSP1Q3giDoqbS9T6JjkZh6ZRCVzYA04vYtyGIsLyf2sZAZbxg.png', 2, '2020-06-13 23:35:18', '2020-06-14 08:45:25'),
(27, 11, 9, 'file_0_20200614_aNSP1Q3giDoqbS9T6JjkZh6ZRCVzYA04vYtyGIsLyf2sZAZbxg.jpg', 2, '2020-06-13 23:35:18', '2020-06-14 08:45:25'),
(28, 11, 9, 'file_0_20200614_aNSP1Q3giDoqbS9T6JjkZh6ZRCVzYA04vYtyGIsLyf2sZAZbxg.png', 2, '2020-06-13 23:35:18', '2020-06-14 08:45:25'),
(29, 11, 9, 'file_0_20200614_aqWzkqlOEZtageDz5RMcTGor2ZuqdApIX0GjZK3ypjy7OchGsv.png', 2, '2020-06-13 23:35:28', '2020-06-14 08:45:25'),
(30, 11, 7, 'file_0_20200614_wJGeFBZhIReI6VHzIKjwfkcZaiR6OFWeaQ1U9QLJS0xSkYRPl7.png', 2, '2020-06-13 23:44:37', '2020-06-14 08:45:25'),
(31, 11, 8, 'file_0_20200614_O6n1bBeTraZgqLC9JOH1UnNOh0fZGVoLKEbvUEHVvDl6DzuFG2.png', 2, '2020-06-13 23:45:06', '2020-06-14 08:45:25'),
(32, 11, 8, 'file_0_20200614_FVjcZq9WwfP0mhthfkonSe9grMq8xxXzu4YomN2qthThiwk00P.png', 2, '2020-06-13 23:46:46', '2020-06-14 08:45:25'),
(33, 11, 8, 'file_0_20200614_3frrs7wWo8KfwUmjF5qSUtXpOqBnQDpUorhIi4AXNvjZU1v5EW.png', 2, '2020-06-13 23:48:10', '2020-06-14 08:45:25'),
(34, 11, 8, 'file_0_20200614_e8juR1f4And3ycJ4XOZHgLjt40dNJxSwcuSfteHuQSZJ9X5aXC.png', 2, '2020-06-13 23:49:28', '2020-06-14 08:45:25'),
(35, 11, 8, 'file_0_20200614_FIIAaItWDN95UXLBz6kuwgspLtdAwKo7FvBN81zEQ8oIOHOi1f.png', 2, '2020-06-13 23:50:06', '2020-06-14 08:45:25'),
(36, 11, 8, 'file_0_20200614_dAyJtxkMV25xadq5vyEcMEqdh7NTUj3qHV01IgjyGrtYUinUxf.png', 2, '2020-06-13 23:53:04', '2020-06-14 08:45:25'),
(37, 11, 8, 'file_0_20200614_DYHpVZZqU3n9ZHkZfu0YHRre0AuAUUcvGMbHPihsT8blMB2WWV.png', 2, '2020-06-13 23:54:56', '2020-06-14 08:45:25'),
(38, 11, 9, 'file_0_20200614_ipJGYyaJVtHUn80RyEhRlEZ31U7f7i9LlzliTcESUAa2HJcKDZ.png', 2, '2020-06-13 23:55:22', '2020-06-14 08:45:25'),
(39, 11, 9, 'file_0_20200614_ipJGYyaJVtHUn80RyEhRlEZ31U7f7i9LlzliTcESUAa2HJcKDZ.jpg', 2, '2020-06-13 23:55:22', '2020-06-14 08:45:25'),
(40, 11, 9, 'file_0_20200614_ipJGYyaJVtHUn80RyEhRlEZ31U7f7i9LlzliTcESUAa2HJcKDZ.png', 2, '2020-06-13 23:55:22', '2020-06-14 08:45:25'),
(41, 11, 9, 'file_0_20200614_qPXKG4EdIceoubkPClpHDdcJm9wRKX4Fy72C5TJVMyjaCLaDUN.png', 2, '2020-06-13 23:55:41', '2020-06-14 08:45:25'),
(42, 11, 7, 'file_0_20200614_d6xBQefMWCWP2CRO1KALOiIdhFzvwq0YDdnBZWn1ixm8hCPDjQ.png', 2, '2020-06-13 23:57:40', '2020-06-14 08:45:25'),
(43, 11, 7, 'file_0_20200614_tPD9Vtniu0Vc1hsdAIPlQwqtmoJqYkM9I8lfUjLLtQM3VtPMa1.png', 2, '2020-06-13 23:59:49', '2020-06-14 08:45:25'),
(44, 11, 6, 'file_0_20200614_5tzwWLIH1gkkHh6vCcKeB39yX5SHYOE6WNx27l419HiC8Z1mMw.png', 2, '2020-06-14 00:00:09', '2020-06-14 08:45:25'),
(45, 11, 7, 'file_0_20200614_UM1sw4qkWTf3zURo0E86L0fqvOajcstM5xrTMNewkXBFfk3UiN.png', 2, '2020-06-14 00:05:23', '2020-06-14 08:45:25'),
(46, 11, 7, 'file_0_20200614_VgWUWEjKcspGkU2joFpO7wiCWGFmUwSXSQ8gQqYD3YPaDz2qDY.png', 2, '2020-06-14 00:07:31', '2020-06-14 08:45:25'),
(47, 11, 7, 'file_0_20200614_eMTYYis9zetIVuHlTVrq2WEls8NBAuaSpWFTgon7e7OcjJZzL3.png', 2, '2020-06-14 00:09:41', '2020-06-14 08:45:25'),
(48, 11, 9, 'file_0_20200614_jCt1GojrUXmk0zLL151km3DYqeZ5HXxQJjx1AWdB0FqlquTL6y.png', 2, '2020-06-14 00:14:28', '2020-06-14 08:45:25'),
(49, 11, 9, 'file_0_20200614_kdNjniehiuzA806BxA7qMKSx3QkGp3TnPRz0LORl2IGzkEDJNg.jpg', 2, '2020-06-14 00:38:52', '2020-06-14 08:45:25'),
(50, 11, 9, 'file_0_20200614_Vy5XEFSINRRmkmUNfj2kjy7vLlHMifL4Zwg1lLYqzblqvRL4Uk.png', 2, '2020-06-14 00:45:11', '2020-06-14 08:45:25'),
(51, 11, 7, 'file_0_20200614_G2NnM059ZJugVbgYiIBgugkbQGvvNNuXcGhVt7B5lZNZAfE0Xa.png', 2, '2020-06-14 00:45:25', '2020-06-14 09:07:01'),
(52, 11, 7, 'file_0_20200614_7O36LFNONVow4iGpsaaTXoXkNMlivvQ4EUUaOTvq3CYLuHDkjo.jpg', 2, '2020-06-14 01:07:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_applicants`
--

CREATE TABLE `type_applicants` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `type_applicants`
--

INSERT INTO `type_applicants` (`id`, `name`) VALUES
(1, 'Foreign Diplomatic and Consular Officials and Members of their Staff'),
(2, 'Officials, Consultants, Experts, and Employees of the United Nations (UN) Organization and its agencies'),
(3, 'United States (US) Military Personnel including dependents and other US nationals with fares paid for by the US government or on US Government-owned or chartered transport facilities'),
(4, 'Overseas Filipino Worker Directly Hired Abroad going to their jobsite'),
(5, 'Crew member of airplanes plying international routes'),
(6, 'Philippine Foreign Service Personnel officially assigned abroad and their dependents'),
(7, 'Officials and Employees of the Philippine Government traveling on official business (excluding Government-Owned and Controlled Corporations)'),
(8, 'Grantee of foreign government funded trips\r\n'),
(9, 'Bona-fide Student with approved foreign scholarship by appropriate Philippine government agency'),
(10, 'Infant (Up to second birthday on date of travel)'),
(11, 'Personnel (and their dependents) of multinational companies with regional headquarters, but not engaged in business, in the Philippines'),
(12, 'Those authorized by the President of the Republic of the Philippines for reasons of national interest'),
(13, 'Balikbayans whose stay in the Philippines is less than one (1) year as provided under R.A. 6768'),
(14, 'Family members of former Filipinos accompanying the latter as provided under R.A. 6768');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `last_name`, `first_name`, `middle_name`, `email`, `username`, `password`, `level`, `status`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Simbulan', 'Angie', 'P', 'angiesimbulan.tieza@gmail.com', 'APS0001', '$2y$10$v1LRQR87EQek05gPQHJQ1OAOa43Olq7G0qW4RSlfpXSZJDh8tC8vi', 2, 1, NULL, 1, 2, '2020-06-11 03:01:29', '2020-06-12 01:39:45', NULL),
(2, 'Miranda', 'Xybriel Dennis', 'Mista', 'xybrielmiranda.tieza@gmail.com', 'XMM0002', '$2y$10$SjdO8LNM69lzBTiS3cKUhOkgrcHptHFLSvq72eHtESYOuj.NvLc4y', 0, 1, 'EKQVu2yyKEQDePQPxFl58njgP2LWlyNTrgTAVzKAdlVQfB4sLc1kPTHp11Nl', 1, 2, '2020-06-11 03:01:29', '2020-06-12 01:29:30', NULL),
(3, 'test', 'test', 'test', 'test@mail.com', 'test', '$2y$10$ncYR3St.JLGGz8fb09RWZOhi7Ek7GGd1tckO0n1ciF8o26vp6PZaG', 3, 1, '1DJusEep3TvospLLJ0sEARqko1dpS4mxufrHAt3dtbwSjubDHh1VV7NVqJXH', 2, 2, NULL, '2020-06-11 04:41:31', '2020-06-11 11:17:12'),
(4, 'Placido', 'Emmanuel', 'Santiago', 'emmanplacido.tieza@gmail.com', 'ESP0004', '$2y$10$/bPXqqbFpTAbbQKEHkWSXOFuzmFhhYGxpUn77Rw1sle5JZLljOXJa', 1, 1, 'vqvonPIXYNNw9Lg34loFHjeInxBuhNwwCFcxbAeLvQjIxqXjMerMwLMTzBlC', 2, 2, NULL, '2020-06-12 22:51:49', NULL),
(5, 'Dela Cruz', 'Raquel', 'Sagun', 'test@mail.com', 'RSD0005', NULL, 3, 1, NULL, 2, 2, NULL, '2020-06-11 14:20:35', NULL),
(6, 'Tada', 'Meriel', 'Sagun', 'test@mail.com', 'MST0006', NULL, 3, 1, NULL, 2, 2, NULL, '2020-06-12 01:19:56', NULL),
(7, 'Test', 'Test', 'Test', 'test@mail.com', 'TTT0007', NULL, 3, 1, NULL, 2, 2, NULL, '2020-06-12 01:25:36', '2020-06-12 01:25:39'),
(8, 'T', 'S', 'S', 'test@mail.com', 'SST0008', NULL, 3, 1, NULL, 2, 2, NULL, '2020-06-12 01:27:28', '2020-06-12 01:27:40'),
(9, 'Jeruz', 'Daryl John', 'D', 'test@mail.com', 'DDJ0009', '$2y$10$Luk1LCtiX/sE0ro9jkbWh.gcyp4S9QYe2F.cJUbIDCkm8wRytgVyy', 2, 1, '39LRlvWZqG0Xs5mBhcmj4JzlTxiFWmrDG1LEHeqFOL67kz6z2YLqAqWduxFY', 2, 4, NULL, '2020-06-12 22:58:37', NULL),
(10, 'Rodriguez', 'Emerson', 'L', 'test@mail.com', 'ELR0010', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(11, 'Lagrosas', 'Paquito', 'D', 'test@mail.com', 'PDL0011', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(12, 'Magdaraog', 'Roda', 'M', 'test@mail.com', 'RMM0012', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(13, 'Valleser', 'Marjon', 'B', 'test@mail.com', 'MBV0013', NULL, 1, 1, NULL, 2, NULL, NULL, NULL, NULL),
(14, 'Divina', 'Andrea Camille', 'M', 'test@mail.com', 'AMD0014', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(15, 'Atutubo', 'Nhea Mea', 'D', 'test@mail.com', 'NDA0015', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(16, 'Si', 'Ryan Louise', 'N', 'test@mail.com', 'RNS0016', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(17, 'Garcia', 'Maria Theresa', 'M', 'test@mail.com', 'MMG0017', NULL, 1, 1, NULL, 2, NULL, NULL, NULL, NULL),
(18, 'Pimentel', 'Charina', 'P', 'test@mail.com', 'CPP0018', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(19, 'Lim', 'Allen Angelo', 'L', 'test@mail.com', 'ALL0019', NULL, 2, 1, NULL, 2, NULL, NULL, NULL, NULL),
(20, 'Concepcion', 'Arthur', 'S', 'test@mail.com', 'ASC0020', '$2y$10$3UiZYjilkv4Uv1bTigrWROOQwS/IKVCX2NsS2BVibx/59DpGLZNgi', 2, 1, 'L2uxgADKpiyhb0PCad8iQa1F4xEXnlTBUDE5bORMf8Q58FBdTzMM1reB1LWt', 2, 2, NULL, '2020-06-13 06:33:42', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airlines`
--
ALTER TABLE `airlines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reason_denials`
--
ALTER TABLE `reason_denials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections_upload`
--
ALTER TABLE `sections_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_application`
--
ALTER TABLE `tec_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_application_upload`
--
ALTER TABLE `tec_application_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_applicants`
--
ALTER TABLE `type_applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airlines`
--
ALTER TABLE `airlines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reason_denials`
--
ALTER TABLE `reason_denials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `sections_upload`
--
ALTER TABLE `sections_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tec_application`
--
ALTER TABLE `tec_application`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tec_application_upload`
--
ALTER TABLE `tec_application_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `type_applicants`
--
ALTER TABLE `type_applicants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
