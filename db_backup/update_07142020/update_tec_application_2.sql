ALTER TABLE `tec_application` CHANGE `user_id` `user_id` INT(11) NULL;
ALTER TABLE `tec_application` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `tec_application_upload` ADD `deleted_by` INT NULL AFTER `upload_at`;