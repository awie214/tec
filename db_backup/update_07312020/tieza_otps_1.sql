-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2020 at 08:16 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tieza_otps_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `airlines`
--

CREATE TABLE `airlines` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `airlines`
--

INSERT INTO `airlines` (`id`, `name`) VALUES
(1, 'Aero Mexico'),
(2, 'Aeroflot Russian Airlines'),
(3, 'Air Asia Berhad'),
(4, 'Air Busan'),
(5, 'Air Canada'),
(6, 'Air China Limited'),
(7, 'Air Macau'),
(8, 'Air Mauritius'),
(9, 'Air New Zealand'),
(10, 'Air Niugini'),
(11, 'Aleson Shipping Lines, Inc.'),
(12, 'All Nippon Airways'),
(13, 'American Airlines'),
(14, 'Asiana Airlines'),
(15, 'Austrian Airlines'),
(16, 'Bangkok Airways'),
(17, 'British Airways'),
(18, 'Cathay Pacific Airways'),
(19, 'Cebu Pacific Air'),
(20, 'China Airlines'),
(21, 'China Eastern Airlines'),
(22, 'China Southern Airlines'),
(23, 'Delta Airlines'),
(24, 'El-Al Israel Airlines'),
(25, 'Emirates Airlines'),
(26, 'Ethiopian Airlines'),
(27, 'Etihad Airways'),
(28, 'EVA Airways'),
(29, 'Fiji Airways'),
(30, 'Finnair'),
(31, 'Garuda Indonesia Airlines'),
(32, 'Gulf Air'),
(33, 'Hahn Air'),
(34, 'Hawaiian Airlines'),
(35, 'Hongkong Airlines'),
(36, 'Hongkong Dragon Airlines Ltd.'),
(37, 'Japan Airlines'),
(38, 'Jeju Air'),
(39, 'Jin Air - Cebu'),
(40, 'Jin Air - Clark'),
(41, 'KLM Royal Dutch Airlines'),
(42, 'Kenya Airways'),
(43, 'Korean Air'),
(44, 'Kuwait Airways'),
(45, 'Lufthansa German Airlines'),
(46, 'Malaysia Airlines'),
(47, 'Oman Air'),
(48, 'Pakistan Airlines'),
(49, 'PAL  Express'),
(50, 'Philippine Air Asia'),
(51, 'Philippine Airlines'),
(52, 'Qantas Airways'),
(53, 'Qatar Aiways'),
(54, 'Royal Brunei Airlines'),
(55, 'Royal Jordanian Airlines'),
(56, 'Saudi Arabian Airlines'),
(57, 'Scoot Tiger Air'),
(58, 'Silkair'),
(59, 'Singapore Airlines'),
(60, 'South African Airways'),
(61, 'Swiss International Air Lines'),
(62, 'Tap Potugal'),
(63, 'Thai Airways'),
(64, 'Turkish Airlines'),
(65, 'United Airlines'),
(66, 'Ukraine International Airlines'),
(67, 'Vietnam Airlines'),
(68, 'Wallem Philippine Shipping'),
(69, 'Xiamen Air'),
(70, 'Jetstar');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `name`, `description`, `created_at`, `deleted_at`) VALUES
(1, 'BPI', NULL, '2020-07-18 01:09:45', NULL),
(2, 'Metrobank', NULL, '2020-07-18 01:09:51', NULL),
(3, 'BDO', NULL, '2020-07-18 01:10:01', NULL),
(4, 'Security Bank', NULL, '2020-07-18 01:10:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'PH', 'Philippines', NULL, 1, NULL, NULL, NULL, NULL),
(2, 'AF', 'Afghanistan', NULL, 1, NULL, NULL, NULL, NULL),
(3, 'AL', 'Albania', NULL, 1, NULL, NULL, NULL, NULL),
(4, 'DZ', 'Algeria', NULL, 1, NULL, NULL, NULL, NULL),
(5, 'AS', 'American Samoa', NULL, 1, NULL, NULL, NULL, NULL),
(6, 'AD', 'Andorra', NULL, 1, NULL, NULL, NULL, NULL),
(7, 'AO', 'Angola', NULL, 1, NULL, NULL, NULL, NULL),
(8, 'AI', 'Anguilla', NULL, 1, NULL, NULL, NULL, NULL),
(9, 'AQ', 'Antarctica', NULL, 1, NULL, NULL, NULL, NULL),
(10, 'AG', 'Antigua and/or Barbuda', NULL, 1, NULL, NULL, NULL, NULL),
(11, 'AR', 'Argentina', NULL, 1, NULL, NULL, NULL, NULL),
(12, 'AM', 'Armenia', NULL, 1, NULL, NULL, NULL, NULL),
(13, 'AW', 'Aruba', NULL, 1, NULL, NULL, NULL, NULL),
(14, 'AU', 'Australia', NULL, 1, NULL, NULL, NULL, NULL),
(15, 'AT', 'Austria', NULL, 1, NULL, NULL, NULL, NULL),
(16, 'AZ', 'Azerbaijan', NULL, 1, NULL, NULL, NULL, NULL),
(17, 'BS', 'Bahamas', NULL, 1, NULL, NULL, NULL, NULL),
(18, 'BH', 'Bahrain', NULL, 1, NULL, NULL, NULL, NULL),
(19, 'BD', 'Bangladesh', NULL, 1, NULL, NULL, NULL, NULL),
(20, 'BB', 'Barbados', NULL, 1, NULL, NULL, NULL, NULL),
(21, 'BY', 'Belarus', NULL, 1, NULL, NULL, NULL, NULL),
(22, 'BE', 'Belgium', NULL, 1, NULL, NULL, NULL, NULL),
(23, 'BZ', 'Belize', NULL, 1, NULL, NULL, NULL, NULL),
(24, 'BJ', 'Benin', NULL, 1, NULL, NULL, NULL, NULL),
(25, 'BM', 'Bermuda', NULL, 1, NULL, NULL, NULL, NULL),
(26, 'BT', 'Bhutan', NULL, 1, NULL, NULL, NULL, NULL),
(27, 'BO', 'Bolivia', NULL, 1, NULL, NULL, NULL, NULL),
(28, 'BA', 'Bosnia and Herzegovina', NULL, 1, NULL, NULL, NULL, NULL),
(29, 'BW', 'Botswana', NULL, 1, NULL, NULL, NULL, NULL),
(30, 'BV', 'Bouvet Island', NULL, 1, NULL, NULL, NULL, NULL),
(31, 'BR', 'Brazil', NULL, 1, NULL, NULL, NULL, NULL),
(32, 'IO', 'British lndian Ocean Territory', NULL, 1, NULL, NULL, NULL, NULL),
(33, 'BN', 'Brunei Darussalam', NULL, 1, NULL, NULL, NULL, NULL),
(34, 'BG', 'Bulgaria', NULL, 1, NULL, NULL, NULL, NULL),
(35, 'BF', 'Burkina Faso', NULL, 1, NULL, NULL, NULL, NULL),
(36, 'BI', 'Burundi', NULL, 1, NULL, NULL, NULL, NULL),
(37, 'KH', 'Cambodia', NULL, 1, NULL, NULL, NULL, NULL),
(38, 'CM', 'Cameroon', NULL, 1, NULL, NULL, NULL, NULL),
(39, 'CA', 'Canada', NULL, 1, NULL, NULL, NULL, NULL),
(40, 'CV', 'Cape Verde', NULL, 1, NULL, NULL, NULL, NULL),
(41, 'KY', 'Cayman Islands', NULL, 1, NULL, NULL, NULL, NULL),
(42, 'CF', 'Central African Republic', NULL, 1, NULL, NULL, NULL, NULL),
(43, 'TD', 'Chad', NULL, 1, NULL, NULL, NULL, NULL),
(44, 'CL', 'Chile', NULL, 1, NULL, NULL, NULL, NULL),
(45, 'CN', 'China', NULL, 1, NULL, NULL, NULL, NULL),
(46, 'CX', 'Christmas Island', NULL, 1, NULL, NULL, NULL, NULL),
(47, 'CC', 'Cocos (Keeling) Islands', NULL, 1, NULL, NULL, NULL, NULL),
(48, 'CO', 'Colombia', NULL, 1, NULL, NULL, NULL, NULL),
(49, 'KM', 'Comoros', NULL, 1, NULL, NULL, NULL, NULL),
(50, 'CG', 'Congo', NULL, 1, NULL, NULL, NULL, NULL),
(51, 'CK', 'Cook Islands', NULL, 1, NULL, NULL, NULL, NULL),
(52, 'CR', 'Costa Rica', NULL, 1, NULL, NULL, NULL, NULL),
(53, 'HR', 'Croatia (Hrvatska)', NULL, 1, NULL, NULL, NULL, NULL),
(54, 'CU', 'Cuba', NULL, 1, NULL, NULL, NULL, NULL),
(55, 'CY', 'Cyprus', NULL, 1, NULL, NULL, NULL, NULL),
(56, 'CZ', 'Czech Republic', NULL, 1, NULL, NULL, NULL, NULL),
(57, 'CD', 'Democratic Republic of Congo', NULL, 1, NULL, NULL, NULL, NULL),
(58, 'DK', 'Denmark', NULL, 1, NULL, NULL, NULL, NULL),
(59, 'DJ', 'Djibouti', NULL, 1, NULL, NULL, NULL, NULL),
(60, 'DM', 'Dominica', NULL, 1, NULL, NULL, NULL, NULL),
(61, 'DO', 'Dominican Republic', NULL, 1, NULL, NULL, NULL, NULL),
(62, 'TP', 'East Timor', NULL, 1, NULL, NULL, NULL, NULL),
(63, 'EC', 'Ecudaor', NULL, 1, NULL, NULL, NULL, NULL),
(64, 'EG', 'Egypt', NULL, 1, NULL, NULL, NULL, NULL),
(65, 'SV', 'El Salvador', NULL, 1, NULL, NULL, NULL, NULL),
(66, 'GQ', 'Equatorial Guinea', NULL, 1, NULL, NULL, NULL, NULL),
(67, 'ER', 'Eritrea', NULL, 1, NULL, NULL, NULL, NULL),
(68, 'EE', 'Estonia', NULL, 1, NULL, NULL, NULL, NULL),
(69, 'ET', 'Ethiopia', NULL, 1, NULL, NULL, NULL, NULL),
(70, 'FK', 'Falkland Islands (Malvinas)', NULL, 1, NULL, NULL, NULL, NULL),
(71, 'FO', 'Faroe Islands', NULL, 1, NULL, NULL, NULL, NULL),
(72, 'FJ', 'Fiji', NULL, 1, NULL, NULL, NULL, NULL),
(73, 'FI', 'Finland', NULL, 1, NULL, NULL, NULL, NULL),
(74, 'FR', 'France', NULL, 1, NULL, NULL, NULL, NULL),
(75, 'FX', 'France, Metropolitan', NULL, 1, NULL, NULL, NULL, NULL),
(76, 'GF', 'French Guiana', NULL, 1, NULL, NULL, NULL, NULL),
(77, 'PF', 'French Polynesia', NULL, 1, NULL, NULL, NULL, NULL),
(78, 'TF', 'French Southern Territories', NULL, 1, NULL, NULL, NULL, NULL),
(79, 'GA', 'Gabon', NULL, 1, NULL, NULL, NULL, NULL),
(80, 'GM', 'Gambia', NULL, 1, NULL, NULL, NULL, NULL),
(81, 'GE', 'Georgia', NULL, 1, NULL, NULL, NULL, NULL),
(82, 'DE', 'Germany', NULL, 1, NULL, NULL, NULL, NULL),
(83, 'GH', 'Ghana', NULL, 1, NULL, NULL, NULL, NULL),
(84, 'GI', 'Gibraltar', NULL, 1, NULL, NULL, NULL, NULL),
(85, 'GR', 'Greece', NULL, 1, NULL, NULL, NULL, NULL),
(86, 'GL', 'Greenland', NULL, 1, NULL, NULL, NULL, NULL),
(87, 'GD', 'Grenada', NULL, 1, NULL, NULL, NULL, NULL),
(88, 'GP', 'Guadeloupe', NULL, 1, NULL, NULL, NULL, NULL),
(89, 'GU', 'Guam', NULL, 1, NULL, NULL, NULL, NULL),
(90, 'GT', 'Guatemala', NULL, 1, NULL, NULL, NULL, NULL),
(91, 'GN', 'Guinea', NULL, 1, NULL, NULL, NULL, NULL),
(92, 'GW', 'Guinea-Bissau', NULL, 1, NULL, NULL, NULL, NULL),
(93, 'GY', 'Guyana', NULL, 1, NULL, NULL, NULL, NULL),
(94, 'HT', 'Haiti', NULL, 1, NULL, NULL, NULL, NULL),
(95, 'HM', 'Heard and Mc Donald Islands', NULL, 1, NULL, NULL, NULL, NULL),
(96, 'HN', 'Honduras', NULL, 1, NULL, NULL, NULL, NULL),
(97, 'HK', 'Hong Kong', NULL, 1, NULL, NULL, NULL, NULL),
(98, 'HU', 'Hungary', NULL, 1, NULL, NULL, NULL, NULL),
(99, 'IS', 'Iceland', NULL, 1, NULL, NULL, NULL, NULL),
(100, 'IN', 'India', NULL, 1, NULL, NULL, NULL, NULL),
(101, 'ID', 'Indonesia', NULL, 1, NULL, NULL, NULL, NULL),
(102, 'IR', 'Iran (Islamic Republic of)', NULL, 1, NULL, NULL, NULL, NULL),
(103, 'IQ', 'Iraq', NULL, 1, NULL, NULL, NULL, NULL),
(104, 'IE', 'Ireland', NULL, 1, NULL, NULL, NULL, NULL),
(105, 'IL', 'Israel', NULL, 1, NULL, NULL, NULL, NULL),
(106, 'IT', 'Italy', NULL, 1, NULL, NULL, NULL, NULL),
(107, 'CI', 'Ivory Coast', NULL, 1, NULL, NULL, NULL, NULL),
(108, 'JM', 'Jamaica', NULL, 1, NULL, NULL, NULL, NULL),
(109, 'JP', 'Japan', NULL, 1, NULL, NULL, NULL, NULL),
(110, 'JO', 'Jordan', NULL, 1, NULL, NULL, NULL, NULL),
(111, 'KZ', 'Kazakhstan', NULL, 1, NULL, NULL, NULL, NULL),
(112, 'KE', 'Kenya', NULL, 1, NULL, NULL, NULL, NULL),
(113, 'KI', 'Kiribati', NULL, 1, NULL, NULL, NULL, NULL),
(114, 'KP', 'Korea, Democratic People\'s Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(115, 'KR', 'Korea, Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(116, 'KW', 'Kuwait', NULL, 1, NULL, NULL, NULL, NULL),
(117, 'KG', 'Kyrgyzstan', NULL, 1, NULL, NULL, NULL, NULL),
(118, 'LA', 'Lao People\'s Democratic Republic', NULL, 1, NULL, NULL, NULL, NULL),
(119, 'LV', 'Latvia', NULL, 1, NULL, NULL, NULL, NULL),
(120, 'LB', 'Lebanon', NULL, 1, NULL, NULL, NULL, NULL),
(121, 'LS', 'Lesotho', NULL, 1, NULL, NULL, NULL, NULL),
(122, 'LR', 'Liberia', NULL, 1, NULL, NULL, NULL, NULL),
(123, 'LY', 'Libyan Arab Jamahiriya', NULL, 1, NULL, NULL, NULL, NULL),
(124, 'LI', 'Liechtenstein', NULL, 1, NULL, NULL, NULL, NULL),
(125, 'LT', 'Lithuania', NULL, 1, NULL, NULL, NULL, NULL),
(126, 'LU', 'Luxembourg', NULL, 1, NULL, NULL, NULL, NULL),
(127, 'MO', 'Macau', NULL, 1, NULL, NULL, NULL, NULL),
(128, 'MK', 'Macedonia', NULL, 1, NULL, NULL, NULL, NULL),
(129, 'MG', 'Madagascar', NULL, 1, NULL, NULL, NULL, NULL),
(130, 'MW', 'Malawi', NULL, 1, NULL, NULL, NULL, NULL),
(131, 'MY', 'Malaysia', NULL, 1, NULL, NULL, NULL, NULL),
(132, 'MV', 'Maldives', NULL, 1, NULL, NULL, NULL, NULL),
(133, 'ML', 'Mali', NULL, 1, NULL, NULL, NULL, NULL),
(134, 'MT', 'Malta', NULL, 1, NULL, NULL, NULL, NULL),
(135, 'MH', 'Marshall Islands', NULL, 1, NULL, NULL, NULL, NULL),
(136, 'MQ', 'Martinique', NULL, 1, NULL, NULL, NULL, NULL),
(137, 'MR', 'Mauritania', NULL, 1, NULL, NULL, NULL, NULL),
(138, 'MU', 'Mauritius', NULL, 1, NULL, NULL, NULL, NULL),
(139, 'TY', 'Mayotte', NULL, 1, NULL, NULL, NULL, NULL),
(140, 'MX', 'Mexico', NULL, 1, NULL, NULL, NULL, NULL),
(141, 'FM', 'Micronesia, Federated States of', NULL, 1, NULL, NULL, NULL, NULL),
(142, 'MD', 'Moldova, Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(143, 'MC', 'Monaco', NULL, 1, NULL, NULL, NULL, NULL),
(144, 'MN', 'Mongolia', NULL, 1, NULL, NULL, NULL, NULL),
(145, 'MS', 'Montserrat', NULL, 1, NULL, NULL, NULL, NULL),
(146, 'MA', 'Morocco', NULL, 1, NULL, NULL, NULL, NULL),
(147, 'MZ', 'Mozambique', NULL, 1, NULL, NULL, NULL, NULL),
(148, 'MM', 'Myanmar', NULL, 1, NULL, NULL, NULL, NULL),
(149, 'NA', 'Namibia', NULL, 1, NULL, NULL, NULL, NULL),
(150, 'NR', 'Nauru', NULL, 1, NULL, NULL, NULL, NULL),
(151, 'NP', 'Nepal', NULL, 1, NULL, NULL, NULL, NULL),
(152, 'NL', 'Netherlands', NULL, 1, NULL, NULL, NULL, NULL),
(153, 'AN', 'Netherlands Antilles', NULL, 1, NULL, NULL, NULL, NULL),
(154, 'NC', 'New Caledonia', NULL, 1, NULL, NULL, NULL, NULL),
(155, 'NZ', 'New Zealand', NULL, 1, NULL, NULL, NULL, NULL),
(156, 'NI', 'Nicaragua', NULL, 1, NULL, NULL, NULL, NULL),
(157, 'NE', 'Niger', NULL, 1, NULL, NULL, NULL, NULL),
(158, 'NG', 'Nigeria', NULL, 1, NULL, NULL, NULL, NULL),
(159, 'NU', 'Niue', NULL, 1, NULL, NULL, NULL, NULL),
(160, 'NF', 'Norfork Island', NULL, 1, NULL, NULL, NULL, NULL),
(161, 'MP', 'Northern Mariana Islands', NULL, 1, NULL, NULL, NULL, NULL),
(162, 'NO', 'Norway', NULL, 1, NULL, NULL, NULL, NULL),
(163, 'OM', 'Oman', NULL, 1, NULL, NULL, NULL, NULL),
(164, 'PK', 'Pakistan', NULL, 1, NULL, NULL, NULL, NULL),
(165, 'PW', 'Palau', NULL, 1, NULL, NULL, NULL, NULL),
(166, 'PA', 'Panama', NULL, 1, NULL, NULL, NULL, NULL),
(167, 'PG', 'Papua New Guinea', NULL, 1, NULL, NULL, NULL, NULL),
(168, 'PY', 'Paraguay', NULL, 1, NULL, NULL, NULL, NULL),
(169, 'PE', 'Peru', NULL, 1, NULL, NULL, NULL, NULL),
(170, 'PN', 'Pitcairn', NULL, 1, NULL, NULL, NULL, NULL),
(171, 'PL', 'Poland', NULL, 1, NULL, NULL, NULL, NULL),
(172, 'PT', 'Portugal', NULL, 1, NULL, NULL, NULL, NULL),
(173, 'PR', 'Puerto Rico', NULL, 1, NULL, NULL, NULL, NULL),
(174, 'QA', 'Qatar', NULL, 1, NULL, NULL, NULL, NULL),
(175, 'SS', 'Republic of South Sudan', NULL, 1, NULL, NULL, NULL, NULL),
(176, 'RE', 'Reunion', NULL, 1, NULL, NULL, NULL, NULL),
(177, 'RO', 'Romania', NULL, 1, NULL, NULL, NULL, NULL),
(178, 'RU', 'Russian Federation', NULL, 1, NULL, NULL, NULL, NULL),
(179, 'RW', 'Rwanda', NULL, 1, NULL, NULL, NULL, NULL),
(180, 'KN', 'Saint Kitts and Nevis', NULL, 1, NULL, NULL, NULL, NULL),
(181, 'LC', 'Saint Lucia', NULL, 1, NULL, NULL, NULL, NULL),
(182, 'VC', 'Saint Vincent and the Grenadines', NULL, 1, NULL, NULL, NULL, NULL),
(183, 'WS', 'Samoa', NULL, 1, NULL, NULL, NULL, NULL),
(184, 'SM', 'San Marino', NULL, 1, NULL, NULL, NULL, NULL),
(185, 'ST', 'Sao Tome and Principe', NULL, 1, NULL, NULL, NULL, NULL),
(186, 'SA', 'Saudi Arabia', NULL, 1, NULL, NULL, NULL, NULL),
(187, 'SN', 'Senegal', NULL, 1, NULL, NULL, NULL, NULL),
(188, 'RS', 'Serbia', NULL, 1, NULL, NULL, NULL, NULL),
(189, 'SC', 'Seychelles', NULL, 1, NULL, NULL, NULL, NULL),
(190, 'SL', 'Sierra Leone', NULL, 1, NULL, NULL, NULL, NULL),
(191, 'SG', 'Singapore', NULL, 1, NULL, NULL, NULL, NULL),
(192, 'SK', 'Slovakia', NULL, 1, NULL, NULL, NULL, NULL),
(193, 'SI', 'Slovenia', NULL, 1, NULL, NULL, NULL, NULL),
(194, 'SB', 'Solomon Islands', NULL, 1, NULL, NULL, NULL, NULL),
(195, 'SO', 'Somalia', NULL, 1, NULL, NULL, NULL, NULL),
(196, 'ZA', 'South Africa', NULL, 1, NULL, NULL, NULL, NULL),
(197, 'GS', 'South Georgia South Sandwich Islands', NULL, 1, NULL, NULL, NULL, NULL),
(198, 'ES', 'Spain', NULL, 1, NULL, NULL, NULL, NULL),
(199, 'LK', 'Sri Lanka', NULL, 1, NULL, NULL, NULL, NULL),
(200, 'SH', 'St. Helena', NULL, 1, NULL, NULL, NULL, NULL),
(201, 'PM', 'St. Pierre and Miquelon', NULL, 1, NULL, NULL, NULL, NULL),
(202, 'SD', 'Sudan', NULL, 1, NULL, NULL, NULL, NULL),
(203, 'SR', 'Suriname', NULL, 1, NULL, NULL, NULL, NULL),
(204, 'SJ', 'Svalbarn and Jan Mayen Islands', NULL, 1, NULL, NULL, NULL, NULL),
(205, 'SZ', 'Swaziland', NULL, 1, NULL, NULL, NULL, NULL),
(206, 'SE', 'Sweden', NULL, 1, NULL, NULL, NULL, NULL),
(207, 'CH', 'Switzerland', NULL, 1, NULL, NULL, NULL, NULL),
(208, 'SY', 'Syrian Arab Republic', NULL, 1, NULL, NULL, NULL, NULL),
(209, 'TW', 'Taiwan', NULL, 1, NULL, NULL, NULL, NULL),
(210, 'TJ', 'Tajikistan', NULL, 1, NULL, NULL, NULL, NULL),
(211, 'TZ', 'Tanzania, United Republic of', NULL, 1, NULL, NULL, NULL, NULL),
(212, 'TH', 'Thailand', NULL, 1, NULL, NULL, NULL, NULL),
(213, 'TG', 'Togo', NULL, 1, NULL, NULL, NULL, NULL),
(214, 'TK', 'Tokelau', NULL, 1, NULL, NULL, NULL, NULL),
(215, 'TO', 'Tonga', NULL, 1, NULL, NULL, NULL, NULL),
(216, 'TT', 'Trinidad and Tobago', NULL, 1, NULL, NULL, NULL, NULL),
(217, 'TN', 'Tunisia', NULL, 1, NULL, NULL, NULL, NULL),
(218, 'TR', 'Turkey', NULL, 1, NULL, NULL, NULL, NULL),
(219, 'TM', 'Turkmenistan', NULL, 1, NULL, NULL, NULL, NULL),
(220, 'TC', 'Turks and Caicos Islands', NULL, 1, NULL, NULL, NULL, NULL),
(221, 'TV', 'Tuvalu', NULL, 1, NULL, NULL, NULL, NULL),
(222, 'US', 'United States', NULL, 1, NULL, NULL, NULL, NULL),
(223, 'UG', 'Uganda', NULL, 1, NULL, NULL, NULL, NULL),
(224, 'UA', 'Ukraine', NULL, 1, NULL, NULL, NULL, NULL),
(225, 'AE', 'United Arab Emirates', NULL, 1, NULL, NULL, NULL, NULL),
(226, 'GB', 'United Kingdom', NULL, 1, NULL, NULL, NULL, NULL),
(227, 'UM', 'United States minor outlying islands', NULL, 1, NULL, NULL, NULL, NULL),
(228, 'UY', 'Uruguay', NULL, 1, NULL, NULL, NULL, NULL),
(229, 'UZ', 'Uzbekistan', NULL, 1, NULL, NULL, NULL, NULL),
(230, 'VU', 'Vanuatu', NULL, 1, NULL, NULL, NULL, NULL),
(231, 'VA', 'Vatican City State', NULL, 1, NULL, NULL, NULL, NULL),
(232, 'VE', 'Venezuela', NULL, 1, NULL, NULL, NULL, NULL),
(233, 'VN', 'Vietnam', NULL, 1, NULL, NULL, NULL, NULL),
(234, 'VG', 'Virgin Islands (British)', NULL, 1, NULL, NULL, NULL, NULL),
(235, 'VI', 'Virgin Islands (U.S.)', NULL, 1, NULL, NULL, NULL, NULL),
(236, 'WF', 'Wallis and Futuna Islands', NULL, 1, NULL, NULL, NULL, NULL),
(237, 'EH', 'Western Sahara', NULL, 1, NULL, NULL, NULL, NULL),
(238, 'YE', 'Yemen', NULL, 1, NULL, NULL, NULL, NULL),
(239, 'YU', 'Yugoslavia', NULL, 1, NULL, NULL, NULL, NULL),
(240, 'ZR', 'Zaire', NULL, 1, NULL, NULL, NULL, NULL),
(241, 'ZM', 'Zambia', NULL, 1, NULL, NULL, NULL, NULL),
(242, 'ZW', 'Zimbabwe', NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2020_06_09_194234_create_user_level', 2),
(6, '2020_06_10_084854_create_tec_application', 3),
(7, '2020_06_10_104057_create_countries', 3),
(8, '2020_06_10_141620_create_profile', 4),
(9, '2020_06_11_172604_create_sections', 5),
(10, '2020_06_11_172758_create_reason_denials', 5),
(11, '2020_06_11_224850_create_type_applicants', 6),
(12, '2020_06_12_215539_create_airlines', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `passport_no` varchar(50) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `passport_no`, `user_id`) VALUES
(1, '1234567asdasd', 1),
(2, '123123', 3),
(3, 'PN123456789', 5),
(4, '123456789', 6);

-- --------------------------------------------------------

--
-- Table structure for table `reason_denials`
--

CREATE TABLE `reason_denials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reason_denials`
--

INSERT INTO `reason_denials` (`id`, `name`) VALUES
(1, 'The passenger failed to submit required documents in time.'),
(2, 'The passenger is not qualified for the Section being applied for'),
(3, 'The submitted documents are not qualified for the Section you are applying for.'),
(4, 'The passenger stayed in the Philippines for more than 1 year.'),
(5, 'The passenger is not covered by the Travel Tax law.'),
(6, 'The documents presented passed its date of expiry.');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_type` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `code`, `name`, `section_type`) VALUES
(1, '2A', 'Foreign Diplomatic and Consular Officials and Members of their Staff, including the immediate members of their families and household domestics, with authorization from the Philippine Government', 1),
(2, '2B1', 'Officials, Consultants, Experts, and Employees of the United Nations (UN) Organization and its agencies (WHO, World Bank, FAO, ILO, UNICEF, UNIFA, etc.) and those exempted under laws, treaties and international agreements', 1),
(3, '2B2', 'Dependents of Officials, Consultants, Experts and Employees whose travel is funded by the international organizations or institutions concerned and those exempted under existing laws, treaties and international agreements', 1),
(4, '2B2A', 'The Philippine Sports Commission and its delegation or representatives to any international sports convention, conference and meeting, and athletes, coaches and other officials to any international competition', 1),
(5, '2B2B', 'Balik Scientist Awardees in accordance with Republic Act (RA) 11035, including the Spouse and Dependents of a Balik Scientist under Long-Term engagement', 1),
(6, '2C', 'United States (US) Military Personnel including dependents and other US nationals with fares paid for by the US government or on US Government-owned or chartered transport facilities', 1),
(7, '2D', 'Overseas Filipino Worker Directly Hired Abroad going to their job site, including but not limited to: a. Officers and employees of multinational or foreign corporations abroad; b. Missionaries; c. Officers and employees of a branch or subsidiary of a Philippine corporation located abroad; d. Employees of Philippine embassies abroad who were hired on-site; e. Household staff of members of the Philippine Foreign Service officially assigned abroad', 1),
(8, '2E', 'Crew members of airplanes plying international routes', 1),
(9, '2F', 'Filipino citizen who is a permanent resident of another country whose stay in the Philippines is less than one (1) year and is going back to their country of permanent residence', 1),
(10, '2G', 'Members of the Philippine Foreign Service officially assigned abroad who are leaving the country to assume their posts, including their dependents', 1),
(11, '2H', 'Officials and Employees of the Philippine Government traveling on official business (excluding Government-Owned and Controlled Corporations)', 1),
(12, '2I', 'Grantees of foreign government funded trips with which the Philippine Government maintains diplomatic relations', 1),
(13, '2J', 'Bona-fide Student with a foreign scholarship approved by an appropriate Philippine government agency; United Nations Development Programme (UNDP) Fellow', 1),
(14, '2K', 'Infants  who are two (2) years old and below.', 1),
(15, '2L', 'Personnel (and their dependents) of multinational companies with regional headquarters, but not engaged in business, in the Philippines', 1),
(16, '2M', 'Those authorized by the President of the Republic of the Philippines for reasons of national interest', 1),
(17, '2M1', 'Filipino Muslim Pilgrimage going to Mecca', 1),
(18, '2MX', 'Exporters joining international trade, fairs, exhibitions and selling missions endorsed by the Export Development Council (EDC)', 1),
(19, '2MB', 'Filipino leaving the country exited Mindanao and Palawan bound to BIMP-EAGA', 1),
(20, '2B3', 'Balikbayan who is a Filipino citizen, who have been continuously out of the Philippines for a period of at least one (1) year and whose stay in the Philippines is less than one (1) year', 1),
(21, '2B4', 'Filipino Spouse or child of a former Filipino citizen who travelled with the latter to the Philippines whose stay in the Philippines is less than one (1) year', 1),
(22, NULL, 'Those with unused ticket', 2),
(23, NULL, 'Non-immigrant foreign nationals who have not stayed in the Philippines for more than one (1) year', 2),
(24, NULL, 'Those who are qualified for travel tax exemption', 2),
(25, NULL, 'Those who are qualified for reduced travel tax', 2),
(26, NULL, 'Those with downgraded ticket', 2),
(27, NULL, 'Those with double payment (travel tax inadvertently paid twice for the same ticket)', 2),
(28, NULL, 'Those with undue travel tax', 2),
(29, NULL, 'Minors who are two (2) years old and one (1) day up to twelve (12) years old  (must be up to 12th birthday on the date of travel)', 3),
(30, NULL, 'Accredited Filipino journalist whose travel is in pursuit of journalistic assignment  Note: “Filipino journalists” shall include writers and editors of the press, reporters and announcers for radio and television.', 3),
(31, NULL, 'Those authorized by the President of the Republic of the Philippines for reasons of national interest', 3),
(32, NULL, 'Person With Disability, regardless of age, who is a child of an OFW  Note: This includes those who are twenty-one (21) years old and above', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sections_upload`
--

CREATE TABLE `sections_upload` (
  `id` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `key_tag` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections_upload`
--

INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`, `key_tag`) VALUES
(1, 7, 'NOTE: IF HIRED THROUGH POEA, the Overseas Employment Certificate (OEC) serves as the<br> Travel Tax Exemption Certificate (TEC) and THERE IS NO NEED TO APPLY FOR A TEC. <br> If directly hired abroad, please upload your Certificate of Employment issued by<br> the Philippine Embassy / Consulate in the place of work or an Employment Contract<br> authenticated by the Philippine Embassy / Consulate.', NULL, NULL),
(2, 1, 'Certification from the Office of Protocol, Department of Foreign Affairs or their respective<br> Embassy / Consulate.', NULL, NULL),
(3, 2, 'Certification from the United Nations (UN) Organization or its agencies (WHO, World <br>Bank, FAO, ILO, UNICEF, UNIFA, etc.)<br>', NULL, NULL),
(4, 6, 'Government Transport Request (GTR) for a plane ticket or certification from the<br> US Embassy that the fare is paid from the US Government funds<br>', NULL, NULL),
(5, 8, 'Certification from the Civil Aeronautics Board that the crewmember is joining his aircraft,<br> indicating the crewmember’s name, position, and location of aircraft<br>', NULL, NULL),
(6, NULL, 'Copies of the identification pages of passport and stamp of last arrival<br> in the Philippines.<br>', NULL, NULL),
(7, 9, 'a. Proof of permanent residence in foreign country (e.g. US Green card, Canadian<br> Form 1000, etc.) FRONT and BACK or b.  Valid Certification from the Philippine<br> consulate / embassy indicating that the Filipino has resided uninterruptedly for a period<br> of five (5) years in the foreign country without having been absent <br>therefrom for more than six (6) months in any one year or appropriate<br> stamps in the passport to prove such length of stay.', NULL, 6),
(8, NULL, 'Proof of permanent residence in foreign country (e.g. US Green card, Canadian<br> Form 1000, etc)or Certification of Residence issued by the Philippine Embassy<br> / Consulate in the country which does not grant permanent resident status or<br> appropriate entries in the Passport. BACK PAGE<br>', NULL, NULL),
(9, 10, 'Certification from the Department of Foreign Affairs.', NULL, NULL),
(10, 11, 'Clear copy of Travel Authority or Travel Order from the Department Secretary<br> concerned to the effect that such officials / employees are traveling on<br> official business.', NULL, 7),
(11, 12, 'Proof that travel is funded or provided by a foreign government<br>', NULL, NULL),
(12, 13, 'Certification to this effect from concerned Philippine government agency. NOTE: “Student” is defined<br> as a person attending formal classes in a foreign educational institution for the purpose <br>of taking up a course leading to a diploma, the duration of which is <br>not less than one (1) year. For UNDP Fellow: Certification from UNDP specifying<br> that the Fellow is exempted from Travel Tax.<br>', NULL, NULL),
(13, 14, 'Birth certificate of the infant<br>', NULL, 4),
(14, 15, 'Certification to this effect from the Board of Investments.<br>', NULL, NULL),
(15, 16, 'Written authorization from the Office of the President expressly entitling the passenger<br> to travel tax exemption<br>', NULL, NULL),
(16, 20, 'All pages of your current Passport. Please combined all scanned pages of the<br> passport in one PDF file.<br>', NULL, NULL),
(17, 21, 'Copy of Foreign passport of former Filipino or other evidence of<br> former Philippine Citizenship.<br>', NULL, NULL),
(18, 21, 'Latest arrival stamp in the Philippines.', NULL, NULL),
(19, 20, 'VISA or Residence Card issued by the country of residence.<br><br>\r\nNote: In case your last arrival in the Philippines is stamped in an old passport,<br> you will need to scan this page from your old passport and include it on the PDF file.', NULL, NULL),
(20, 21, 'Marriage contract of accompanying spouse or Birth certificate or <br>adoption papers of the child.', NULL, 3),
(21, NULL, 'All pages of your current passport, Please combine all scanned pages of the passport in one PDF file.<br><br>Note: In case your last arrival in the Philippines is stamped in an old passport, <br>you will need to scan this page from your old passport and include this in the PDF file.', NULL, NULL),
(22, 3, 'Certification from the agency or organization concerned that the passenger belongs to<br> one of the categories, and that their travel is funded by the <br>agency or organization concerned<br>', NULL, NULL),
(23, 4, 'Endorsement from the Philippine Sports Commission.', NULL, NULL),
(24, 4, 'Travel Authority issued by the Philippine Sports Commission.', NULL, NULL),
(25, 4, ' Invitation from the host country.<', NULL, NULL),
(26, 5, 'Endorsement from DOST specifying that the Balik Scientist Awardee is exempted from<br>Travel Tax.', NULL, NULL),
(27, 5, 'For the Spouse or Dependent of a Balik Scientist under Long-Term engagement:<br> Proof of Relationship to the Balik Scientist. (Marriage Certificate, Birth Certificate)', NULL, NULL),
(28, NULL, 'Government Transport Request (GTR) for a plane ticket or certification from the<br> US Embassy that the fare is paid from US Government funds.', NULL, NULL),
(29, NULL, 'NOTE: IF HIRED THROUGH POEA, the Overseas Employment Certificate (OEC) serves as the<br> Travel Tax Exemption Certificate (TEC) and THERE IS NO NEED TO APPLY FOR A TEC. <br> If directly hired abroad, please upload your Certificate of Employment issued by<br> the Philippine Embassy / Consulate in the place of work or an Employment Contract<br> authenticated by the Philippine Embassy / Consulate.', NULL, NULL),
(30, 20, 'Airline ticket used in traveling to the Philippines. <br>                                    NOTE: In case your last arrival in the Philippines is stamped in an old <br>passport, you will need to scan this page from your old passport and<br> include it in the PDF file.', NULL, NULL),
(31, 18, 'Endorsement from the Export Development Council (EDC).', NULL, NULL),
(32, 22, 'Copy of fare refund voucher or certification from the airline authorized signatory<br> that the ticket is unused, non-rebookable, and has no fare refund value.<br>', NULL, NULL),
(33, 22, 'Original TIEZA Official Receipt (passenger and airline copies), if travel tax was<br> paid directly to TIEZA.<br>', NULL, NULL),
(34, 23, 'In case the original passport cannot be presented, a certification from the<br> Bureau of Immigration indicating the passenger’s identity, the immigration status, and the<br> applicable date of arrival shall be submitted.<br>', NULL, 8),
(35, 23, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA<br>', NULL, NULL),
(36, 24, 'Supporting documents for travel tax exemption (kindly refer to the table on<br> TRAVEL TAX EXEMPTION)<br>', NULL, NULL),
(37, 24, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA<br>', NULL, NULL),
(38, 25, 'Supporting documents for reduced travel tax (kindly refer to the table on <br>REDUCED TRAVEL TAX)', NULL, NULL),
(39, 25, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA<br>', NULL, NULL),
(40, 26, 'Certification from the airline that the ticket was downgraded or a certified copy<br> of the airline flight manifest.<br>', NULL, NULL),
(41, 26, 'Original TIEZA Official Receipt (passenger copy), if travel tax was paid directly to TIEZA.<br>', NULL, NULL),
(42, 27, 'Original TIEZA Official Receipt (passenger and airline copies), if travel tax was paid directly to TIEZA.<br>', NULL, NULL),
(43, 28, 'Original TIEZA Official Receipt (passenger and airline copies), if travel tax was paid directly to TIEZA.<br>', NULL, NULL),
(44, 29, 'In case the original passport cannot be presented, the certified true copy<br> / authenticated copy of the birth certificate and photocopy of identification page of passport<br>', NULL, 4),
(45, 29, 'Airline ticket, if already issued<br>', NULL, NULL),
(46, 30, 'Certification from the passenger’s editor or station manager that he/she is<br> an accredited journalist<br>', NULL, NULL),
(47, 30, 'Certification from the Office of the Press Secretary that the travel is<br> in pursuit of journalistic assignment<br>', NULL, NULL),
(48, 31, 'Written authorization from the Office of the President expressly entitling the passenger to<br> the reduced travel tax<br>', NULL, NULL),
(49, 31, 'Airline ticket, if already issued<br>', NULL, NULL),
(50, 32, 'Original Person With Disability (PWD) ID card issued by offices established by<br> the National Council on Disability Affairs (NCDA)<br>', NULL, NULL),
(51, 32, 'Original Overseas Employment Certificate (OEC) / Certified True Copy of Balik-Manggagawa Form / Certified<br> True Copy of the OFW’s Travel Exit Permit / Certification of Accreditation or Registration / OFW’s valid work visa / Work Permit / OFW’s<br> Valid Employment Contract / OFW’s valid company ID<br> / OFW’s recent pay slip<br>', NULL, 5),
(52, 32, 'Certified true copy / authenticated copy of the birth certificate<br>', NULL, 4),
(53, 32, 'Airline ticket, if already issued<br>', NULL, NULL),
(54, 32, 'Certification from the manning agency that the seaman’s dependent is joining the<br> seaman’s vessel<br>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tec_application`
--

CREATE TABLE `tec_application` (
  `id` int(10) UNSIGNED NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ext_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_application` datetime NOT NULL,
  `date_validity` date DEFAULT NULL,
  `passport_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_ticket_issued` date DEFAULT NULL,
  `ticket_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_flight` date NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `denial_id` int(11) DEFAULT NULL,
  `denial_msg` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_processor_id` int(11) DEFAULT NULL,
  `id_picture_2x2_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_identification_page_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket_booking_ref_no_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_file_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applicant_type_id` int(11) DEFAULT NULL,
  `airlines_id` int(11) DEFAULT NULL,
  `generate_to_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tec_application`
--

INSERT INTO `tec_application` (`id`, `last_name`, `first_name`, `middle_name`, `ext_name`, `email`, `mobile_no`, `user_id`, `date_application`, `date_validity`, `passport_no`, `date_ticket_issued`, `ticket_no`, `date_flight`, `section_id`, `country_id`, `status`, `denial_id`, `denial_msg`, `assign_processor_id`, `id_picture_2x2_fn`, `passport_identification_page_fn`, `ticket_booking_ref_no_fn`, `additional_file_fn`, `applicant_type_id`, `airlines_id`, `generate_to_email`, `supervisor_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CABINGAS', 'RONNIE', 'CASTILLO', NULL, 'TMC@ph.hrgworldwide.com', '639188236107', 139, '2020-06-24 09:26:43', '2021-06-24', 'SO021700A', '2020-06-23', '0063368018564', '2020-06-24', NULL, 222, 2, NULL, NULL, 18, 'id_picture_2x2_20200624_LJFsnenRL0dtLSxTE2Wl7ciawCIwBTLvQnp4eUcNAyhcOwNr8Y.jpg', 'passport_identification_page_20200624_LJFsnenRL0dtLSxTE2Wl7ciawCIwBTLvQnp4eUcNAyhcOwNr8Y.png', 'ticket_booking_ref_no_20200624_LJFsnenRL0dtLSxTE2Wl7ciawCIwBTLvQnp4eUcNAyhcOwNr8Y.pdf', NULL, 12, 43, 'TEC-00001_07062020png', 31, 18, 1, NULL, '2020-07-06 15:21:18', NULL),
(2, 'MANAGUEY', 'RITA', 'DAG-OY', NULL, 'segovia.marygrace77@gmail.com', '09162745639', 140, '2020-06-24 10:18:48', '2021-02-25', 'P5326290A', '2020-06-17', 'GF155/QLXDWQ', '2020-07-05', NULL, 85, 2, NULL, NULL, 18, 'id_picture_2x2_20200624_KoOm7TYMfBwk9OJ0lGpsKDME0zATQV3ZG95iXqJNtEyX4jkJvB.png', 'passport_identification_page_20200624_rChUmpiZaaS3ciMWcL7ITUZH5S8eAtJVN9e7eWXTbId3x0F4uj.png', 'ticket_booking_ref_no_20200624_KoOm7TYMfBwk9OJ0lGpsKDME0zATQV3ZG95iXqJNtEyX4jkJvB.jpeg', NULL, 9, 32, 'TEC-00002_06302020png', 31, 18, 1, NULL, '2020-06-30 05:11:59', NULL),
(3, 'BRITANICO', 'RONALD', 'BELDUA', NULL, 'ronaldbrit519@gmail.com', '09178443063', 141, '2020-07-03 10:33:10', '2020-10-03', 'P0171141B', '2020-10-03', 'BR272/KDJNH5', '2020-08-18', NULL, 222, 2, NULL, NULL, 10, 'id_picture_2x2_20200703_izmQMrcVRM7TxDFtLuqQEwizUnlanthzYPfYyQE6EtFDLAmBt7.jpg', 'passport_identification_page_20200703_izmQMrcVRM7TxDFtLuqQEwizUnlanthzYPfYyQE6EtFDLAmBt7.jpg', 'ticket_booking_ref_no_20200703_izmQMrcVRM7TxDFtLuqQEwizUnlanthzYPfYyQE6EtFDLAmBt7.jpg', 'additional_file_20200703_l1ao7ni8R160BCEYeq3bQxzxy2pPaFs7HuQzT36I8KcnpFpRyv.jpg', 9, 28, 'TEC-00003_07032020png', 13, 10, 13, NULL, '2020-07-03 04:55:53', NULL),
(4, 'CHOI', 'TAEYANG', 'CONCEPCION', NULL, 'eccachoi@gmail.com', '09054615450', 142, '2020-07-03 20:49:31', '2020-09-11', 'P0112696B', '2020-07-02', '1802331311570', '2020-07-13', NULL, 114, 2, NULL, NULL, 12, 'id_picture_2x2_20200703_qteJOc51r3gqyupNFA9wLbn2RD4QzwB0n5UgnS0WIVBFeUGbxb.png', 'passport_identification_page_20200703_qteJOc51r3gqyupNFA9wLbn2RD4QzwB0n5UgnS0WIVBFeUGbxb.png', 'ticket_booking_ref_no_20200703_qteJOc51r3gqyupNFA9wLbn2RD4QzwB0n5UgnS0WIVBFeUGbxb.png', NULL, 14, 43, 'TEC-00004_07032020png', 13, 12, 13, NULL, '2020-07-03 13:12:14', NULL),
(5, 'ABULOC', 'KRISTINE BETH', 'CREENCIA', NULL, 'TMC@ph.travelleadersint.com', '09209099476', 143, '2020-07-06 18:03:02', '2020-07-08', 'P4894623B', '2020-07-06', 'KE624 JUS646', '2020-07-07', NULL, 222, 2, NULL, NULL, 15, 'id_picture_2x2_20200706_lE2Mw7E7OvWJKW5UQ4pNsEhm0laQ00SDAB93fTBXJs62UuxRq4.jpg', 'passport_identification_page_20200706_dAYTY3LweKPFqIKP5sv0i4WxCbsIDSVOKFafgv0mCSXnwV5EHm.pdf', 'ticket_booking_ref_no_20200706_dAYTY3LweKPFqIKP5sv0i4WxCbsIDSVOKFafgv0mCSXnwV5EHm.pdf', NULL, 12, 43, 'TEC-00005_07062020png', 17, 15, 17, NULL, '2020-07-06 10:26:51', NULL),
(6, 'DE LEON', 'MYROADE', 'RAMIREZ', NULL, 'TMC@ph.travelleadersint.com', '639452585688', 144, '2020-07-10 16:12:14', '2021-07-10', 'S0021780A', '2020-07-10', 'JBYCW2/HXALOW', '2020-07-10', NULL, 222, 2, NULL, NULL, 32, 'id_picture_2x2_20200710_Zgw9fEAyJPmZACIWO1NHCJSEfBfCBBYjUxcGIQTrmcWS0fqUQG.jpg', 'passport_identification_page_20200710_Zgw9fEAyJPmZACIWO1NHCJSEfBfCBBYjUxcGIQTrmcWS0fqUQG.pdf', 'ticket_booking_ref_no_20200710_Zgw9fEAyJPmZACIWO1NHCJSEfBfCBBYjUxcGIQTrmcWS0fqUQG.pdf', NULL, 12, 43, 'TEC-00006_07102020png', 31, 32, 31, NULL, '2020-07-10 08:23:25', NULL),
(7, 'Reyes', 'Noemi', 'Rola', NULL, 'TMC@ph.hrgworldwide.com', '09209099476', 145, '2020-07-14 15:50:28', '2020-07-16', 'S0021789A', '2020-07-14', 'KE624/LYXRR4', '2020-07-16', NULL, 222, 2, NULL, NULL, 14, 'id_picture_2x2_20200714_nBJhONeS7eXTBJWvtCeLFWlMVVC0xRXRpdXNJPsqmYgPTh7L5f.jpg', 'passport_identification_page_20200714_nBJhONeS7eXTBJWvtCeLFWlMVVC0xRXRpdXNJPsqmYgPTh7L5f.pdf', 'ticket_booking_ref_no_20200714_nBJhONeS7eXTBJWvtCeLFWlMVVC0xRXRpdXNJPsqmYgPTh7L5f.pdf', 'additional_file_20200714_nBJhONeS7eXTBJWvtCeLFWlMVVC0xRXRpdXNJPsqmYgPTh7L5f.pdf', 12, 43, 'TEC-00007_07142020png', 17, 14, 17, NULL, '2020-07-14 07:55:50', NULL),
(8, 'TIGCAL', 'CATALINA', 'GUMAPAS', NULL, 'iangabrielle01@gmail.com', '09354082450', 146, '2020-07-14 20:50:29', '2020-10-30', 'P3153734A', '2020-07-14', 'KF5AQW', '2020-08-03', NULL, 222, 2, NULL, NULL, 16, 'id_picture_2x2_20200714_y76KukysmXgDO81Xd0FmkCA8eDMLO5qnn5xPINh1Cv41tqffre.jpeg', 'passport_identification_page_20200714_y76KukysmXgDO81Xd0FmkCA8eDMLO5qnn5xPINh1Cv41tqffre.jpg', 'ticket_booking_ref_no_20200714_y76KukysmXgDO81Xd0FmkCA8eDMLO5qnn5xPINh1Cv41tqffre.jpg', 'additional_file_20200714_y76KukysmXgDO81Xd0FmkCA8eDMLO5qnn5xPINh1Cv41tqffre.jpg', 9, 28, 'TEC-00008_07142020png', 17, 16, 17, NULL, '2020-07-14 12:54:55', NULL),
(9, 'HAYASHI', 'MA. CORAZON', 'VILLACORTE', NULL, 'cora_hayashi@yahoo.com', '09178321661', 147, '2020-07-15 14:28:36', '2020-08-02', 'P8547159A', '2020-07-12', '079 2405248755', '2020-08-02', NULL, 109, 3, 4, NULL, 32, 'id_picture_2x2_20200715_ANSUHMqPp0Vy8ibOWNf3EkSZbENMmlgv4cLFWazfJlqjaHG1oN.jpg', 'passport_identification_page_20200715_ANSUHMqPp0Vy8ibOWNf3EkSZbENMmlgv4cLFWazfJlqjaHG1oN.jpg', 'ticket_booking_ref_no_20200715_ANSUHMqPp0Vy8ibOWNf3EkSZbENMmlgv4cLFWazfJlqjaHG1oN.jpg', 'additional_file_20200715_sKPlwO2j9J9DKb1d3xOokhiLOwroPi7LhLc6wVk5J8T8Q5STpA.jpg', 9, 51, 'APP-00009_07152020png', 31, 32, 31, NULL, '2020-07-15 09:01:26', NULL),
(10, 'Reyes', 'Noemi', 'Rola', NULL, 'TMC@ph.travelleadersint.com', '09209099476', 148, '2020-07-18 15:48:43', '2020-07-18', 'S0021789A', '2020-07-17', '006-3368041313', '2020-07-18', NULL, 222, 2, NULL, NULL, 19, 'id_picture_2x2_20200718_b6jhGrLzwhqGv1tVvByTmvaEChfDqYwOUyIgPIUkbCx3Qg2mbY.jpg', 'passport_identification_page_20200718_b6jhGrLzwhqGv1tVvByTmvaEChfDqYwOUyIgPIUkbCx3Qg2mbY.pdf', 'ticket_booking_ref_no_20200718_b6jhGrLzwhqGv1tVvByTmvaEChfDqYwOUyIgPIUkbCx3Qg2mbY.pdf', NULL, 12, 14, 'TEC-00010_07182020png', 31, 19, 31, NULL, '2020-07-18 07:56:00', NULL),
(11, 'Daniel', 'Ronaldo', 'Panoncillo', NULL, 'ronaldodaniel1960@gmail.com', '09463939204', 149, '2020-07-20 14:45:49', '2020-08-07', 'P3694897B', '2020-07-20', 'BR272/KPYFS8', '2020-08-07', NULL, 222, 2, NULL, NULL, 52, 'id_picture_2x2_20200720_H0QpB09MfLnYycC6eBNknLJ5wqyBJZt9cN32mMQdpauMQWEjpW.jpg', 'passport_identification_page_20200720_H0QpB09MfLnYycC6eBNknLJ5wqyBJZt9cN32mMQdpauMQWEjpW.jpg', 'ticket_booking_ref_no_20200720_H0QpB09MfLnYycC6eBNknLJ5wqyBJZt9cN32mMQdpauMQWEjpW.jpg', 'additional_file_20200720_H0QpB09MfLnYycC6eBNknLJ5wqyBJZt9cN32mMQdpauMQWEjpW.jpg', 9, 28, 'TEC-00011_07202020png', 31, 52, 31, NULL, '2020-07-20 07:58:02', NULL),
(12, 'Gironella', 'Fernando', 'Bitao', NULL, 'gerliegironellacarranza@gmail.com', 'P9979879A', 150, '2020-07-20 16:28:39', '2020-07-25', '09178127027', '2020-07-20', 'QR933/U36CHC', '2020-07-25', NULL, 106, 2, NULL, NULL, 18, 'id_picture_2x2_20200720_YePDiha56y3fK4DJoHnb4dLplEKduZS0jnLbBEZKSm6tz8XAnK.jpeg', 'passport_identification_page_20200720_YePDiha56y3fK4DJoHnb4dLplEKduZS0jnLbBEZKSm6tz8XAnK.jpeg', 'ticket_booking_ref_no_20200720_YePDiha56y3fK4DJoHnb4dLplEKduZS0jnLbBEZKSm6tz8XAnK.jpg', 'additional_file_20200720_YePDiha56y3fK4DJoHnb4dLplEKduZS0jnLbBEZKSm6tz8XAnK.pdf', 9, 53, 'TEC-00012_07202020png', 31, 18, 31, NULL, '2020-07-20 08:58:17', NULL),
(13, 'OCTAVIO', 'BRIGIDA', 'MANALO', NULL, 'bmoctavio@gmail.com', '09985384311', 151, '2020-07-20 16:42:45', '2020-07-25', 'P5896481A', '2020-07-13', 'PR104/K9IICB', '2020-07-25', NULL, 222, 2, NULL, NULL, 32, 'id_picture_2x2_20200720_aYmiIwwZUy0rT6XjDLWRzLyQqMlHRjCGDkBdSF1Kq3B9aHJeg6.jpg', 'passport_identification_page_20200720_aYmiIwwZUy0rT6XjDLWRzLyQqMlHRjCGDkBdSF1Kq3B9aHJeg6.pdf', 'ticket_booking_ref_no_20200720_aYmiIwwZUy0rT6XjDLWRzLyQqMlHRjCGDkBdSF1Kq3B9aHJeg6.pdf', 'additional_file_20200720_aYmiIwwZUy0rT6XjDLWRzLyQqMlHRjCGDkBdSF1Kq3B9aHJeg6.pdf', 9, 51, 'TEC-00013_07202020png', 31, 32, 31, NULL, '2020-07-20 08:48:39', NULL),
(14, 'ACASIO', 'RORIE RICHIE', 'MENDOZA', NULL, 'tmc@ph.travelleadersint.com', '09175849923', 152, '2020-07-20 17:09:32', '2020-07-22', 'S0006988A', '2020-07-20', '001-3368041333', '2020-07-22', NULL, 222, 2, NULL, NULL, 19, 'id_picture_2x2_20200721_glzjIJytMbYveGKjnqSDQCyqF30TvhvuV9xFmRGyJt9o7veqDg.jpg', 'passport_identification_page_20200721_glzjIJytMbYveGKjnqSDQCyqF30TvhvuV9xFmRGyJt9o7veqDg.pdf', 'ticket_booking_ref_no_20200721_glzjIJytMbYveGKjnqSDQCyqF30TvhvuV9xFmRGyJt9o7veqDg.pdf', NULL, 12, 37, 'TEC-00014_07212020png', 31, 19, 31, NULL, '2020-07-21 02:38:40', NULL),
(15, 'BENAURO', 'DOMINIC', 'BOSE', NULL, 'tmc@ph.travelleadersint.com', '09209099476', 153, '2020-07-21 08:59:05', '2021-07-20', 'S0021792A', '2020-07-21', 'PR102/W8G8PQ', '2020-07-22', NULL, 222, 2, NULL, NULL, 10, 'id_picture_2x2_20200721_WFEDfpiHMEGBUmap919oB8d9QLSrNBy1FeSQpKuHXkrTrwXhlP.png', 'passport_identification_page_20200721_WFEDfpiHMEGBUmap919oB8d9QLSrNBy1FeSQpKuHXkrTrwXhlP.png', 'ticket_booking_ref_no_20200721_WFEDfpiHMEGBUmap919oB8d9QLSrNBy1FeSQpKuHXkrTrwXhlP.pdf', NULL, 12, 51, 'TEC-00015_07212020png', 13, 10, 13, NULL, '2020-07-21 01:08:48', NULL),
(16, 'Madanguit', 'Hans Kevin', 'R', NULL, 'hkrmadanguit@gmail.com', '09064843975', 154, '2020-07-21 09:04:15', '2020-08-01', 'P0087078B', '2020-07-21', '0792405278707', '2020-08-01', NULL, 212, 2, NULL, NULL, 11, 'id_picture_2x2_20200721_e3O6hIR83WDLZotuenyHrm5lRYdSaJE04ymiKMFtnTtOl1wsZD.jpg', 'passport_identification_page_20200721_e3O6hIR83WDLZotuenyHrm5lRYdSaJE04ymiKMFtnTtOl1wsZD.pdf', 'ticket_booking_ref_no_20200721_e3O6hIR83WDLZotuenyHrm5lRYdSaJE04ymiKMFtnTtOl1wsZD.pdf', 'additional_file_20200721_e3O6hIR83WDLZotuenyHrm5lRYdSaJE04ymiKMFtnTtOl1wsZD.pdf', 12, 51, 'TEC-00016_07212020png', 13, 11, 13, NULL, '2020-07-21 01:23:41', NULL),
(17, 'miranda', 'jesus jericson', 'cosare', NULL, 'tmc@ph.travelleadersint.com', '639209099476', 155, '2020-07-21 09:13:29', '2020-07-22', 'S0021791A', '2020-07-21', 'W8G8PQ', '2020-07-22', NULL, 222, 2, NULL, NULL, 12, 'id_picture_2x2_20200721_edGQTqPFmCltW3o2qAMpX02b5UZkbo8dGBH2WQQZ6wGbAJsjuz.png', 'passport_identification_page_20200721_edGQTqPFmCltW3o2qAMpX02b5UZkbo8dGBH2WQQZ6wGbAJsjuz.png', 'ticket_booking_ref_no_20200721_edGQTqPFmCltW3o2qAMpX02b5UZkbo8dGBH2WQQZ6wGbAJsjuz.png', 'additional_file_20200721_edGQTqPFmCltW3o2qAMpX02b5UZkbo8dGBH2WQQZ6wGbAJsjuz.png', 12, 51, 'TEC-00017_07212020png', 13, 12, 13, NULL, '2020-07-21 01:14:58', NULL),
(18, 'STRICKLAND', 'MARIA ISCARLET', 'EBOL', NULL, 'TMC@PH.TRAVELLEADERSINT.COM', '639209099476', 156, '2020-07-23 10:20:49', '2020-08-02', '910314142', '2020-07-23', '0063368041362', '2020-08-02', NULL, 222, 2, NULL, NULL, 18, 'id_picture_2x2_20200723_a7d6Qt1CawCZNsig7q0CuTKl36aHWA1Mcw7bPlZTllheDAEv6b.jpg', 'passport_identification_page_20200723_a7d6Qt1CawCZNsig7q0CuTKl36aHWA1Mcw7bPlZTllheDAEv6b.pdf', 'ticket_booking_ref_no_20200723_a7d6Qt1CawCZNsig7q0CuTKl36aHWA1Mcw7bPlZTllheDAEv6b.pdf', NULL, 12, 51, 'TEC-00018_07232020png', 31, 18, 31, NULL, '2020-07-23 02:33:13', NULL),
(19, 'STRICKLAND', 'SOFIA MARIE', '', NULL, 'TMC@PH.TRAVELLEADERSINT.COM', '639209099476', 157, '2020-07-23 10:47:37', '2020-08-02', '910314141', '2020-07-22', '006-3368041363', '2020-08-02', NULL, 222, 2, NULL, NULL, 19, 'id_picture_2x2_20200723_TGgGBbI3ge6QMsUDLxG8f0VpWTTsgkLnAXvdxTupeMDDY7l0So.jpg', 'passport_identification_page_20200723_TGgGBbI3ge6QMsUDLxG8f0VpWTTsgkLnAXvdxTupeMDDY7l0So.pdf', 'ticket_booking_ref_no_20200723_TGgGBbI3ge6QMsUDLxG8f0VpWTTsgkLnAXvdxTupeMDDY7l0So.pdf', NULL, 12, 51, 'TEC-00019_07232020png', 31, 19, 31, NULL, '2020-07-23 02:53:13', NULL),
(20, 'STRICKLAND', 'CLAIRE', 'TALANAY', NULL, 'tmc@ph.travelleadersint.com', '639209099476', 158, '2020-07-23 11:40:25', '2020-08-02', '910314139', '2020-07-22', '0063368041361', '2020-08-02', NULL, 222, 2, NULL, NULL, 32, 'id_picture_2x2_20200723_0aDYhhwu9mdbu33fLF92jhPhS63zipKSKBX5sGwdKlsiqkfZ3v.jpg', 'passport_identification_page_20200723_0aDYhhwu9mdbu33fLF92jhPhS63zipKSKBX5sGwdKlsiqkfZ3v.pdf', 'ticket_booking_ref_no_20200723_0aDYhhwu9mdbu33fLF92jhPhS63zipKSKBX5sGwdKlsiqkfZ3v.pdf', NULL, 12, 51, 'TEC-00020_07232020png', 31, 32, 31, NULL, '2020-07-23 03:48:28', NULL),
(21, 'JACLA', 'JOHN PAUL', 'S', NULL, 'TMC@PH.TRAVELLEADERSINT.COM', '+639209099476', 159, '2020-07-24 13:49:10', '2020-07-28', 'S0012515A', '2020-07-24', '0793368041378', '2020-07-28', NULL, 222, 2, NULL, NULL, 11, 'id_picture_2x2_20200724_FcyDS7gpVWXdzbSSZRAtBqg4HyWPPcSO3NplW1oLDyMmqoscHA.png', 'passport_identification_page_20200724_FcyDS7gpVWXdzbSSZRAtBqg4HyWPPcSO3NplW1oLDyMmqoscHA.pdf', 'ticket_booking_ref_no_20200724_FcyDS7gpVWXdzbSSZRAtBqg4HyWPPcSO3NplW1oLDyMmqoscHA.pdf', NULL, 12, 51, 'TEC-00021_07242020png', 13, 11, 13, NULL, '2020-07-24 05:54:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tec_application_upload`
--

CREATE TABLE `tec_application_upload` (
  `id` int(11) NOT NULL,
  `tec_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `upload_by` int(11) DEFAULT NULL,
  `upload_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tr_amount`
--

CREATE TABLE `tr_amount` (
  `id` int(11) NOT NULL,
  `amount` double(6,2) NOT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tr_amount`
--

INSERT INTO `tr_amount` (`id`, `amount`, `description`, `created_at`, `deleted_at`) VALUES
(1, 1620.00, 'Php 1,620.00 - Economy and Business Class ticket', '2020-07-18 00:42:28', NULL),
(2, 2700.00, 'Php 2,700.00 - First Class Ticket', '2020-07-18 00:42:28', NULL),
(3, 810.00, 'Php 810.00 - Standard Reduced Travel Tax', '2020-07-18 00:43:13', NULL),
(4, 300.00, 'Php 300.00 - Privileged Reduced Travel Tax for Dependents of Overseas Filipino Workers (OFWs)', '2020-07-18 00:43:25', NULL),
(5, 400.00, 'Php 400.00 - First Class Privileged Reduced Travel Tax for Dependents of Overseas Filipino Workers (OFWs)', '2020-07-18 00:43:35', NULL),
(6, 1350.00, 'Php 1,350.00 - First Class Standard Reduced Travel Tax', '2020-07-18 00:43:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tr_application`
--

CREATE TABLE `tr_application` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_minor` tinyint(4) DEFAULT NULL,
  `guardian_last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guardian_middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_application` datetime NOT NULL,
  `date_validity` date DEFAULT NULL,
  `passport_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_ticket_issued` date DEFAULT NULL,
  `ticket_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_flight` date NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `denial_id` int(11) DEFAULT NULL,
  `denial_msg` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_processor_id` int(11) DEFAULT NULL,
  `id_picture_2x2_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_identification_page_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket_booking_ref_no_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_file_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acknowledge_receipt_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_353_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_353_airline_fn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `applicant_type_id` int(11) DEFAULT NULL,
  `airlines_id` int(11) DEFAULT NULL,
  `generate_to_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor_id` int(11) DEFAULT NULL,
  `type_applicant_id` int(11) DEFAULT NULL,
  `travel_tax_amount` decimal(15,2) NOT NULL,
  `type_travel_tax_refund` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bank_account_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refund_amount` decimal(15,2) DEFAULT NULL,
  `reason_refund` int(11) DEFAULT NULL,
  `reason_refund_others` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_refund_status` int(11) DEFAULT NULL,
  `tieza_official_receipt_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tr_application_upload`
--

CREATE TABLE `tr_application_upload` (
  `id` int(11) NOT NULL,
  `tr_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `section_upload_id` int(11) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `upload_by` int(11) DEFAULT NULL,
  `upload_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `type_applicants`
--

CREATE TABLE `type_applicants` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `type_applicants`
--

INSERT INTO `type_applicants` (`id`, `name`) VALUES
(1, 'Foreign Diplomatic and Consular Officials and Members of their Staff'),
(2, 'Officials, Consultants, Experts, and Employees of the United Nations (UN) Organization and its agencies'),
(3, 'United States (US) Military Personnel including dependents and other US nationals with fares paid for by the US government or on US Government-owned or chartered transport facilities'),
(4, 'Overseas Filipino Worker Directly Hired Abroad going to their jobsite'),
(5, 'Crew member of airplanes plying international routes'),
(6, 'Philippine Foreign Service Personnel officially assigned abroad and their dependents'),
(7, 'Officials and Employees of the Philippine Government traveling on official business (excluding Government-Owned and Controlled Corporations)'),
(8, 'Grantee of foreign government funded trips\r\n'),
(9, 'Bona-fide Student with approved foreign scholarship by appropriate Philippine government agency'),
(10, 'Infant (Up to second birthday on date of travel)'),
(11, 'Personnel (and their dependents) of multinational companies with regional headquarters, but not engaged in business, in the Philippines'),
(12, 'Those authorized by the President of the Republic of the Philippines for reasons of national interest'),
(13, 'Balikbayans whose stay in the Philippines is less than one (1) year as provided under R.A. 6768'),
(14, 'Family members of former Filipinos accompanying the latter as provided under R.A. 6768');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `test` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `last_name`, `first_name`, `middle_name`, `email`, `username`, `password`, `level`, `status`, `remember_token`, `mobile_no`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`, `test`) VALUES
(1, 'Simbulan', 'Angie', 'P', 'angiesimbulan.tieza@gmail.com', 'APS0001', '$2y$10$D.8QZRQle4gz.qBO0aSNKOqKPKadsC1rv/ETJaHliFRp/4s6ZcGIC', 0, 1, 'va0F19O0MwwoXKZsRdzPc7HB5Nky4rAverYBnnr9yooGOMiKHOksWuoCMBqM', NULL, 1, 5, '2020-06-11 03:01:29', '2020-06-14 23:17:04', NULL, NULL),
(2, 'Miranda', 'Xybriel Dennis', 'Mista', 'xybrielmiranda.tieza@gmail.com', 'XMM0002', '$2y$10$XNjsgkjhapLbgf3pSP2J9ubdd1AeGf77JqIW0ZxqJsB6D/piot4/y', 2, 1, '2CEdJPHhc4N6FaUkHKMkJxH93hd7RTXReoHmWYL4dZOt6UtVZN6uJL0zG3u6', NULL, 1, 1, '2020-06-11 03:01:29', '2020-06-18 00:51:06', NULL, NULL),
(3, 'test', 'test', 'test', 'test@mail.com', 'test', '$2y$10$ncYR3St.JLGGz8fb09RWZOhi7Ek7GGd1tckO0n1ciF8o26vp6PZaG', 3, 1, '1DJusEep3TvospLLJ0sEARqko1dpS4mxufrHAt3dtbwSjubDHh1VV7NVqJXH', NULL, 2, 2, NULL, '2020-06-11 04:41:31', '2020-06-11 11:17:12', NULL),
(4, 'Placido', 'Emmanuel', 'Santiago', 'emmanplacido.tieza@gmail.com', 'ESP0004', '$2y$10$E4cF5f5ckOI5an4/BWvUFOuy./D0FeoDAUZ6o6OczTALbLWCvf9We', 1, 1, 'k4X5hebBRsIPuB2MDse2n20mVk4Yq5rA5scVsA9SgmhLHvfQjOSjcFETrTBn', NULL, 2, 1, NULL, '2020-06-21 23:51:03', NULL, NULL),
(5, 'Dela Cruz', 'Raquel', 'Sagun', 'test@mail.com', 'RSD0005', '$2y$10$nDIIaD8mCkgwWMupVWUvjOORjIouivKUqH1JhN0yWzhLPpt2GBGWS', 0, 1, 'JeLjmfrxxm0PsBC1I89kzJku8F9pacU9rrNpV6gLRAeQ4NYZavr1ubjGdZuB', NULL, 2, 2, NULL, '2020-06-14 03:21:56', NULL, NULL),
(6, 'Tada', 'Meriel', 'Sagun', 'test@mail.com', 'MST0006', NULL, 3, 1, NULL, NULL, 2, 4, NULL, '2020-06-14 08:29:22', '2020-06-14 23:36:50', NULL),
(7, 'Test', 'Test', 'Test', 'test@mail.com', 'TTT0007', NULL, 3, 1, NULL, NULL, 2, 2, NULL, '2020-06-12 01:25:36', '2020-06-12 01:25:39', NULL),
(8, 'T', 'S', 'S', 'test@mail.com', 'SST0008', NULL, 3, 1, NULL, NULL, 2, 2, NULL, '2020-06-12 01:27:28', '2020-06-12 01:27:40', NULL),
(9, 'Jeruz', 'Daryl John', 'D', 'test@mail.com', 'DDJ0009', '$2y$10$ZsRZpsMqXzNPzo8h3rcPc.vcdmMB8m6RMUXtHO5GpLrVBA19UjD9C', 2, 1, 'vTQPq4zJ8eE5uJtYegSOFL34O1l6WvPJe6uRqhmaBr7fCH0w1N6HmwqzUmTv', NULL, 2, 5, NULL, '2020-06-14 13:36:19', NULL, NULL),
(10, 'Rodriguez', 'Emerson', 'L', 'test@mail.com', 'ELR0010', '$2y$10$WFPqRRim.R0pxRDhcBGbT.LOm/RP.xGuc7NfB8FnwTjfcEuIxsMKS', 2, 1, 'pcncrfVRE4UGXvlyX6uFvcXzlhxKDTgR5GTLghb8L6DZmZDzRcxGKSfGjLhe', NULL, 2, 1, NULL, '2020-06-14 23:57:17', NULL, NULL),
(11, 'Lagrosas', 'Paquito  Jr.', 'D', 'test@mail.com', 'PDL0011', '$2y$10$O3JHi2nhdWPOVPldR.Z.Zu4MHRTPbwObkAx7uU.gspStODy9KJfRe', 2, 1, NULL, NULL, 2, 1, NULL, '2020-06-14 23:48:22', NULL, NULL),
(12, 'Magdaraog', 'Roda', 'M', 'test@mail.com', 'RMM0012', '$2y$10$rGJJxyOof2B7Cwr2E0VGoOw3oeNo5ZBLUAHJkiZK5GGBaIURZB7iO', 2, 1, 'Y8oTQyigb9nzL4bDxyCxEpJyetiYeASVUSKaB24fH6cbmSuiep7c44NohGsh', NULL, 2, 1, NULL, '2020-06-14 23:51:22', NULL, NULL),
(13, 'Valleser', 'Marjon', 'B', 'test@mail.com', 'MBV0013', '$2y$10$7o/s5FA9d5.s8Uvc/3YZ0uhjeyaGjrLXRufytfI8MVSSd/JLwhD6S', 1, 1, 'NKtvlX9oLu3aBHvMXZti2zIlkEaqF7pW6KLIwMZIe71oG5LuGESB3KgTvHiI', NULL, 2, 1, NULL, '2020-06-14 23:53:26', NULL, NULL),
(14, 'Divina', 'Andrea Camille', 'M', 'test@mail.com', 'AMD0014', '$2y$10$OBUUrqnFYrggNjVhwYhHHeEB4LMt2C151q6pgIuI2EzB9lDv6kAQi', 2, 1, NULL, NULL, 2, 1, NULL, '2020-06-14 23:43:03', NULL, NULL),
(15, 'Atutubo', 'Nhea Mea', 'D', 'test@mail.com', 'NDA0015', '$2y$10$2CIzlMUySP8uY3NkdkhKeu.8GnQfHJMwltMoSJ0OtU5zwFLy9SHku', 2, 1, 'xnasZd2m8qMfiThVZkiZHVuqotyiYM9LGgPQND5RRUVGOzyiP47Z3MfmdFpP', NULL, 2, 1, NULL, '2020-06-14 23:40:26', NULL, NULL),
(16, 'Si', 'Ryan Louise', 'N', 'test@mail.com', 'RNS0016', '$2y$10$EDh3Y8P/5M7u/8q0DxDIAuNRochlTKk/5vGNXd.gaNpEOBJJp/juW', 2, 1, 'JZ0cPDaKoyF57mO2mpi84YSCCzABEPcDM9K0dQCYOXyAX2qSe2t9G0FUTMTv', NULL, 2, 1, NULL, '2020-06-14 23:55:17', NULL, NULL),
(17, 'Garcia', 'Maria Theresa', 'M', 'test@mail.com', 'MMG0017', '$2y$10$4Wv53GdVNzzff3tana6Qz.IQUMp82FGO9g7OHFFS9UBK0hoKnv1ny', 1, 1, 'CNSGoHZvDiLKbkLLt1j8HITGOWGlurgm9ncTiX5hYoUdxHOwf3ArHCfffa7K', NULL, 2, 1, NULL, '2020-06-16 02:49:09', NULL, NULL),
(18, 'Pimentel', 'Charina', 'C', 'test@mail.com', 'CCP0018', '$2y$10$Pfw3pY9tDG/pKU9NkszqnOoXFUg3susBBKN23zhsNCgmOwy7r6IHG', 2, 1, 'FrslNV7KgMNbjqK1dzSQXAwlZIFk27Rq3b74UdfNMELkIsvZVmLrrVvboqKP', NULL, 2, 1, NULL, '2020-07-20 03:45:46', NULL, NULL),
(19, 'Lim', 'Allen Angelo', 'L', 'test@mail.com', 'ALL0019', '$2y$10$V30R9N44zT.Qyb9SKyCs3eb/d1Kfo/XAJaFCT0NJphgTgG5mWdKzW', 2, 1, NULL, NULL, 2, 1, NULL, '2020-06-14 23:49:14', NULL, NULL),
(20, 'Concepcion', 'Arthur', 'S', 'test@mail.com', 'ASC0020', '$2y$10$3UiZYjilkv4Uv1bTigrWROOQwS/IKVCX2NsS2BVibx/59DpGLZNgi', 2, 1, 'L2uxgADKpiyhb0PCad8iQa1F4xEXnlTBUDE5bORMf8Q58FBdTzMM1reB1LWt', NULL, 2, 2, NULL, '2020-06-13 06:33:42', '2020-06-14 23:36:22', NULL),
(21, 'SIMBULAN', 'CAMILLE GABRIELLE', 'PICACHE', NULL, 'CPS0021', NULL, 3, 1, NULL, NULL, 1, 24, '2020-06-14 08:39:05', '2020-06-14 14:01:59', '2020-06-14 23:36:41', NULL),
(22, 'BERNARDINO', 'CARLOTA', 'PICACHE', NULL, 'CPB0022', NULL, 3, 1, NULL, NULL, 1, 4, '2020-06-14 08:53:39', '2020-06-14 09:31:56', '2020-06-14 23:33:20', NULL),
(23, 'Cruz', 'Bumbo', 'S', 'bumbocruz@tieza.gov.ph', 'BSC0023', '$2y$10$tHpESOaQvPQkH035/QesdODuW7/qpzLbsoEFu9Vci7UUTtHBdmHaS', 1, 1, '2U7SL0UGbpgZRGb3EDF8of3ylEwRfIYH0FdfSjU0WznNovo8G2VvaMWupmwu', NULL, 5, NULL, NULL, NULL, NULL, NULL),
(24, 'Dela Cruz', 'Rocky', 'SAGUN', 'test@gmail.com', 'RSD0024', '$2y$10$ZtUoGdDo7qvUDpPYFOUzb.SjiQkBhiaUDz4IgH8I8ZT5LULgWUGcy', 1, 1, 'tUJeB4Emwo93hv7HxrcOSZYbtkoity5CTvUL1tZ0LMS18qbPeTMAOYoKowvL', NULL, 5, 5, NULL, '2020-06-14 23:37:30', NULL, NULL),
(25, 'TARAY', 'LALAINE', 'BERNARDO', NULL, 'LBT0025', NULL, 3, 1, NULL, NULL, 1, 24, '2020-06-14 14:04:18', '2020-06-14 14:38:50', '2020-06-14 23:37:04', NULL),
(26, 'SANTOS', 'ARLENE', 'RAMOS', NULL, 'ARS0026', NULL, 3, 1, NULL, NULL, 9, 9, '2020-06-15 00:36:54', '2020-06-15 00:36:54', '2020-06-15 14:09:22', NULL),
(27, 'SANTOS', 'MARY', 'RAMOS', NULL, 'MRS0027', NULL, 3, 1, NULL, NULL, 9, 9, '2020-06-15 01:48:47', '2020-06-15 03:11:30', '2020-06-15 14:09:50', NULL),
(28, 'DELA CRUZ', 'MINERVA', 'RAMOS', NULL, 'MRD0028', NULL, 3, 1, NULL, NULL, 9, 24, '2020-06-15 04:00:05', '2020-06-15 04:03:34', '2020-06-15 14:07:33', NULL),
(29, 'SANTOS', 'MARICEL', 'REYES', NULL, 'MRS0029', NULL, 3, 1, NULL, NULL, 9, 24, '2020-06-15 12:12:08', '2020-06-15 12:17:25', '2020-06-15 14:10:14', NULL),
(30, 'SANTOS', 'MARICEL', 'REYES', NULL, 'MRS0030', NULL, 3, 1, NULL, NULL, 9, 9, '2020-06-15 12:12:29', '2020-06-15 12:12:29', NULL, NULL),
(31, 'Samera', 'Genaro', 'V', 'orangesam.3rd@gmail.com', 'GVS0031', '$2y$12$SKVmD4xXFG.Z0/BNa.uMYeti2cBGFrydrUspTkgD/Hxn4/FYGp7f6', 1, 1, 'lixzwg8cO4sCw2x7jCHKFdYWd17EBRAbNEn8ouUQjcQCmN60MaDQe8ZFiGN4', NULL, 1, 5, NULL, '2020-06-15 14:05:10', NULL, NULL),
(32, 'Estozo', 'Maiko', 'M', 'maikoestozo@gmail.com', 'MME0032', '$2y$10$7r9ZqXmBSGC3/O5XZNZRKuhAeNYSEtGvFrMXurAaKpi81Sll6aftO', 2, 1, 'UzRM5b11i3i8Kv7UWv5XlH7rBfIWPrxqzPNA4aAAEezNmqu2f6U1bGAhDEPm', NULL, 1, 1, NULL, '2020-06-15 13:27:02', NULL, NULL),
(33, 'Rodriguez', 'Emerson', 'L', NULL, 'ELR0033', NULL, 3, 1, NULL, NULL, 11, 13, '2020-06-15 13:34:58', '2020-06-15 16:08:36', '2020-06-15 14:05:54', NULL),
(39, 'Lim', 'Allen Angelo', 'Zulueta', NULL, 'AZL0039', NULL, 3, 1, NULL, NULL, 14, 17, '2020-06-15 13:42:54', '2020-06-15 13:46:13', '2020-06-15 14:08:28', NULL),
(40, 'PIMENTEL', 'CHARINA', 'C', NULL, 'CCP0040', NULL, 3, 1, NULL, NULL, 16, 31, '2020-06-15 13:42:58', '2020-06-16 06:52:08', '2020-06-15 14:07:00', NULL),
(41, 'Lim', 'Allen Angelo', 'Zulueta', NULL, 'AZL0041', NULL, 3, 1, NULL, NULL, 14, 14, '2020-06-15 13:43:38', '2020-06-15 13:43:38', '2020-06-15 14:08:40', NULL),
(49, 'lim', 'allen angelo', 'zulueta', NULL, 'azl0049', NULL, 3, 1, NULL, NULL, 16, 16, '2020-06-15 14:00:33', '2020-06-15 14:39:21', '2020-06-15 14:08:54', NULL),
(50, 'ESTOZO', 'MAIKO', 'T', NULL, 'MTE0050', NULL, 3, 1, NULL, NULL, 15, 17, '2020-06-15 14:15:40', '2020-06-15 14:22:23', NULL, NULL),
(51, 'Estozo', 'Maiko', 'T', NULL, 'MTE0051', NULL, 3, 1, NULL, NULL, 15, 15, '2020-06-15 14:47:38', '2020-06-15 14:51:46', NULL, NULL),
(52, 'Rimas', 'Maricel', 'Maala', 'rimas@gmail.com', 'MMR0052', '$2y$10$VqZl7YwxDu9SzXRWE.pDF.JLlbrCj3Fk4JW.86gEyfOpI.EM6F0nm', 2, 1, 'KOufZsQgWWcpMGCXeu6OCzB5yvQlotkfNITI7F2dRxIeEcf9xtKuMETTQUIf', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(53, 'magdaraog', 'rhoda', 'samar', NULL, 'rsm0053', NULL, 3, 1, NULL, NULL, 12, 13, '2020-06-16 02:38:19', '2020-06-16 03:07:05', NULL, NULL),
(54, 'Lagrosas', 'Paquito Jr.', 'D', NULL, 'PDL0054', NULL, 3, 1, NULL, NULL, 10, 10, '2020-06-16 03:21:26', '2020-06-16 03:21:26', NULL, NULL),
(55, 'Magdaraog', 'Rhoda', 'R', NULL, 'RRM0055', NULL, 3, 1, NULL, NULL, 11, 11, '2020-06-16 03:22:52', '2020-06-16 03:22:52', NULL, NULL),
(56, 'Pimentel', 'Charina', 'C', NULL, 'CCP0056', NULL, 3, 1, NULL, NULL, 18, 13, '2020-06-16 04:38:30', '2020-06-16 04:41:31', NULL, NULL),
(57, 'Guerrero', 'Jansen', 'B', NULL, 'JBG0057', NULL, 3, 1, NULL, NULL, 52, 31, '2020-06-16 06:38:22', '2020-06-16 07:12:56', NULL, NULL),
(58, 'red', 'velvet', 'hue', NULL, 'vhr0058', NULL, 3, 1, NULL, NULL, 18, 18, '2020-06-16 07:06:00', '2020-06-16 07:06:00', NULL, NULL),
(59, 'Red', 'Velvet', 'Hue', NULL, 'VHR0059', NULL, 3, 1, NULL, NULL, 19, 31, '2020-06-16 07:22:39', '2020-06-16 07:31:48', NULL, NULL),
(60, 'Valleser', 'Philomena Faith', 'P.', NULL, 'PPV0060', NULL, 3, 1, NULL, NULL, 52, 52, '2020-06-16 07:37:03', '2020-06-16 07:37:03', NULL, NULL),
(61, 'GUERRERO', 'JANSEN', 'B', NULL, 'JBG0061', NULL, 3, 1, NULL, NULL, 32, 31, '2020-06-16 08:15:42', '2020-06-16 08:34:14', NULL, NULL),
(62, 'RED', 'VELVET', 'HUE', NULL, 'VHR0062', NULL, 3, 1, NULL, NULL, 52, 31, '2020-06-16 08:55:04', '2020-06-16 09:15:58', NULL, NULL),
(63, 'PONTEJOS', 'JESSAMIE', 'ONA', NULL, 'JOP0063', NULL, 3, 1, NULL, NULL, 32, 13, '2020-06-16 09:14:33', '2020-06-16 10:02:43', NULL, NULL),
(64, 'magdaraog80980', 'aaliyah999', '767886', NULL, 'a7m0064', NULL, 3, 1, NULL, NULL, 12, 12, '2020-06-16 10:48:09', '2020-06-16 11:06:09', NULL, NULL),
(65, 'Mocha', 'Usona', 'Faker', NULL, 'UFM0065', NULL, 3, 1, NULL, NULL, 19, 31, '2020-06-17 04:42:40', '2020-06-17 05:48:24', NULL, NULL),
(66, 'HEMSWORTH', 'CHRIS', 'MACHO', NULL, 'CMH0066', NULL, 3, 1, NULL, NULL, 32, 31, '2020-06-17 08:31:07', '2020-06-17 10:28:00', NULL, NULL),
(67, 'richards', 'alden', 'momo', NULL, 'amr0067', NULL, 3, 1, NULL, NULL, 52, 52, '2020-06-17 10:34:39', '2020-06-17 10:34:39', NULL, NULL),
(68, 'magtanggol', 'victor', 'b', NULL, 'vbm0068', NULL, 3, 1, NULL, NULL, 12, 12, '2020-06-17 12:46:24', '2020-06-17 12:46:24', NULL, NULL),
(69, 'gonzaga', 'thorn', 'reco', NULL, 'trg0069', NULL, 3, 1, NULL, NULL, 18, 18, '2020-06-17 13:02:50', '2020-06-17 13:02:50', NULL, NULL),
(70, 'DELA CRUZ', 'JUAN', 'M', NULL, 'JMD0070', NULL, 3, 1, NULL, NULL, 32, 32, '2020-06-17 13:08:34', '2020-06-17 13:08:34', NULL, NULL),
(71, 'dela cruz', 'juan', 'cano', NULL, 'jcd0071', NULL, 3, 1, NULL, NULL, 52, 52, '2020-06-17 13:24:43', '2020-06-17 13:24:43', NULL, NULL),
(72, 'park', 'saeroyi', 'j', NULL, 'sjp0072', NULL, 3, 1, NULL, NULL, 52, 52, '2020-06-17 13:39:18', '2020-06-17 13:39:18', NULL, NULL),
(73, 'pimentel', 'charina', 'c', NULL, 'ccp0073', NULL, 3, 1, NULL, NULL, 52, 52, '2020-06-17 13:51:57', '2020-06-17 13:51:57', NULL, NULL),
(74, 'BERNARDINO', 'LALAINE', 'PICACHE', 'milo_camille@yahoo.com', 'LPB0074', NULL, 3, 1, NULL, '09876545678', 2, 2, '2020-06-18 01:12:30', '2020-06-18 01:12:30', NULL, NULL),
(75, 'magdaraog', 'rhoda', 's', 'rm@yahoo.com', 'rsm0075', NULL, 3, 1, NULL, '928398048230', 12, 12, '2020-06-19 04:50:57', '2020-06-19 04:50:57', NULL, NULL),
(76, 'smith', 'sam', 'h', 'samsmith@gmail.com', 'shs0076', NULL, 3, 1, NULL, '80708708808', 12, 12, '2020-06-19 06:00:57', '2020-06-19 06:00:57', NULL, NULL),
(77, 'PARK', 'SAEROYI', 'J', 'parkseojoon@baeksang.com', 'SJP0077', NULL, 3, 1, NULL, '0922134679', 32, 31, '2020-06-19 09:08:44', '2020-06-19 15:36:59', NULL, NULL),
(78, 'RED', 'VELVET', 'HUE', 'charm26.cp@gmail.com', 'VHR0078', NULL, 3, 1, NULL, '09198007816', 32, 31, '2020-06-19 09:14:36', '2020-06-19 15:38:20', NULL, NULL),
(79, 'LOONYO', 'DJ', 'O', 'DJloonyo@engeng.com', 'DOL0079', NULL, 3, 1, NULL, '09097890012', 32, 32, '2020-06-19 09:23:48', '2020-06-19 09:23:48', NULL, NULL),
(80, 'SIMBULAN', 'CAMILLE GABRIELLE', 'PICACHE', 'camillegabrielle0818@gmail.com', 'CPS0080', NULL, 3, 1, NULL, '09291625284', 2, 2, '2020-06-20 06:28:36', '2020-06-20 06:28:36', NULL, NULL),
(81, 'SIMBULAN', 'CAMILLE GABRIELLE', 'PICACHE', 'camillegabrielle0818@gmail.com', 'CPS0081', NULL, 3, 1, NULL, '09291625284', 2, 2, '2020-06-20 08:48:07', '2020-06-20 08:48:07', NULL, NULL),
(82, 'SIMBULAN', 'CAMILLE GABRIELLE', 'PICACHE', 'camillegabrielle0818@gmail.com', 'CPS0082', NULL, 3, 1, NULL, '09291625284', 2, 2, '2020-06-20 09:03:49', '2020-06-20 09:03:49', NULL, NULL),
(83, 'TARAY', 'LALAINE', 'BERNARDINO', 'lalainetaray@gmail.com', 'LBT0083', NULL, 3, 1, NULL, '09291630099', 2, 2, '2020-06-20 09:11:12', '2020-06-20 09:11:12', NULL, NULL),
(84, 'BERNARDINO', 'CARLOTA', 'PICACHE', 'talots0825@gmail.com', 'CPB0084', NULL, 3, 1, NULL, '09120987645', 2, 2, '2020-06-20 09:16:21', '2020-06-20 09:16:21', NULL, NULL),
(85, 'SOBERANO', 'LIZA', 'GIL', 'soberano@gmail.com', 'LGS0085', NULL, 3, 1, NULL, '12345678909', 2, 2, '2020-06-20 09:19:28', '2020-06-20 09:19:28', NULL, NULL),
(86, 'SIMBULAN', 'CAMILLE GABRIELLE', 'PICACHE', 'camillegabrielle0818@gmail.com', 'CPS0086', NULL, 3, 1, NULL, '09291625284', 2, 2, '2020-06-20 10:05:20', '2020-06-20 10:05:20', NULL, NULL),
(87, 'TARAY', 'LALAINE', 'BERNARDINO', 'lalainetaray@gmail.com', 'LBT0087', NULL, 3, 1, NULL, '09291630099', 2, 2, '2020-06-20 10:16:51', '2020-06-20 10:16:51', NULL, NULL),
(88, 'SOBERANO', 'LIZA', 'GIL', 'soberano@gmail.com', 'LGS0088', NULL, 3, 1, NULL, '12345678909', 2, 2, '2020-06-20 10:18:52', '2020-06-20 10:54:14', NULL, NULL),
(89, 'Sandy', 'Spongebob', 'L', 'emersonptrp@yahoo.com', 'SLS0089', NULL, 3, 1, NULL, '09190000000', 10, 10, '2020-06-20 13:07:59', '2020-06-21 02:43:43', NULL, NULL),
(90, 'Spongebob', 'Squarepants', 'B', 'emerson@GMAIL.COM', 'SBS0090', NULL, 3, 1, NULL, '123456789', 10, 13, '2020-06-20 13:11:28', '2020-06-21 02:45:44', NULL, NULL),
(91, 'Alawi', 'Ivana', 'Y', 'ivana@alawi.com', 'IYA0091', NULL, 3, 1, NULL, '0919806679', 19, 31, '2020-06-20 13:16:10', '2020-06-20 13:52:24', NULL, NULL),
(92, 'Dimagiba', 'Giba', 'A', 'emerson@gmai.com', 'GAD0092', NULL, 3, 1, NULL, '123456+789', 10, 13, '2020-06-20 13:18:40', '2020-06-20 13:43:25', NULL, NULL),
(93, 'Moreno', 'Iskolar', 'M', 'iskomoderno@maynila.com', 'IMM0093', NULL, 3, 1, NULL, '09197008900', 19, 31, '2020-06-20 13:20:05', '2020-06-20 14:56:21', NULL, NULL),
(94, 'Forcefull', 'Evacuation', 'E', 'emerson@yahoo.com', 'EEF0094', NULL, 3, 1, NULL, '123456789', 10, 13, '2020-06-20 13:20:57', '2020-06-20 13:45:25', NULL, NULL),
(95, 'Estozo', 'Maiko', 'T', 'maiko.estozo@gmail.com', 'MTE0095', NULL, 3, 1, NULL, '09429772007', 15, 15, '2020-06-20 13:21:50', '2020-06-20 13:21:50', NULL, NULL),
(96, 'Duon', 'Din', 'S', 'emeerson@gmail.com', 'DSD0096', NULL, 3, 1, NULL, '123456789', 10, 13, '2020-06-20 13:22:53', '2020-06-20 14:08:59', NULL, NULL),
(97, 'Sy', 'Nisino', 'K', 'emerson@yahoo.com', 'NKS0097', NULL, 3, 1, NULL, '123456789', 10, 10, '2020-06-20 13:25:17', '2020-06-20 13:25:17', NULL, NULL),
(98, 'Kingkong', 'Godzilla', 'K', 'emerson@hotmail.com', 'GKK0098', NULL, 3, 1, NULL, '123456789', 10, 10, '2020-06-20 13:27:17', '2020-06-20 13:27:17', NULL, NULL),
(99, 'Cruz', 'Bumbo', 'S', 'bumbocruz@hotmail.com', 'BSC0099', NULL, 3, 1, NULL, '09154330849', 14, 14, '2020-06-20 13:29:05', '2020-06-20 13:29:05', NULL, NULL),
(100, 'Game', 'Na Game', 'Na', '12312321@yahoo.com', 'NNG0100', NULL, 3, 1, NULL, '1234569789', 10, 13, '2020-06-20 13:29:14', '2020-06-20 13:48:37', NULL, NULL),
(101, 'DELAPOSAS', 'BATO', 'PICK', 'batodelaposas@pnp.com', 'BPD0101', NULL, 3, 1, NULL, '0919902256', 32, 32, '2020-06-20 13:30:01', '2020-06-20 13:30:01', NULL, NULL),
(102, 'Pimentel', 'Charina', 'C', 'charm26.cp@gmail.com', 'CCP0102', NULL, 3, 1, NULL, '0922622760', 15, 15, '2020-06-20 13:30:37', '2020-06-20 13:30:37', NULL, NULL),
(103, 'Sit', 'Sirit', 'Sit', '123458@hotmail.com', 'SSS0103', NULL, 3, 1, NULL, '123456789', 10, 10, '2020-06-20 13:31:39', '2020-06-20 13:31:39', NULL, NULL),
(104, 'gray', 'catriona', 'v', 'catrionagray@missu.com', 'cvg0104', NULL, 3, 1, NULL, '09198033789', 52, 52, '2020-06-20 13:32:24', '2020-06-20 13:32:24', NULL, NULL),
(105, 'Wrong', 'Answer', 'Only', '545455@yahoo.com', 'AOW0105', NULL, 3, 1, NULL, '1234565', 10, 10, '2020-06-20 13:33:46', '2020-06-20 13:33:46', NULL, NULL),
(106, 'zubiri', 'juan miguel', 'V', 'juanmigz@zub.com', 'jVz0106', NULL, 3, 1, NULL, '09098009088', 18, 18, '2020-06-20 13:36:08', '2020-06-20 13:52:53', NULL, NULL),
(107, 'Egg', 'Surprise', '!', '23412@gmail.com', 'S!E0107', NULL, 3, 1, NULL, '12365496', 10, 10, '2020-06-20 13:38:06', '2020-06-20 13:38:06', NULL, NULL),
(108, 'Sumo', 'Sam', 'W', 'wljnioaj@yahoo.com', 'SWS0108', NULL, 3, 1, NULL, '46549849*', 10, 13, '2020-06-20 13:39:48', '2020-06-20 14:17:22', NULL, NULL),
(109, 'Cruz', 'Bumbo', 'S', 'bumbocruz@hotmail.com', 'BSC0109', NULL, 3, 1, NULL, '90154330849', 15, 15, '2020-06-20 13:42:41', '2020-06-20 13:42:41', NULL, NULL),
(110, 'Sana', 'All', 'P', '65466@yahoo.com', 'APS0110', NULL, 3, 1, NULL, '46516419', 10, 13, '2020-06-20 13:46:32', '2020-06-21 02:17:23', NULL, NULL),
(111, 'captain', 'america', 'S', '1651+59@gmail.com', 'aSc0111', NULL, 3, 1, NULL, '456+459', 10, 10, '2020-06-20 13:48:45', '2020-06-21 02:44:45', NULL, NULL),
(112, 'jolie', 'Angeline', 'p', 'maleficent@jolie.com', 'Apj0112', NULL, 3, 1, NULL, '09097891256', 52, 52, '2020-06-20 13:50:59', '2020-06-20 13:50:59', NULL, NULL),
(113, 'Buendia', 'Ely', 'B', '654654@yahoo.com', 'EBB0113', NULL, 3, 1, NULL, '54654+98', 10, 13, '2020-06-20 13:59:28', '2020-06-21 02:15:43', NULL, NULL),
(114, 'Pimentel', 'Charina', 'C', 'charm26.cp@gmail.com', 'ccp0114', NULL, 3, 1, NULL, '0922622760', 16, 17, '2020-06-20 14:02:20', '2020-06-21 01:00:09', NULL, NULL),
(115, 'Toshiba', 'Lenovo', 'g', '+564+594@YAHOO.COM', 'LgT0115', NULL, 3, 1, NULL, '5645641+9', 10, 13, '2020-06-20 14:03:39', '2020-06-21 02:48:14', NULL, NULL),
(116, 'Potter', 'Harry', 'b', '6540+941+@yahoo.com', 'HbP0116', NULL, 3, 1, NULL, '061+65465', 10, 13, '2020-06-20 14:05:43', '2020-06-21 02:22:29', NULL, NULL),
(117, 'Enriquez', 'Michael', 'H', 'mikeenriquez@gma.com', 'mhE0117', NULL, 3, 1, NULL, '09097965600', 52, 1, '2020-06-20 14:06:54', '2020-06-21 03:29:32', NULL, NULL),
(118, 'Maleficent', 'Mistress', 'E', '651+6541@gmail.com', 'MEM0118', NULL, 3, 1, NULL, '6+54+59', 10, 10, '2020-06-20 14:09:41', '2020-06-20 14:09:41', NULL, NULL),
(119, 'hija', 'Ako', 'A', '654+984+9@GMAIL.COM', 'AAh0119', NULL, 3, 1, NULL, '516495615', 10, 10, '2020-06-20 14:11:38', '2020-06-20 14:11:38', NULL, NULL),
(120, 'Hulk', 'Hogan', 'W', '6541+594@yahoo.com', 'HWH0120', NULL, 3, 1, NULL, '231+564+95', 10, 13, '2020-06-20 14:14:44', '2020-06-21 02:24:34', NULL, NULL),
(121, 'Sunny', 'Side', 'U', '+654+98@yahoo.com', 'SUS0121', NULL, 3, 1, NULL, '26+54+89', 10, 10, '2020-06-20 14:16:10', '2020-06-20 14:16:10', NULL, NULL),
(122, 'Lim', 'Allen Angelo', 'Zulueta', 'angelozuluetalim@gmail.com', 'AZL0122', NULL, 3, 1, NULL, '09178994879', 16, 17, '2020-06-20 14:27:22', '2020-06-21 01:02:24', NULL, NULL),
(123, 'Ben', 'Ten', 'R', '6541654@YAHOO.COM', 'TRB0123', NULL, 3, 1, NULL, '6326+54+59', 10, 13, '2020-06-20 14:33:58', '2020-06-21 02:26:19', NULL, NULL),
(124, 'Drake', 'Nathan', 'm', '+654+984@gmail.com', 'NmD0124', NULL, 3, 1, NULL, '45649', 10, 10, '2020-06-20 14:37:15', '2020-06-20 14:37:15', NULL, NULL),
(125, 'Summer', 'Solstice', 'L.', '56+4984@gmail.com', 'SLS0125', NULL, 3, 1, NULL, '534+894', 10, 13, '2020-06-20 14:39:33', '2020-06-21 02:16:47', NULL, NULL),
(126, 'Smallvile', 'Kent', 'S', '561+954@GMAIL.COM', 'KSS0126', NULL, 3, 1, NULL, '2+564+89', 10, 10, '2020-06-20 14:46:59', '2020-06-21 02:44:58', NULL, NULL),
(127, 'SIMBULAN', 'CAMILLE GABRIELLE', 'PICACHE', 'camillegabrielle0818@gmail.com', 'CPS0127', NULL, 3, 1, NULL, '09291625284', 2, 1, '2020-06-21 03:44:51', '2020-06-21 04:19:21', NULL, NULL),
(128, 'Samar', 'Rhoda', 'M', 'rhh@gmail.com', 'rms0128', NULL, 3, 1, NULL, '2312340-1', 12, 12, '2020-06-21 03:58:01', '2020-06-21 04:04:14', NULL, NULL),
(129, 'himura', 'kenshin', 'batousai', 'charm26.cp@gmail.com', 'kbh0129', NULL, 3, 1, NULL, '09198978867', 18, 18, '2020-06-21 04:21:07', '2020-06-21 04:21:07', NULL, NULL),
(130, 'AKIZUKI', 'ROBERT', 'H', 'charm26.cp@gmail.com', 'RHA0130', NULL, 3, 1, NULL, '0912678568', 18, 13, '2020-06-21 04:51:45', '2020-06-21 07:48:01', NULL, NULL),
(131, 'magdaraog', 'jahred', 'c', 'jm@yahoo.com', 'jcm0131', NULL, 3, 1, NULL, '09214483948', 12, 12, '2020-06-21 05:59:38', '2020-06-21 05:59:38', NULL, NULL),
(132, 'lazaro', 'ben', NULL, 'charm26.cp@gmail.com', 'bl0132', NULL, 3, 1, NULL, '0919902256', 18, 18, '2020-06-21 06:07:03', '2020-06-21 06:07:03', NULL, NULL),
(133, 'valles', 'rina', 'k', 't@yahho.com', 'rkv0133', NULL, 3, 1, NULL, '908-9999-9090', 12, 12, '2020-06-21 06:10:58', '2020-06-21 06:10:58', NULL, NULL),
(134, 'magdaraog', 'alexandria', 's', 'alexa@g.com', 'asm0134', NULL, 3, 1, NULL, '686879696', 12, 12, '2020-06-21 07:11:33', '2020-06-21 07:11:33', NULL, NULL),
(135, 'PIMENTEL', 'Charina Test Test Tet', 'C', 'charm26.cp@gmail.com', 'ccP0135', NULL, 3, 1, NULL, '0919902256', 18, 1, '2020-06-21 08:55:57', '2020-06-21 12:33:21', NULL, NULL),
(136, 'valleser', 'philomena faith', 'pimentel', 'charm26.cp@gmail.com', 'ppv0136', NULL, 3, 1, NULL, '09097891256', 18, 18, '2020-06-21 09:26:26', '2020-06-21 09:26:26', NULL, NULL),
(137, 'Milby', 'Samuel', 'S', 'milbysamuel@abscbn.com', 'SSM0137', NULL, 3, 1, NULL, '09121212121', 11, 11, '2020-06-21 11:16:22', '2020-06-21 11:19:45', NULL, NULL),
(138, 'Santos', 'Maribeth', 'Domingo', 'maribethsantos@mail.com', 'MS0138', NULL, 3, 1, NULL, '0906000000', 2, 1, '2020-06-21 23:48:33', '2020-06-22 00:41:35', NULL, NULL),
(139, 'CABINGAS', 'RONNIE', 'CASTILLO', 'TMC@ph.hrgworldwide.com', 'RCC0139', NULL, 3, 1, NULL, '639188236107', 18, 1, '2020-06-24 01:26:43', '2020-07-06 15:21:18', NULL, NULL),
(140, 'MANAGUEY', 'RITA', 'DAG-OY', 'segovia.marygrace77@gmail.com', 'RDM0140', NULL, 3, 1, NULL, '09162745639', 18, 1, '2020-06-24 02:18:48', '2020-06-30 05:11:59', NULL, NULL),
(141, 'BRITANICO', 'RONALD', 'BELDUA', 'ronaldbrit519@gmail.com', 'RBB0141', NULL, 3, 1, NULL, '09178443063', 10, 10, '2020-07-03 02:33:10', '2020-07-03 04:55:00', NULL, NULL),
(142, 'CHOI', 'TAEYANG', 'CONCEPCION', 'eccachoi@gmail.com', 'TCC0142', NULL, 3, 1, NULL, '09054615450', 12, 12, '2020-07-03 12:49:31', '2020-07-03 13:10:33', NULL, NULL),
(143, 'ABULOC', 'KRISTINE BETH', 'CREENCIA', 'TMC@ph.travelleadersint.com', 'KCA0143', NULL, 3, 1, NULL, '09209099476', 15, 17, '2020-07-06 10:03:02', '2020-07-06 10:24:46', NULL, NULL),
(144, 'DE LEON', 'MYROADE', 'RAMIREZ', 'TMC@ph.travelleadersint.com', 'MRD0144', NULL, 3, 1, NULL, '639452585688', 32, 32, '2020-07-10 08:12:14', '2020-07-10 08:12:14', NULL, NULL),
(145, 'Reyes', 'Noemi', 'Rola', 'TMC@ph.hrgworldwide.com', 'NRR0145', NULL, 3, 1, NULL, '09209099476', 14, 17, '2020-07-14 07:50:26', '2020-07-14 07:55:41', NULL, NULL),
(146, 'TIGCAL', 'CATALINA', 'GUMAPAS', 'iangabrielle01@gmail.com', 'CGT0146', NULL, 3, 1, NULL, '09354082450', 16, 17, '2020-07-14 12:50:29', '2020-07-14 12:54:17', NULL, NULL),
(147, 'HAYASHI', 'MA. CORAZON', 'VILLACORTE', 'cora_hayashi@yahoo.com', 'MVH0147', NULL, 3, 1, NULL, '09178321661', 32, 32, '2020-07-15 06:28:36', '2020-07-15 08:56:15', NULL, NULL),
(148, 'Reyes', 'Noemi', 'Rola', 'TMC@ph.travelleadersint.com', 'NRR0148', NULL, 3, 1, NULL, '09209099476', 19, 19, '2020-07-18 07:48:43', '2020-07-18 07:48:43', NULL, NULL),
(149, 'Daniel', 'Ronaldo', 'Panoncillo', 'ronaldodaniel1960@gmail.com', 'RPD0149', NULL, 3, 1, NULL, '09463939204', 52, 52, '2020-07-20 06:45:49', '2020-07-20 07:51:09', NULL, NULL),
(150, 'Gironella', 'Fernando', 'Bitao', 'gerliegironellacarranza@gmail.com', 'FBG0150', NULL, 3, 1, NULL, 'P9979879A', 18, 18, '2020-07-20 08:28:39', '2020-07-20 08:53:05', NULL, NULL),
(151, 'OCTAVIO', 'BRIGIDA', 'MANALO', 'bmoctavio@gmail.com', 'BMO0151', NULL, 3, 1, NULL, '09985384311', 32, 31, '2020-07-20 08:42:45', '2020-07-20 08:47:59', NULL, NULL),
(152, 'ACASIO', 'RORIE RICHIE', 'MENDOZA', 'tmc@ph.travelleadersint.com', 'RMA0152', NULL, 3, 1, NULL, '09175849923', 19, 19, '2020-07-20 09:09:32', '2020-07-21 02:30:23', NULL, NULL),
(153, 'BENAURO', 'DOMINIC', 'BOSE', 'tmc@ph.travelleadersint.com', 'DBB0153', NULL, 3, 1, NULL, '09209099476', 10, 10, '2020-07-21 00:59:05', '2020-07-21 00:59:05', NULL, NULL),
(154, 'Madanguit', 'Hans Kevin', 'R', 'hkrmadanguit@gmail.com', 'HRM0154', NULL, 3, 1, NULL, '09064843975', 11, 11, '2020-07-21 01:04:15', '2020-07-21 01:22:43', NULL, NULL),
(155, 'miranda', 'jesus jericson', 'cosare', 'tmc@ph.travelleadersint.com', 'jcm0155', NULL, 3, 1, NULL, '639209099476', 12, 12, '2020-07-21 01:13:29', '2020-07-21 01:13:29', NULL, NULL),
(156, 'STRICKLAND', 'MARIA ISCARLET', 'EBOL', 'TMC@PH.TRAVELLEADERSINT.COM', 'MES0156', NULL, 3, 1, NULL, '639209099476', 18, 18, '2020-07-23 02:20:49', '2020-07-23 02:20:49', NULL, NULL),
(157, 'STRICKLAND', 'SOFIA MARIE', '', 'TMC@PH.TRAVELLEADERSINT.COM', 'SMS0157', NULL, 3, 1, NULL, '639209099476', 19, 31, '2020-07-23 02:47:37', '2020-07-23 02:51:04', NULL, NULL),
(158, 'STRICKLAND', 'CLAIRE', 'TALANAY', 'tmc@ph.travelleadersint.com', 'CTS0158', NULL, 3, 1, NULL, '639209099476', 32, 32, '2020-07-23 03:40:25', '2020-07-23 03:40:25', NULL, NULL),
(159, 'JACLA', 'JOHN PAUL', 'S', 'TMC@PH.TRAVELLEADERSINT.COM', 'JSJ0159', NULL, 3, 1, NULL, '+639209099476', 11, 11, '2020-07-24 05:49:10', '2020-07-24 05:49:10', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `airlines`
--
ALTER TABLE `airlines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reason_denials`
--
ALTER TABLE `reason_denials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections_upload`
--
ALTER TABLE `sections_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_application`
--
ALTER TABLE `tec_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tec_application_upload`
--
ALTER TABLE `tec_application_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_amount`
--
ALTER TABLE `tr_amount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_application`
--
ALTER TABLE `tr_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_application_upload`
--
ALTER TABLE `tr_application_upload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_applicants`
--
ALTER TABLE `type_applicants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `airlines`
--
ALTER TABLE `airlines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=243;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `reason_denials`
--
ALTER TABLE `reason_denials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sections_upload`
--
ALTER TABLE `sections_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tec_application`
--
ALTER TABLE `tec_application`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tec_application_upload`
--
ALTER TABLE `tec_application_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tr_amount`
--
ALTER TABLE `tr_amount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tr_application`
--
ALTER TABLE `tr_application`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tr_application_upload`
--
ALTER TABLE `tr_application_upload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_applicants`
--
ALTER TABLE `type_applicants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
