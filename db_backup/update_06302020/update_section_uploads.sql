UPDATE `sections_upload` SET `description` = 'Certification from the United Nations (UN) Organization or its agencies (WHO, World <br>Bank, FAO, ILO, UNICEF, UNIFA, etc.)<br>' WHERE `sections_upload`.`id` = 3;

INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '3', 'Certification from the agency or organization concerned that the passenger belongs to<br> one of the categories, and that their travel is funded by the <br>agency or organization concerned<br>', NULL);
INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '4', 'Endorsement from the Philippine Sports Commission.', NULL);
INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '4', 'Travel Authority issued by the Philippine Sports Commission.', NULL);
INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '4', ' Invitation from the host country.', NULL);
INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '5', 'Endorsement from DOST specifying that the Balik Scientist Awardee is exempted from<br>Travel Tax.', NULL);
INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '5', 'For the Spouse or Dependent of a Balik Scientist under Long-Term engagement:<br> Proof of Relationship to the Balik Scientist. (Marriage Certificate, Birth Certificate)', NULL);

UPDATE `sections_upload` SET `description` = 'NOTE: IF HIRED THROUGH POEA, the Overseas Employment Certificate (OEC) serves as the<br> Travel Tax Exemption Certificate (TEC) and THERE IS NO NEED TO APPLY FOR A TEC. <br> If directly hired abroad, please upload your Certificate of Employment issued by<br> the Philippine Embassy / Consulate in the place of work or an Employment Contract<br> authenticated by the Philippine Embassy / Consulate.' WHERE `sections_upload`.`id` = 1;


UPDATE `sections_upload` SET `section_id` = NULL WHERE `sections_upload`.`id` = 6;
UPDATE `sections_upload` SET `description` = 'a. Proof of permanent residence in foreign country (e.g. US Green card, Canadian<br> Form 1000, etc.) FRONT and BACK or b. Valid Certification from the Philippine<br> consulate / embassy indicating that the Filipino has resided uninterruptedly for a period<br> of five (5) years in the foreign country without having been absent <br>therefrom for more than six (6) months in any one year or appropriate<br> stamps in the passport to prove such length of stay.' WHERE `sections_upload`.`id` = 7;
UPDATE `sections_upload` SET `section_id` = NULL WHERE `sections_upload`.`id` = 8;
UPDATE `sections_upload` SET `section_id` = NULL WHERE `sections_upload`.`id` = 21;

UPDATE `sections_upload` SET `description` = 'Certification from the Department of Foreign Affairs.' WHERE `sections_upload`.`id` = 9;
UPDATE `sections_upload` SET `description` = 'Clear copy of Travel Authority or Travel Order from the Department Secretary<br> concerned to the effect that such officials / employees are traveling on<br> official business.' WHERE `sections_upload`.`id` = 10;

UPDATE `sections_upload` SET `description` = 'Certification to this effect from concerned Philippine government agency. NOTE: “Student” is defined<br> as a person attending formal classes in a foreign educational institution for the purpose <br>of taking up a course leading to a diploma, the duration of which is <br>not less than one (1) year. For UNDP Fellow: Certification from UNDP specifying<br> that the Fellow is exempted from Travel Tax.<br>' WHERE `sections_upload`.`id` = 12;

INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '20', 'Airline ticket used in traveling to the Philippines. <br> NOTE: In case your last arrival in the Philippines is stamped in an old <br>passport, you will need to scan this page from your old passport and<br> include it in the PDF file.', NULL);

UPDATE `sections_upload` SET `description` = 'Copy of Foreign passport of former Filipino or other evidence of<br> former Philippine Citizenship.<br>' WHERE `sections_upload`.`id` = 17;
UPDATE `sections_upload` SET `description` = 'Latest arrival stamp in the Philippines.' WHERE `sections_upload`.`id` = 18;
UPDATE `sections_upload` SET `description` = 'Marriage contract of accompanying spouse or Birth certificate or <br>adoption papers of the child.' WHERE `sections_upload`.`id` = 20;

INSERT INTO `sections_upload` (`id`, `section_id`, `description`, `name`) VALUES (NULL, '18', 'Endorsement from the Export Development Council (EDC).', NULL);