<?php

Route::any('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::any('/', 'WebController\HomeController@index');
Route::any('/home', 'WebController\HomeController@index')->name('home');
Route::any('/account', 'WebController\AccountController@index');
Route::any('/account/update', 'WebController\AccountController@update_account');
Route::any('/date_flight', 'Controller@date_flight');
Route::any('/check_date_flight', 'Controller@check_date_flight');

Route::any('/test_email', 'Controller@test_email');
Route::any('/test_viewmail/{option}', 'Controller@test_viewmail');

//Route::any('/update_name', 'Controller@update_name');

// when authenticated

Route::get('/images/{id}/{file}', [ function ($id, $file) {

    $folder_path = storage_path('tieza/tec/attachment/'.$id.'/'.$file);

    $explode_file = explode('.', $file);

    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
    {
    	$content = 'image/jpeg';
    }
    elseif(in_array($explode_file[1], ['pdf']))
    {
    	$content = 'application/pdf';
    }

    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
    
    abort(404);
}]);

Route::get('/attachment/{option}/{id}/{file}', [ function ($option, $id, $file) {

    $folder_path = storage_path('tieza/'.$option.'/attachment/'.$id.'/'.$file);

    $explode_file = explode('.', $file);

    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
    {
    	$content = 'image/jpeg';
    }
    elseif(in_array($explode_file[1], ['pdf']))
    {
    	$content = 'application/pdf';
    }

    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
    
    abort(404);
}])->middleware('auth');

Route::get('/qr_code/{file}', [ function ($file) {
	$folder_path = storage_path('tieza/tec/qr-code/'.$file);

	$explode_file = explode('.', $file);

	if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	{
	   	$content = 'image/jpeg';
	}
	elseif(in_array($explode_file[1], ['pdf']))
	{
	   	$content = 'application/pdf';
	}

	if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	abort(404);
}]);

Route::middleware(['isTravelTax'])->prefix('travel_tax')->group(function () {
	Route::any('/application/{option}', 'WebController\ApplicationController@application');

	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');
	
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');

	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');

	Route::get('/tr/certificate/{id}/{file}', [ function ($id, $file) {
	    $folder_path = storage_path('tieza/tr/certificate/'.$id.'/'.$file);
	    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => 'image/jpeg'));
	    abort(404);
	}]);
});

Route::middleware(['isProcessor'])->prefix('processor')->group(function () {
	// Web
	Route::any('/application/{option}', 'WebController\ApplicationController@application');
	Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');

	// Api
	Route::any('/tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/tec_application/datatables/attachment', 'APIController\ApplicationController@tec_application_datatables');

	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');

	Route::any('/filter_tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');

	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');
});

Route::middleware(['isRegularAdmin'])->prefix('supervisor')->group(function () {
	// Web
	Route::any('/application/{option}', 'WebController\ApplicationController@application');
	Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');
	Route::any('/application/{option}/delete', 'WebController\ApplicationController@delete_application');
	Route::any('/application/{option}/remove', 'WebController\ApplicationController@remove_application_attachment');
	Route::any('/application/{option}/print', 'WebController\ApplicationController@print_application');
	Route::any('/application/{option}/generate', 'WebController\ApplicationController@generate_application');

	// Api
	Route::any('/tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/tec_application/datatables/attachment', 'APIController\ApplicationController@tec_application_datatables');

	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');

	Route::any('/filter_tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');

	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');

	Route::any('/airlines/json', 'APIController\ApplicationController@filter_airline_by');

	Route::get('/esignature/{file}', [ function ($file) {
	    $folder_path = storage_path('esig/'.$file);

	    if (file_exists($folder_path)) {
	        return response()->file($folder_path, array('Content-Type' =>'image/jpeg'));
	    }
	    abort(404);
	}]);

	Route::get('/certificate/{id}/{file}', [ function ($id, $file) {

	    $folder_path = storage_path('tieza/tec/certificate/'.$id.'/'.$file);

	    $explode_file = explode('.', $file);

	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
	    	$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
	    	$content = 'application/pdf';
	    }

	    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);

	Route::get('tr/certificate/{id}/{file}', [ function ($id, $file) {

	    $folder_path = storage_path('tieza/tr/certificate/'.$id.'/'.$file);

	    $explode_file = explode('.', $file);

	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
	    	$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
	    	$content = 'application/pdf';
	    }

	   if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);
});

Route::middleware(['isSuperAdmin'])->group(function () {
	// only for super admin

	// Web
	Route::any('/system_config/{option}', 'WebController\SystemConfigController@system_config');
	Route::any('/system_config/{option}/save', 'WebController\SystemConfigController@save_system_config');
	Route::any('/system_config/{option}/delete', 'WebController\SystemConfigController@delete_system_config');

	Route::any('/application/{option}', 'WebController\ApplicationController@application');
	Route::any('/application/{option}/save', 'WebController\ApplicationController@save_application');
	Route::any('/application/{option}/delete', 'WebController\ApplicationController@delete_application');
	Route::any('/application/{option}/remove', 'WebController\ApplicationController@remove_application_attachment');
	Route::any('/application/{option}/print', 'WebController\ApplicationController@print_application');
	Route::any('/application/{option}/generate', 'WebController\ApplicationController@generate_application');
	
	// Api
	Route::any('/user_account/json', 'APIController\SystemConfigController@filter_user_account_by');
	Route::any('/filter_list/json', 'APIController\SystemConfigController@get_filter_list_by');

	Route::any('/tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/tec_application/datatables/attachment', 'APIController\ApplicationController@tec_application_datatables');

	Route::any('/tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');
	Route::any('/tr_application/datatables/attachment', 'APIController\ApplicationController@tr_application_datatables');

	Route::any('/filter_tec_application/json', 'APIController\ApplicationController@filter_tec_application_by');
	Route::any('/filter_tr_application/json', 'APIController\ApplicationController@filter_tr_application_by');

	Route::any('/section/json', 'APIController\ApplicationController@filter_section_by');
	Route::any('/section_upload/json', 'APIController\ApplicationController@filter_section_upload_by');

	Route::get('/esignature','APIController\ApplicationController@get_image_application');

	Route::any('/airlines/json', 'APIController\ApplicationController@filter_airline_by');

	Route::get('/certificate/{id}/{file}', [ function ($id, $file) {
	    $folder_path = storage_path('tieza/tec/certificate/'.$id.'/'.$file);

	    if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => 'image/jpeg'));
	    
	    abort(404);
	}]);

	Route::get('tr/certificate/{id}/{file}', [ function ($id, $file) {

	    $folder_path = storage_path('tieza/tr/certificate/'.$id.'/'.$file);

	    $explode_file = explode('.', $file);

	    if(in_array($explode_file[1], ['png', 'jpg', 'jpeg']))
	    {
	    	$content = 'image/jpeg';
	    }
	    elseif(in_array($explode_file[1], ['pdf']))
	    {
	    	$content = 'application/pdf';
	    }

	   if(file_exists($folder_path)) return response()->file($folder_path, array('Content-Type' => $content));
	    
	    abort(404);
	}]);
});


Route::middleware(['isUser'])->group(function () {
	// only for user
});